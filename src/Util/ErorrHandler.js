import * as ActionType from "../Action/ActionTypes";
import { message } from "antd";
import { DEFAULT_LANGUAGE } from "../Enum/PropertiesConstant";
import i18next from "i18next";

export const restoreApp = (dispatch) => {
  dispatch({ type: ActionType.RESET_STORE });
  dispatch({ type: ActionType.SET_LANGUAGE, payload: DEFAULT_LANGUAGE });
};

export const restoreWithThunk = () => {
  return (dispatch) => {
    dispatch({ type: ActionType.RESET_STORE });
    dispatch({ type: ActionType.SET_LANGUAGE, payload: DEFAULT_LANGUAGE });
  };
};

export const errorLoading = (dispatch) => {
  dispatch({ type: ActionType.AUTH_REST_ERROR });
  dispatch({ type: ActionType.TABLE_REST_ERROR });
  dispatch({ type: ActionType.SKELETON_REST_ERROR });
};

export const errorAuth = async (dispatch, error, reLoad) => {
  errorLoading(dispatch);

  if (error.response) {
    if (error.response.status === 401 || error.response.status === 500) {
      error.response.data.nexsoft.payload && message.error({ content: error.response.data.nexsoft.payload.status.message });
      errorLoading(dispatch);
      restoreApp(dispatch);
      reLoad();
    } else if (error.response.status === 400) {
      error.response.data.nexsoft.payload && message.error({ content: error.response.data.nexsoft.payload.status.message });
    } else if (error.response.status === 502) {
      errorLoading(dispatch);
      message.error(i18next.t("COMMON:SERVER_OFF"));
    }
  } else {
    error.message && message.error({ content: i18next.t("COMMON:SERVER_OFF") });
    errorLoading(dispatch);
    restoreApp(dispatch);
  }

  errorLoading(dispatch);
};

export const errorFetching = (dispatch, error) => {
  errorLoading(dispatch);

  if (error.response) {
    if (error.response.status === 401 || error.response.status === 500) {
      error.response.data.nexsoft.payload && message.error({ content: error.response.data.nexsoft.payload.status.message });
      errorLoading(dispatch);
      restoreApp(dispatch);
    } else if (error.response.status === 400) {
      error.response.data.nexsoft.payload && message.error({ content: error.response.data.nexsoft.payload.status.message });
    } else if (error.response.status === 502) {
      errorLoading(dispatch);
      message.error(i18next.t("COMMON:SERVER_OFF"));
    }
  } else {
    error.message && message.error({ content: i18next.t("COMMON:SERVER_OFF") });
    errorLoading(dispatch);
    // restoreApp(dispatch);
  }

  errorLoading(dispatch);
};

export const errorExpired = (dispatch, error) => {
  if (!error.response) {
    message.error({ content: error.message });
    setTimeout(() => restoreApp(dispatch), 256);
  } else {
    message.error({ content: error.response.data.nexsoft.payload.status.message });
    setTimeout(() => restoreApp(dispatch), 256);
  }
};
