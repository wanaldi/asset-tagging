import {useSelector} from "react-redux";
import {Redirect, Route} from "react-router-dom";
import {ROUTER_CONSTANT} from "../Enum/RouterConstant";

export const PrivateRoute = ({component: Component, ...rest}) => {
	const isAuthenticated = useSelector((state) => state.authentication.isAuthenticated);

	return (
		<Route
			{...rest}
			render={(props) => isAuthenticated ? (<Component {...props} />) : (<Redirect to={{pathname: ROUTER_CONSTANT.LOGIN}}/>)}
		/>
	);
};
