export const stateRedux = (state, permission) => {
  return {
    prepare_component: state.common.prepare_component,
    token: state.authentication.token,
    refresh_token: state.authentication.refresh_token,
    user: parseInt(state.authentication.user),
    permission: state.permission[permission],
    table_loading: state.common.table_loading,
    skeleton_loading: state.common.skeleton_loading,
    language: state.authentication.language,
  };
};
