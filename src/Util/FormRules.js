import Label from "../Component/Common/Label/Label";
import { REGEX_CONSTANT } from "../Enum/RegexConstant";

export const rulesLogin = (translate) => {
  return {
    username: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMMON:FIELD_USERNAME") })} size="smaller" />,
      },
    ],
    password: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMMON:FIELD_PASSWORD") })} size="smaller" />,
      },
    ],
  };
};

export const rulesUser = (translate) => {
  return {
    username: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_USERNAME") })} size="smaller" />,
      },
    ],
    nik: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_NIK") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NIK,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("USER:FIELD_NIK"), len: 20 })} size="smaller" />,
      },
    ],
    npwp: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_NPWP") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NPWP,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT_NPWP")} size="smaller" />,
      },
    ],
    address: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_ADDRESS") })} size="smaller" />,
      },
      {
        min: 10,
        message: <Label text={translate("COMMON:MIN", { field: translate("USER:FIELD_ADDRESS"), len: 10 })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("USER:FIELD_ADDRESS"), len: 100 })} size="smaller" />,
      },
    ],
    country_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_COUNTRY") })} size="smaller" />,
      },
    ],
    district_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_DISTRICT") })} size="smaller" />,
      },
    ],
    sub_district_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_SUB_DISTRICT") })} size="smaller" />,
      },
    ],
    urban_village_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_URBAN_VILLAGE") })} size="smaller" />,
      },
    ],
    postal_code_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_POSTAL_CODE") })} size="smaller" />,
      },
    ],
    island_id: [],
    latitude: [
      {
        pattern: REGEX_CONSTANT.FLOAT,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    client_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_CLIENT_ID") })} size="smaller" />,
      },
    ],
    password: [
      {
        required: true,
        message: <span style={{ fontSize: "small" }}>{translate("COMMON:REQUIRE", { field: translate("USER:FIELD_PASSWORD") })}</span>,
      },
      {
        min: 8,
        message: <span style={{ fontSize: "small" }}>{translate("COMMON:MIN", { field: translate("USER:FIELD_PASSWORD"), len: 8 })}</span>,
      },
      {
        max: 20,
        message: <span style={{ fontSize: "small" }}>{translate("COMMON:MAX", { field: translate("USER:FIELD_PASSWORD"), len: 20 })}</span>,
      },
      {
        pattern: REGEX_CONSTANT.PASSWORD,
        message: <span style={{ fontSize: "small" }}>{translate("USER:ADD.VALIDATE_PASSWORD")}</span>,
      },
    ],
    first_name: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_AUTH_FIRST_NAME") })} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("USER:FIELD_AUTH_FIRST_NAME"), len: 20 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NAME,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    last_name: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_AUTH_FIRST_NAME") })} size="smaller" />,
      },
      {
        max: 35,
        message: <Label text={translate("COMMON:MAX", { field: translate("USER:FIELD_AUTH_LAST_NAME"), len: 35 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NAME,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    phone_country_code: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_AUTH_PHONE") })} size="smaller" />,
      },
    ],
    phone: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_AUTH_PHONE") })} size="smaller" />,
      },
      {
        max: 13,
        message: <Label text={translate("COMMON:MAX", { field: translate("USER:FIELD_AUTH_PHONE"), len: 13 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.PHONE_NUMBER,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    email: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_AUTH_EMAIL") })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("USER:FIELD_AUTH_EMAIL"), len: 100 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.EMAIL,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    group_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_GROUP") })} size="smaller" />,
      },
    ],
    role_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_ROLE") })} size="smaller" />,
      },
    ],
    locale: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_LOCALE") })} size="smaller" />,
      },
    ],
    status: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_STATUS") })} size="smaller" />,
      },
    ],
    additional_information_key: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_KEY") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INFORMATION_KEY,
        message: <Label text={translate("COMMON:VALIDATE_INFORMATION")} size="smaller" />,
      },
    ],
    additional_information_value: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_VALUE") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INFORMATION_VALUE,
        message: <Label text={translate("COMMON:VALIDATE_INFORMATION")} size="smaller" />,
      },
    ],
  };
};

export const rulesNexchiefAccount = (translate) => {
  return {
    username: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_USERNAME") })} size="smaller" />,
      },
    ],
    position: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_POSITION") })} size="smaller" />,
      },
    ],
    join_date: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("CONTACT_PERSON:FIELD_JOIN_DATE") })} size="smaller" />,
      },
    ],
    resign_date: [],
    nik: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_NIK") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NIK,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("NEXCHIEF_ACCOUNT:FIELD_NIK"), len: 20 })} size="smaller" />,
      },
    ],
    npwp: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_NPWP") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NPWP,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT_NPWP")} size="smaller" />,
      },
    ],
    address: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_ADDRESS") })} size="smaller" />,
      },
      {
        min: 10,
        message: <Label text={translate("COMMON:MIN", { field: translate("NEXCHIEF_ACCOUNT:FIELD_ADDRESS"), len: 10 })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("NEXCHIEF_ACCOUNT:FIELD_ADDRESS"), len: 100 })} size="smaller" />,
      },
    ],
    country_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_COUNTRY") })} size="smaller" />,
      },
    ],
    district_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_DISTRICT") })} size="smaller" />,
      },
    ],
    sub_district_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_SUB_DISTRICT") })} size="smaller" />,
      },
    ],
    urban_village_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_URBAN_VILLAGE") })} size="smaller" />,
      },
    ],
    postal_code_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_POSTAL_CODE") })} size="smaller" />,
      },
    ],
    island_id: [],
    latitude: [
      {
        pattern: REGEX_CONSTANT.FLOAT,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    client_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_CLIENT_ID") })} size="smaller" />,
      },
    ],
    password: [
      {
        required: true,
        message: <span style={{ fontSize: "small" }}>{translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_PASSWORD") })}</span>,
      },
      {
        min: 8,
        message: <span style={{ fontSize: "small" }}>{translate("COMMON:MIN", { field: translate("NEXCHIEF_ACCOUNT:FIELD_PASSWORD"), len: 8 })}</span>,
      },
      {
        max: 20,
        message: <span style={{ fontSize: "small" }}>{translate("COMMON:MAX", { field: translate("NEXCHIEF_ACCOUNT:FIELD_PASSWORD"), len: 20 })}</span>,
      },
      {
        pattern: REGEX_CONSTANT.PASSWORD,
        message: <span style={{ fontSize: "small" }}>{translate("NEXCHIEF_ACCOUNT:ADD.VALIDATE_PASSWORD")}</span>,
      },
    ],
    first_name: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_FIRST_NAME") })} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("NEXCHIEF_ACCOUNT:FIELD_FIRST_NAME"), len: 20 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NAME,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    last_name: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_LAST_NAME") })} size="smaller" />,
      },
      {
        max: 35,
        message: <Label text={translate("COMMON:MAX", { field: translate("NEXCHIEF_ACCOUNT:FIELD_LAST_NAME"), len: 35 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NAME,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    phone_country_code: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_AUTH_PHONE") })} size="smaller" />,
      },
    ],
    phone: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_PHONE") })} size="smaller" />,
      },
      {
        max: 13,
        message: <Label text={translate("COMMON:MAX", { field: translate("NEXCHIEF_ACCOUNT:FIELD_PHONE"), len: 13 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.PHONE_NUMBER,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    email: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_EMAIL") })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("NEXCHIEF_ACCOUNT:FIELD_EMAIL"), len: 100 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.EMAIL,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    group_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_GROUP") })} size="smaller" />,
      },
    ],
    role_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_ROLE") })} size="smaller" />,
      },
    ],
    locale: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_LOCALE") })} size="smaller" />,
      },
    ],
    status: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_STATUS") })} size="smaller" />,
      },
    ],
    additional_information_key: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_KEY") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INFORMATION_KEY,
        message: <Label text={translate("COMMON:VALIDATE_INFORMATION")} size="smaller" />,
      },
    ],
    additional_information_value: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("NEXCHIEF_ACCOUNT:FIELD_VALUE") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INFORMATION_VALUE,
        message: <Label text={translate("COMMON:VALIDATE_INFORMATION")} size="smaller" />,
      },
    ],
  };
};

export const rulesMappingNexseller = (translate) => {
  return {
    nexchief_account_name: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_NEXCHIEF_ACCOUNT_NAME") })} size="smaller" />,
      },
    ],
    nexseller_code: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_NEXSELLER_CODE") })} size="smaller" />,
      },
    ],
    nexseller_parent_code: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_NEXSELLER_PARENT_CODE") })} size="smaller" />,
      },
    ],
    product_mapping: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_PRODUCT_MAPPING") })} size="smaller" />,
      },
    ],
    salesman_mapping: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_SALESMAN_MAPPING") })} size="smaller" />,
      },
    ],
    customer_mapping: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_CUSTOMER_MAPPING") })} size="smaller" />,
      },
    ],
    hosting_only: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_HOSTING_ONLY") })} size="smaller" />,
      },
    ],
    sync_delivery_note: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_SYNC_DELIVERY_NOTE") })} size="smaller" />,
      },
    ],
    geo_tree: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_GEO_TREE") })} size="smaller" />,
      },
    ],
    activation_date: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_ACTIVATION_DATE") })} size="smaller" />,
      },
    ],
    price_group_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_PRICE_GROUP") })} size="smaller" />,
      },
    ],
    product_category_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_PRODUCT_CATEGORY") })} size="smaller" />,
      },
    ],
    product_class_from_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_PRODUCT_CLASS_FROM") })} size="smaller" />,
      },
    ],
    product_class_thru_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_PRODUCT_CLASS_THRU") })} size="smaller" />,
      },
    ],
    send_sync_data_to_email: [
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("MAPPING_NEXSELLER:FIELD_SEND_SYNC_DATA_TO_EMAIL"), len: 100 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.EMAIL,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    nd6_sync_method: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_ND6_SYNC_METHOD") })} size="smaller" />,
      },
    ],
    nik: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_NIK") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NIK,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("MAPPING_NEXSELLER:FIELD_NIK"), len: 20 })} size="smaller" />,
      },
    ],
    position: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_POSITION") })} size="smaller" />,
      },
    ],
    join_date: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_JOIN_DATE") })} size="smaller" />,
      },
    ],
    resign_date: [],
    npwp: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_NPWP") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NPWP,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT_NPWP")} size="smaller" />,
      },
    ],
    address: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_ADDRESS") })} size="smaller" />,
      },
      {
        min: 10,
        message: <Label text={translate("COMMON:MIN", { field: translate("MAPPING_NEXSELLER:FIELD_ADDRESS"), len: 10 })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("MAPPING_NEXSELLER:FIELD_ADDRESS"), len: 100 })} size="smaller" />,
      },
    ],
    country_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_COUNTRY") })} size="smaller" />,
      },
    ],
    district_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_DISTRICT") })} size="smaller" />,
      },
    ],
    sub_district_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_SUB_DISTRICT") })} size="smaller" />,
      },
    ],
    urban_village_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_URBAN_VILLAGE") })} size="smaller" />,
      },
    ],
    postal_code_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_POSTAL_CODE") })} size="smaller" />,
      },
    ],
    island_id: [],
    latitude: [
      {
        pattern: REGEX_CONSTANT.FLOAT,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    client_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_CLIENT_ID") })} size="smaller" />,
      },
    ],
    password: [
      {
        required: true,
        message: <span style={{ fontSize: "small" }}>{translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_PASSWORD") })}</span>,
      },
      {
        min: 8,
        message: <span style={{ fontSize: "small" }}>{translate("COMMON:MIN", { field: translate("MAPPING_NEXSELLER:FIELD_PASSWORD"), len: 8 })}</span>,
      },
      {
        max: 20,
        message: <span style={{ fontSize: "small" }}>{translate("COMMON:MAX", { field: translate("MAPPING_NEXSELLER:FIELD_PASSWORD"), len: 20 })}</span>,
      },
      {
        pattern: REGEX_CONSTANT.PASSWORD,
        message: <span style={{ fontSize: "small" }}>{translate("MAPPING_NEXSELLER:ADD.VALIDATE_PASSWORD")}</span>,
      },
    ],
    first_name: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_FIRST_NAME") })} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("MAPPING_NEXSELLER:FIELD_FIRST_NAME"), len: 20 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NAME,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    phone_country_code: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_AUTH_PHONE") })} size="smaller" />,
      },
    ],
    phone: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_PHONE") })} size="smaller" />,
      },
      {
        max: 13,
        message: <Label text={translate("COMMON:MAX", { field: translate("MAPPING_NEXSELLER:FIELD_PHONE"), len: 13 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.PHONE_NUMBER,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    email: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_EMAIL") })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("MAPPING_NEXSELLER:FIELD_EMAIL"), len: 100 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.EMAIL,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    group_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_GROUP") })} size="smaller" />,
      },
    ],
    role_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_ROLE") })} size="smaller" />,
      },
    ],
    locale: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_LOCALE") })} size="smaller" />,
      },
    ],
    status: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_STATUS") })} size="smaller" />,
      },
    ],
    additional_information_key: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_KEY") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INFORMATION_KEY,
        message: <Label text={translate("COMMON:VALIDATE_INFORMATION")} size="smaller" />,
      },
    ],
    additional_information_value: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_NEXSELLER:FIELD_VALUE") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INFORMATION_VALUE,
        message: <Label text={translate("COMMON:VALIDATE_INFORMATION")} size="smaller" />,
      },
    ],
  };
};

export const rulesMappingPrincipal = (translate) => {
  return {
    nexseller_code: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_PRINCIPAL_CODE") })} size="smaller" />,
      },
    ],
    nexseller_parent_code: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_PRINCIPAL_PARENT_CODE") })} size="smaller" />,
      },
    ],
    product_mapping: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_PRODUCT_MAPPING") })} size="smaller" />,
      },
    ],
    salesman_mapping: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_SALESMAN_MAPPING") })} size="smaller" />,
      },
    ],
    customer_mapping: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_CUSTOMER_MAPPING") })} size="smaller" />,
      },
    ],
    hosting_only: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_HOSTING_ONLY") })} size="smaller" />,
      },
    ],
    sync_delivery_note: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_SYNC_DELIVERY_NOTE") })} size="smaller" />,
      },
    ],
    geo_tree: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_GEO_TREE") })} size="smaller" />,
      },
    ],
    activation_date: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_ACTIVATION_DATE") })} size="smaller" />,
      },
    ],
    price_group_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_PRICE_GROUP") })} size="smaller" />,
      },
    ],
    product_category_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_PRODUCT_CATEGORY") })} size="smaller" />,
      },
    ],
    product_class_from_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_PRODUCT_CLASS_FROM") })} size="smaller" />,
      },
    ],
    product_class_thru_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_PRODUCT_CLASS_THRU") })} size="smaller" />,
      },
    ],
    send_sync_data_to_email: [
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("MAPPING_PRINCIPAL:FIELD_SEND_SYNC_DATA_TO_EMAIL"), len: 100 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.EMAIL,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    nd6_sync_method: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_ND6_SYNC_METHOD") })} size="smaller" />,
      },
    ],
    nik: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_NIK") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NIK,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("MAPPING_PRINCIPAL:FIELD_NIK"), len: 20 })} size="smaller" />,
      },
    ],
    position: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_POSITION") })} size="smaller" />,
      },
    ],
    join_date: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_JOIN_DATE") })} size="smaller" />,
      },
    ],
    resign_date: [],
    npwp: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_NPWP") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NPWP,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT_NPWP")} size="smaller" />,
      },
    ],
    address: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_ADDRESS") })} size="smaller" />,
      },
      {
        min: 10,
        message: <Label text={translate("COMMON:MIN", { field: translate("MAPPING_PRINCIPAL:FIELD_ADDRESS"), len: 10 })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("MAPPING_PRINCIPAL:FIELD_ADDRESS"), len: 100 })} size="smaller" />,
      },
    ],
    country_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_COUNTRY") })} size="smaller" />,
      },
    ],
    district_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_DISTRICT") })} size="smaller" />,
      },
    ],
    sub_district_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_SUB_DISTRICT") })} size="smaller" />,
      },
    ],
    urban_village_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_URBAN_VILLAGE") })} size="smaller" />,
      },
    ],
    postal_code_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_POSTAL_CODE") })} size="smaller" />,
      },
    ],
    island_id: [],
    latitude: [
      {
        pattern: REGEX_CONSTANT.FLOAT,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    client_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_CLIENT_ID") })} size="smaller" />,
      },
    ],
    password: [
      {
        required: true,
        message: <span style={{ fontSize: "small" }}>{translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_PASSWORD") })}</span>,
      },
      {
        min: 8,
        message: <span style={{ fontSize: "small" }}>{translate("COMMON:MIN", { field: translate("MAPPING_PRINCIPAL:FIELD_PASSWORD"), len: 8 })}</span>,
      },
      {
        max: 20,
        message: <span style={{ fontSize: "small" }}>{translate("COMMON:MAX", { field: translate("MAPPING_PRINCIPAL:FIELD_PASSWORD"), len: 20 })}</span>,
      },
      {
        pattern: REGEX_CONSTANT.PASSWORD,
        message: <span style={{ fontSize: "small" }}>{translate("MAPPING_PRINCIPAL:ADD.VALIDATE_PASSWORD")}</span>,
      },
    ],
    first_name: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_FIRST_NAME") })} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("MAPPING_PRINCIPAL:FIELD_FIRST_NAME"), len: 20 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NAME,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    phone_country_code: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_AUTH_PHONE") })} size="smaller" />,
      },
    ],
    phone: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_PHONE") })} size="smaller" />,
      },
      {
        max: 13,
        message: <Label text={translate("COMMON:MAX", { field: translate("MAPPING_PRINCIPAL:FIELD_PHONE"), len: 13 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.PHONE_NUMBER,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    email: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_EMAIL") })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("MAPPING_PRINCIPAL:FIELD_EMAIL"), len: 100 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.EMAIL,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    group_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_GROUP") })} size="smaller" />,
      },
    ],
    role_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_ROLE") })} size="smaller" />,
      },
    ],
    locale: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_LOCALE") })} size="smaller" />,
      },
    ],
    status: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_STATUS") })} size="smaller" />,
      },
    ],
    additional_information_key: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_KEY") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INFORMATION_KEY,
        message: <Label text={translate("COMMON:VALIDATE_INFORMATION")} size="smaller" />,
      },
    ],
    additional_information_value: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("MAPPING_PRINCIPAL:FIELD_VALUE") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INFORMATION_VALUE,
        message: <Label text={translate("COMMON:VALIDATE_INFORMATION")} size="smaller" />,
      },
    ],
  };
};


// ------------------------------------------------NEXCHIEF--------------------------------------------------------------------------


export const rulesCompanyProfile = (translate) => {
  return {
    company_title_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_COMPANY_TITLE") })} size="smaller" />,
      },
    ],
    npwp: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_NPWP") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NPWP,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT_NPWP")} size="smaller" />,
      },
    ],
    name: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_NAME") })} size="smaller" />,
      },
      {
        max: 50,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_NAME"), len: 50 })} size="smaller" />,
      },
    ],
    address_1: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_ADDRESS") })} size="smaller" />,
      },
      {
        min: 10,
        message: <Label text={translate("COMMON:MIN", { field: translate("COMPANY_PROFILE:FIELD_ADDRESS"), len: 10 })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_ADDRESS"), len: 100 })} size="smaller" />,
      },
    ],
    address_2: [
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_ADDRESS"), len: 100 })} size="smaller" />,
      },
    ],
    address_3: [
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_ADDRESS"), len: 100 })} size="smaller" />,
      },
    ],
    hamlet: [
      {
        min: 3,
        message: <Label text={translate("COMMON:MIN", { field: translate("COMPANY_PROFILE:FIELD_HAMLET"), len: 3 })} size="smaller" />,
      },
      {
        max: 3,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_HAMLET"), len: 3 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INTEGER,
        message: <Label text={translate("COMMON:VALIDATE_INTEGER", { field: translate("COMPANY_PROFILE:FIELD_HAMLET") })} size="smaller" />,
      },
    ],
    neighbourhood: [
      {
        min: 3,
        message: <Label text={translate("COMMON:MIN", { field: translate("COMPANY_PROFILE:FIELD_NEIGHBOURHOOD"), len: 3 })} size="smaller" />,
      },
      {
        max: 3,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_NEIGHBOURHOOD"), len: 3 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INTEGER,
        message: <Label text={translate("COMMON:VALIDATE_INTEGER", { field: translate("COMPANY_PROFILE:FIELD_NEIGHBOURHOOD") })} size="smaller" />,
      },
    ],
    country_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_COUNTRY") })} size="smaller" />,
      },
    ],
    district_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_DISTRICT") })} size="smaller" />,
      },
    ],
    sub_district_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_SUB_DISTRICT") })} size="smaller" />,
      },
    ],
    urban_village_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_URBAN_VILLAGE") })} size="smaller" />,
      },
    ],
    postal_code_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_POSTAL_CODE") })} size="smaller" />,
      },
    ],
    island_id: [],
    latitude: [
      {
        pattern: REGEX_CONSTANT.FLOAT,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    longitude: [
      {
        pattern: REGEX_CONSTANT.FLOAT,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    phone_country_code: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: null })} size="smaller" />,
      },
      {
        max: 10,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_PHONE"), len: 10 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.PHONE_COUNTRY_CODE,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    phone: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_PHONE") })} size="smaller" />,
      },
      {
        max: 13,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_PHONE"), len: 13 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.PHONE_NUMBER,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    phone_fax_country_code: [
      {
        max: 10,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_FAX"), len: 10 })} size="smaller" />,
      },
      {
        min: 1,
        message: <Label text={translate("COMMON:MIN", { field: translate("COMPANY_PROFILE:FIELD_FAX"), len: 3 })} size="smaller" />,
      },
      {
        max: 3,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_FAX"), len: 4 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INTEGER,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    fax: [
      {
        min: 5,
        message: <Label text={translate("COMMON:MIN", { field: translate("COMPANY_PROFILE:FIELD_FAX"), len: 6 })} size="smaller" />,
      },
      {
        max: 25,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_FAX"), len: 7 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INTEGER,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    email: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_EMAIL") })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_EMAIl"), len: 100 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.EMAIL,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    alternative_email: [
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_ALTERNATIVE_EMAIL"), len: 100 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.EMAIL,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    logo: [
      {
        max: 100,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_LOGO") })} size="smaller" />,
      },
    ],
    pbf_license_number: [
      {
        max: 50,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_PBF_LICENSE_NUMBER"), len: 50 })} size="smaller" />,
      },
    ],
    pbf_trustee_sipa_number: [
      {
        max: 50,
        message: <Label text={translate("COMMON:MAX", { field: translate("COMPANY_PROFILE:FIELD_PBF_TRUSTEE_SIPA_NUMBER"), len: 50 })} size="smaller" />,
      },
    ],
    additional_information_key: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_KEY") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INFORMATION_KEY,
        message: <Label text={translate("COMMON:VALIDATE_INFORMATION")} size="smaller" />,
      },
    ],
    additional_information_value: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_VALUE") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INFORMATION_VALUE,
        message: <Label text={translate("COMMON:VALIDATE_INFORMATION")} size="smaller" />,
      },
    ],
    status: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("COMPANY_PROFILE:FIELD_STATUS") })} size="smaller" />,
      },
    ],
  };
};

export const rulesPersonProfile = (translate) => {
  return {
    person_title_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_TITLE_NAME") })} size="smaller" />,
      },
    ],
    auth_user_id: [
      {
        pattern: REGEX_CONSTANT.INTEGER,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    client_id: [],
    nik: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_NIK") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NIK,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_NIK"), len: 20 })} size="smaller" />,
      },
    ],
    npwp: [
      {
        pattern: REGEX_CONSTANT.NPWP,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT_NPWP")} size="smaller" />,
      },
    ],
    first_name: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_FIRST_NAME") })} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_FIRST_NAME"), len: 20 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NAME,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    last_name: [
      {
        max: 35,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_LAST_NAME"), len: 35 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NAME,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    sex: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_SEX") })} size="smaller" />,
      },
    ],
    address_1: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_ADDRESS_1") })} size="smaller" />,
      },
      {
        min: 10,
        message: <Label text={translate("COMMON:MIN", { field: translate("PERSON_PROFILE:FIELD_ADDRESS_1"), len: 10 })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_ADDRESS_1"), len: 100 })} size="smaller" />,
      },
    ],
    address_2: [
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_ADDRESS_2"), len: 100 })} size="smaller" />,
      },
    ],
    address_3: [
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_ADDRESS_3"), len: 100 })} size="smaller" />,
      },
    ],
    hamlet: [
      {
        min: 3,
        message: <Label text={translate("COMMON:MIN", { field: translate("PERSON_PROFILE:FIELD_HAMLET"), len: 3 })} size="smaller" />,
      },
      {
        max: 3,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_HAMLET"), len: 3 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INTEGER,
        message: <Label text={translate("COMMON:VALIDATE_INTEGER", { field: translate("PERSON_PROFILE:FIELD_HAMLET") })} size="smaller" />,
      },
    ],
    neighbourhood: [
      {
        min: 3,
        message: <Label text={translate("COMMON:MIN", { field: translate("PERSON_PROFILE:FIELD_NEIGHBOURHOOD"), len: 3 })} size="smaller" />,
      },
      {
        max: 3,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_NEIGHBOURHOOD"), len: 3 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INTEGER,
        message: <Label text={translate("COMMON:VALIDATE_INTEGER", { field: translate("PERSON_PROFILE:FIELD_NEIGHBOURHOOD") })} size="smaller" />,
      },
    ],
    country_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_COUNTRY") })} size="smaller" />,
      },
    ],
    district_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_DISTRICT") })} size="smaller" />,
      },
    ],
    urban_village_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_URBAN_VILLAGE") })} size="smaller" />,
      },
    ],
    postal_code_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_POSTAL_CODE") })} size="smaller" />,
      },
    ],
    island_id: [],
    phone_country_code: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: null })} size="smaller" />,
      },
      {
        max: 10,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_PHONE"), len: 10 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.PHONE_COUNTRY_CODE,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    phone: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_PHONE") })} size="smaller" />,
      },
      {
        max: 13,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_PHONE"), len: 13 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.PHONE_NUMBER,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    alternative_phone_country_code: [
      {
        max: 10,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_PHONE"), len: 10 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.PHONE_COUNTRY_CODE,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    alternative_phone: [
      {
        max: 13,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_ALTERNATIVE_PHONE"), len: 13 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.PHONE_NUMBER,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    email: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_EMAIL") })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_EMAIl"), len: 100 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.EMAIL,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    alternative_email: [
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_ALTERNATIVE_EMAIL"), len: 100 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.EMAIL,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    birth_place: [
      {
        max: 50,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_BIRTH_PLACE"), len: 50 })} size="smaller" />,
      },
    ],
    birth_date: [],
    occupation: [
      {
        max: 50,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_OCCUPATION"), len: 50 })} size="smaller" />,
      },
    ],
    spouse_first_name: [
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_SPOUSE_FIRST_NAME"), len: 20 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NAME,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    spouse_last_name: [
      {
        max: 35,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_SPOUSE_LAST_NAME"), len: 35 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NAME,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    spouse_birth_date: [],
    anniversary_date: [],
    education_history: [
      {
        max: 255,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_EDUCATION_HISTORY"), len: 255 })} size="smaller" />,
      },
    ],
    job_experience_history: [
      {
        max: 255,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_JOB_EXPERIENCE_HISTORY"), len: 255 })} size="smaller" />,
      },
    ],
    additional_information_key: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_KEY") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INFORMATION_KEY,
        message: <Label text={translate("COMMON:VALIDATE_INFORMATION")} size="smaller" />,
      },
    ],
    additional_information_value: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PERSON_PROFILE:FIELD_VALUE") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INFORMATION_VALUE,
        message: <Label text={translate("COMMON:VALIDATE_INFORMATION")} size="smaller" />,
      },
    ],
    remark: [
      {
        max: 255,
        message: <Label text={translate("COMMON:MAX", { field: translate("PERSON_PROFILE:FIELD_REMARK"), len: 255 })} size="smaller" />,
      },
    ],
  };
};

export const rulesDataScope = (translate) => {
  return {
    scope: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("DATA_SCOPE:FIELD_SCOPE") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.DATA_SCOPE,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    description: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("DATA_SCOPE:FIELD_DESCRIPTION") })} size="smaller" />,
      },
    ],
    status: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("DATA_SCOPE:FIELD_STATUS") })} size="smaller" />,
      },
    ],
  };
};

export const rulesRole = (translate) => {
  return {
    role_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("ROLE:FIELD_ROLE_ID") })} size="smaller" />,
      },
    ],
    description: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("ROLE:FIELD_DESCRIPTION") })} size="smaller" />,
      },
    ],
    new_role: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("ROLE:FIELD_NEW_ROLE") })} size="smaller" />,
      },
    ],
    new_description: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("ROLE:FIELD_NEW_DESCRIPTION") })} size="smaller" />,
      },
    ],
    status: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("ROLE:FIELD_STATUS") })} size="smaller" />,
      },
    ],
  };
};

export const rulesContactPerson = (translate) => {
  return {
    first_name: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("CONTACT_PERSON:FIELD_FIRST_NAME") })} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("CONTACT_PERSON:FIELD_FIRST_NAME"), len: 20 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NAME,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    last_name: [
      {
        max: 35,
        message: <Label text={translate("COMMON:MAX", { field: translate("CONTACT_PERSON:FIELD_LAST_NAME"), len: 35 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NAME,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    nik: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("CONTACT_PERSON:FIELD_NIK") })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.NIK,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("CONTACT_PERSON:FIELD_NIK"), len: 20 })} size="smaller" />,
      },
    ],
    person_profile_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("CONTACT_PERSON:FIELD_PERSON_PROFILE") })} size="smaller" />,
      },
    ],
    connector: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("CONTACT_PERSON:FIELD_CONNECTOR") })} size="smaller" />,
      },
    ],
    group_of_distributor: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("CONTACT_PERSON:OPTION_GROUP_OF_DISTRIBUTOR") })} size="smaller" />,
      },
    ],
    principal: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("CONTACT_PERSON:OPTION_PRINCIPAL") })} size="smaller" />,
      },
    ],
    position: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("CONTACT_PERSON:FIELD_POSITION") })} size="smaller" />,
      },
    ],
    phone_code: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: null })} size="smaller" />,
      },
      {
        max: 10,
        message: <Label text={translate("COMMON:MAX", { field: translate("CONTACT_PERSON:FIELD_PHONE"), len: 10 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INTEGER,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    phone: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("CONTACT_PERSON:FIELD_PHONE") })} size="smaller" />,
      },
      {
        max: 13,
        message: <Label text={translate("COMMON:MAX", { field: translate("CONTACT_PERSON:FIELD_PHONE"), len: 13 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.INTEGER,
        message: <Label text={translate("COMMON:NOT_ALLOWED")} size="smaller" />,
      },
    ],
    email: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("CONTACT_PERSON:FIELD_EMAIL") })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("CONTACT_PERSON:FIELD_EMAIl"), len: 100 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.EMAIL,
        message: <Label text={translate("COMMON:VALIDATE_FORMAT")} size="smaller" />,
      },
    ],
    join_date: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("CONTACT_PERSON:FIELD_JOIN_DATE") })} size="smaller" />,
      },
    ],
    resign_date: [],
    superior_id: [],
  };
};

export const rulesDataGroup = (translate) => {
  return {
    group_id: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("DATA_GROUP:FIELD_GROUP_ID") })} size="smaller" />,
      },
    ],
    description: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("DATA_GROUP:FIELD_DESCRIPTION") })} size="smaller" />,
      },
    ],
    status: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("DATA_GROUP:FIELD_STATUS") })} size="smaller" />,
      },
    ],
  };
};

export const changepassword = (translate) => {
  return {
    ID: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_USER_ID") })} size="smaller" />,
      },
    ],
    PASSWORD: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_PASSWORD") })} size="smaller" />,
      },
      {
        min: 8,
        message: <Label text={translate("COMMON:MIN", { field: translate("USER:FIELD_PASSWORD"), len: 8 })} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("USER:FIELD_PASSWORD"), len: 20 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.PASSWORD,
        message: <Label text={translate("USER:ADD.VALIDATE_PASSWORD")} size="smaller" />,
      },
    ],
    CONFIRM_PASSWORD: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_CONFIRM_PASSWORD") })} size="smaller" />,
      },
      ({ getFieldValue }) => ({
        validator(rule, value) {
          if (!value || getFieldValue("new_password") === value) {
            return Promise.resolve();
          }

          return Promise.reject(<Label text={translate("USER:HELP_NOT_MATCH")} size="smaller" />);
        },
      }),
    ],
    OLD_PASSWORD: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_OLD_PASSWORD") })} size="smaller" />,
      },
    ],
  };
};

export const resetPassword = (translate) => {
  return {
    CODE: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_CODE") })} size="smaller" />,
      },
    ],
    PASSWORD: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_PASSWORD") })} size="smaller" />,
      },
      {
        min: 8,
        message: <Label text={translate("COMMON:MIN", { field: translate("USER:FIELD_PASSWORD"), len: 8 })} size="smaller" />,
      },
      {
        max: 20,
        message: <Label text={translate("COMMON:MAX", { field: translate("USER:FIELD_PASSWORD"), len: 20 })} size="smaller" />,
      },
      {
        pattern: REGEX_CONSTANT.PASSWORD,
        message: <Label text={translate("USER:ADD.VALIDATE_PASSWORD")} size="smaller" />,
      },
    ],
    CONFIRM_PASSWORD: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:FIELD_CONFIRM_PASSWORD") })} size="smaller" />,
      },
      ({ getFieldValue }) => ({
        validator(rule, value) {
          if (!value || getFieldValue("new_password") === value) {
            return Promise.resolve();
          }

          return Promise.reject(<Label text={translate("USER:HELP_NOT_MATCH")} size="smaller" />);
        },
      }),
    ],
  };
};

export const rulesParameter = (translate) => {
  return {
    code: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_CODE") })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("PARAMETER:FIELD_CODE"), len: 100 })} size="smaller" />,
      },
    ],
    name: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_NAME") })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("PARAMETER:FIELD_NAME"), len: 100 })} size="smaller" />,
      },
    ],
    en_name: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_EN_NAME") })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("PARAMETER:FIELD_EN_NAME"), len: 100 })} size="smaller" />,
      },
    ],
    group: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_PARAMETER_GROUP") })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("PARAMETER:FIELD_PARAMETER_GROUP"), len: 100 })} size="smaller" />,
      },
    ],
    sequence: [
      {
        required: false,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_SEQUENCE") })} size="smaller" />,
      },
    ],
    level: [
      {
        required: false,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_LEVEL") })} size="smaller" />,
      },
    ],
    type: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_TYPE") })} size="smaller" />,
      },
    ],
    length: [
      {
        required: false,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_LENGTH") })} size="smaller" />,
      },
    ],
    min_value: [
      {
        required: false,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_MIN_VALUE") })} size="smaller" />,
      },
    ],
    max_value: [
      {
        required: false,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_MAX_VALUE") })} size="smaller" />,
      },
    ],
    default_value: [
      {
        required: false,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_DEFAULT_VALUE") })} size="smaller" />,
      },
      {
        max: 100,
        message: <Label text={translate("COMMON:MAX", { field: translate("PARAMETER:FIELD_DEFAULT_VALUE"), len: 100 })} size="smaller" />,
      },
    ],
    select_list_value: [
      {
        required: false,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_SELECT_LIST_VALUE") })} size="smaller" />,
      },
    ],
    select_list_name: [
      {
        required: false,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_SELECT_LIST_NAME") })} size="smaller" />,
      },
    ],
    description: [
      {
        required: false,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_DESCRIPTION") })} size="smaller" />,
      },
    ],
    status: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("PARAMETER:FIELD_STATUS") })} size="smaller" />,
      },
    ],
  };
};

export const rulesCheckUser = (translate) => {
  return {
    data: [
      {
        required: true,
        message: <Label text={translate("COMMON:REQUIRE", { field: translate("USER:CHECK_USER.DATA") })} size="smaller" />,
      },
    ],
  };
};
