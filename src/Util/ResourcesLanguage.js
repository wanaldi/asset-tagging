import CommonEnglish from "../Locale/Common/en.json";
import CompanyProfileEnglish from "../Locale/Admin/CompanyProfile/en.json";
import ContactPersonEnglish from "../Locale/Admin/ContactPerson/en.json";
import PersonProfileEnglish from "../Locale/Admin/PersonProfile/en.json";
import RoleEnglish from "../Locale/Admin/Role/en.json";
import DataGroupEnglish from "../Locale/Admin/DataGroup/en.json";
import DataScopeEnglish from "../Locale/Admin/DataScope/en.json";
import UserEnglish from "../Locale/Admin/User/en.json";
import JobProcessEnglish from "../Locale/Admin/JobProcess/en.json";
import AuditEnglish from "../Locale/Admin/Audit/en.json";
import ParameterEnglish from "../Locale/Admin/Parameter/en.json";
import NexchiefAccountEnglish from "../Locale/Admin/NexchiefAccount/en.json";
import MappingNexsellerEnglish from "../Locale/Admin/MappingNexseller/en.json";
import MappingPrincipalEnglish from "../Locale/Admin/MappingPrincipal/en.json";

import CommonIndonesian from "../Locale/Common/id.json";
import CompanyProfileIndonesian from "../Locale/Admin/CompanyProfile/id.json";
import ContactPersonIndonesian from "../Locale/Admin/ContactPerson/id.json";
import PersonProfileIndonesian from "../Locale/Admin/PersonProfile/id.json";
import RoleIndonesian from "../Locale/Admin/Role/id.json";
import DataGroupIndonesian from "../Locale/Admin/DataGroup/id.json";
import DataScopeIndonesian from "../Locale/Admin/DataScope/id.json";
import UserIndonesian from "../Locale/Admin/User/id.json";
import JobProcessIndonesian from "../Locale/Admin/JobProcess/id.json";
import AuditIndonesian from "../Locale/Admin/Audit/id.json";
import ParameterIndonesian from "../Locale/Admin/Parameter/id.json";
import NexchiefAccountIndonesian from "../Locale/Admin/NexchiefAccount/id.json";
import MappingNexsellerIndonesian from "../Locale/Admin/MappingNexseller/id.json";
import MappingPrincipalIndonesian from "../Locale/Admin/MappingPrincipal/id.json";

export const resourceLanguage = {
  en: {
    COMMON: CommonEnglish,
    COMPANY_PROFILE: CompanyProfileEnglish,
    CONTACT_PERSON: ContactPersonEnglish,
    PERSON_PROFILE: PersonProfileEnglish,
    ROLE: RoleEnglish,
    DATA_GROUP: DataGroupEnglish,
    DATA_SCOPE: DataScopeEnglish,
    USER: UserEnglish,
    JOB_PROCESS: JobProcessEnglish,
    AUDIT: AuditEnglish,
    PARAMETER: ParameterEnglish,
    NEXCHIEF_ACCOUNT: NexchiefAccountEnglish,
    MAPPING_NEXSELLER: MappingNexsellerEnglish,
    MAPPING_PRINCIPAL: MappingPrincipalEnglish,
  },
  id: {
    COMMON: CommonIndonesian,
    COMPANY_PROFILE: CompanyProfileIndonesian,
    CONTACT_PERSON: ContactPersonIndonesian,
    PERSON_PROFILE: PersonProfileIndonesian,
    ROLE: RoleIndonesian,
    DATA_GROUP: DataGroupIndonesian,
    DATA_SCOPE: DataScopeIndonesian,
    USER: UserIndonesian,
    JOB_PROCESS: JobProcessIndonesian,
    AUDIT: AuditIndonesian,
    PARAMETER: ParameterIndonesian,
    NEXCHIEF_ACCOUNT: NexchiefAccountIndonesian,
    MAPPING_NEXSELLER: MappingNexsellerIndonesian,
    MAPPING_PRINCIPAL: MappingPrincipalIndonesian,
  },
};
