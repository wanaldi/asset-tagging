import i18next from "i18next";

export const resourcePagination = (state, onPaging) => {
  return {
    position: ["topRight"],
    size: "small",
    current: state.page,
    defaultCurrent: state.page,
    pageSize: state.limit,
    total: state.initiate_list && state.initiate_list.count_data,
    pageSizeOptions: state.initiate_list && state.initiate_list.valid_limit,
    showSizeChanger: true,
    onChange: onPaging,
    onShowSizeChange: onPaging,
    showTotal: (total, range) => i18next.t("COMMON:COUNT_DATA", { range: `${range[0]}-${range[1]}`, total }),
  };
};
