export const resourceRowSelection = (state, props, selectRow) => {
  return {
    preserveSelectedRowKeys: true,
    selectedRowKeys: state.selectedRowKeys,
    onChange: (selectedRowKeys, selectedRows) => {
      selectRow(selectedRowKeys, selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: props.permission.delete ? false : props.permission.delete_own && record.created_by !== props.user,
    }),
  };
};
