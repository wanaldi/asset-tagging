const serviceWorker = () => {
  let serviceWorkerURL = `${process.env.PUBLIC_URL}/sw.js`;

  if ("serviceWorker" in navigator) {
    navigator.serviceWorker
      .register(serviceWorkerURL)
      .then()
      .catch((error) => console.error(error));
  }
};

export default serviceWorker;
