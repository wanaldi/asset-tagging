import * as ActionType from "../Action/ActionTypes";

const initial_state = {
  table_loading: false,
  skeleton_loading: false,
  post_loading: false,
  auth_loading: false,
  prepare_component: false,

  openKeys: [],
  selectedKeys: [],
  lastPage: null,
};

const CommonReducer = (state = initial_state, { type, payload }) => {
  switch (type) {
    case ActionType.TABLE_REST_FETCHING:
      return { ...state, table_loading: true };

    case ActionType.TABLE_REST_SUCCESS:
      return { ...state, table_loading: false };

    case ActionType.TABLE_REST_ERROR:
      return { ...state, table_loading: false };

    case ActionType.SKELETON_REST_FETCHING:
      return { ...state, skeleton_loading: true };

    case ActionType.SKELETON_REST_SUCCESS:
      return { ...state, skeleton_loading: false };

    case ActionType.SKELETON_REST_ERROR:
      return { ...state, skeleton_loading: false };

    case ActionType.POST_REST_FETCHING:
      return { ...state, post_loading: true };

    case ActionType.POST_REST_SUCCESS:
      return { ...state, post_loading: false };

    case ActionType.POST_REST_ERROR:
      return { ...state, post_loading: false };

    case ActionType.AUTH_REST_FETCHING:
      return { ...state, auth_loading: true };

    case ActionType.AUTH_REST_SUCCESS:
      return { ...state, auth_loading: false };

    case ActionType.AUTH_REST_ERROR:
      return { ...state, auth_loading: false };

    case ActionType.PREPARE_COMPONENT:
      return { ...state, prepare_component: payload };

    case ActionType.OPEN_KEYS:
      return { ...state, openKeys: payload };

    case ActionType.SELECTED_KEYS:
      return { ...state, selectedKeys: payload };

    case ActionType.OPEN_PAGE:
      return { ...state, lastPage: payload };

    default:
      return state;
  }
};

export default CommonReducer;
