import * as ActionType from "../Action/ActionTypes";
import i18next from "i18next";

const initial_state = {
  crypto_random: null,
  code_pkce: null,
  token: null,
  refresh_token: null,
  isAuthenticated: false,
  user: null,

  first_name: null,
  last_name: null,
  role: null,

  language: null,
  scope: null,
  list_menu: null,
  list_permission: null,
};

const AuthenticationReducer = (state = initial_state, { type, payload }) => {
  switch (type) {
    case ActionType.SET_CRYPTO_RANDOM:
      return { ...state, crypto_random: payload };

    case ActionType.SET_CODE_PKCE:
      return { ...state, code_pkce: payload };

    case ActionType.SET_TOKEN:
      return { ...state, token: payload };

    case ActionType.SET_REFRESH_TOKEN:
      return { ...state, refresh_token: payload };

    case ActionType.TRUE_AUTHENTICATED:
      return { ...state, isAuthenticated: true };

    case ActionType.FALSE_AUTHENTICATED:
      return { ...state, isAuthenticated: false };

    case ActionType.SET_FIRST_NAME:
      return { ...state, first_name: payload };

    case ActionType.SET_LAST_NAME:
      return { ...state, last_name: payload };

    case ActionType.SET_ROLE:
      return { ...state, role: payload };

    case ActionType.SET_USER:
      return { ...state, user: payload };

    case ActionType.SET_LANGUAGE:
      i18next.changeLanguage(payload);
      return { ...state, language: payload };

    case ActionType.SET_SCOPE:
      return { ...state, scope: payload };

    case ActionType.SET_MENU:
      return { ...state, list_menu: payload };

    case ActionType.SET_PERMISSION:
      return { ...state, list_permission: payload };

    case ActionType.CLEAR_PROPERTIES:
      return { ...state, crypto_random: initial_state.crypto_random, code_pkce: initial_state.code_pkce };

    default:
      return state;
  }
};

export default AuthenticationReducer;
