import { combineReducers } from "redux";
import * as ActionType from "../Action/ActionTypes";
import AuthenticationReducer from "./AuthenticationReducer";
import CommonReducer from "./CommonReducer";
import PermissionReducer from "./PermissionReducer";

const appReducer = combineReducers({
  authentication: AuthenticationReducer,
  permission: PermissionReducer,
  common: CommonReducer,
});

const rootReducer = (state, action) => {
  if (action.type === ActionType.RESET_STORE) {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;
