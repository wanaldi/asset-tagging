import * as ActionType from "../Action/ActionTypes";

const initial_state = {
  home: null,
  client_user: null,
  nexchief_account: null,
  mapping_nexseller: null,
  // ---------------
  company_profile: null,
  contact_person: null,
  person_profile: null,
  role: null,
  data_group: null,
  data_scope: null,
  // nexsoft_user: null,
  job_process: null,
  parameter: null,
};

const PermissionReducer = (state = initial_state, { type, payload }) => {
  switch (type) {
    case ActionType.PERMISSION_HOME:
      return { ...state, home: payload };
    
    case ActionType.PERMISSION_CLIENT_USER:
      return { ...state, user: payload };
    
    case ActionType.PERMISSION_NEXCHIEF_ACCOUNT:
      return { ...state, nexchief_account: payload };
    
    case ActionType.PERMISSION_MAPPING_NEXSELLER:
      return { ...state, mapping_nexseller: payload };
    
    case ActionType.PERMISSION_MAPPING_PRINCIPAL:
      return { ...state, mapping_principal: payload };
    
    case ActionType.PERMISSION_DATA_GROUP:
      return { ...state, data_group: payload };
    
    // -----------------------------------------

    case ActionType.PERMISSION_COMPANY_PROFILE:
      return { ...state, company_profile: payload };

    case ActionType.PERMISSION_CONTACT_PERSON:
      return { ...state, contact_person: payload };

    case ActionType.PERMISSION_PERSON_PROFILE:
      return { ...state, person_profile: payload };

    case ActionType.PERMISSION_ROLE:
      return { ...state, role: payload };

    case ActionType.PERMISSION_DATA_SCOPE:
      return { ...state, data_scope: payload };
    
    // case ActionType.PERMISSION_NEXSOFT_USER:
    //   return { ...state, user: payload };

    case ActionType.PERMISSION_PARAMETER:
      return { ...state, parameter: payload };

    default:
      return state;
  }
};

export default PermissionReducer;
