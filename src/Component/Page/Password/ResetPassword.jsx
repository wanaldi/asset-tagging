import { Button, Form, Input, Result } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { ROUTER_CONSTANT } from "../../../Enum/RouterConstant";
import { doResetPassword } from "../../../Service/Admin/UserService";
import { resetPassword } from "../../../Util/FormRules";
import Label from "../../Common/Label/Label";
import style from "./ForgetPassword.module.css";
import { ReactComponent as ReactLogo } from '../../../Asset/nc-dummy-logo.svg';

class ResetPassword extends Component {
  _formRef = React.createRef();
  state = {
    isSuccess: false,
  };

  componentDidMount() {
    // if (this.props.location.state) {
    //   if (this.props.location.state.data.email) {
    //     this._formRef.current.setFieldsValue({
    //       data: this.props.location.state.data.email,
    //     });
    //   }

    //   // if (this.props.location.state.data.phone) {
    //   //   this._formRef.current.setFieldsValue({
    //   //     data: this.props.location.state.data.phone,
    //   //   });
    //   // }
    // } else {
    //   this.props.history.push(ROUTER_CONSTANT.FORGET_PASSWORD);
    // }
  }

  _onFinish = async (value, type) => {
    let data = {};

    if (type === "email") {
      data = {
        email: value.data,
        // forget_code: value.forget_code,
        forget_code: this.props.match.params.code,
        new_password: value.new_password,
        verify_new_password: value.verify_new_password,
      };
    } else {
      // data = {
      //   phone: value.data,
      //   forget_code: value.forget_code,
      //   new_password: value.new_password,
      //   verify_new_password: value.verify_new_password,
      // };
    }

    const isSuccess = await doResetPassword("X-NEXTOKEN", type, "AuthServer2020", data, this.props.dispatch);

    if (isSuccess) {
      this.setState({ isSuccess: true });
    }

    console.log(isSuccess);
  };

  render() {
    const rules = resetPassword(this.props.t);
    return (
      <div className={style.Container}>
        <div className={style.Wrapper}>
          <div className={style.Body}>
            <div className={style.Logo}>
              <ReactLogo />
            </div>
            <div className={style.Title}>{this.props.t("COMMON:RESET_PASSWORD")}</div>
            <div className={style.Content}>
              {this.state.isSuccess ? (
                <Result
                  status="success"
                  title={this.props.t("COMMON:SUCCESS_RESET_PASSWORD")}
                  subTitle={<Link to={ROUTER_CONSTANT.LOGIN}>{this.props.t("COMMON:LOGIN")}</Link>}
                />
              ) : (
                <Form layout="vertical" ref={this._formRef} onFinish={(value) => this._onFinish(value, this.props.location.state.type)}>
                  {/* <Form.Item
                    name="data"
                    label={
                      this.props.history.location.state && this.props.history.location.state.type === "email" ? (
                        <Label text={this.props.t("COMMON:FIELD_EMAIL")} />
                      ) : (
                        <Label text={this.props.t("COMMON:FIELD_PHONE")} />
                      )
                    }
                  >
                    <Input disabled={this.props.location.state} />
                  </Form.Item> */}
                  {/* <Form.Item name="forget_code" rules={rules.CODE} label={this.props.t("USER:FIELD_CODE")}>
                    <Input placeholder={this.props.t("USER:VERIFICATION.TYPE_CODE")} />
                  </Form.Item> */}

                  <p style={{ fontSize: "12px", color: "#999999", marginBottom: "0px" }}>{this.props.t("USER:FIELD_PASSWORD")}</p>
                    <Form.Item name="new_password" rules={rules.PASSWORD} style={{ marginBottom: "0px"}}>
                      <Input.Password className={style.Input} placeholder={this.props.t("USER:HELP_PASSWORD")} autoComplete="new-password" />
                    </Form.Item>
                    <div>
                      <p>{this.props.t("COMMON:PASSWORD_MIN_CHAR")}<br></br>{this.props.t("COMMON:PASSWORD_CONTAIN")}</p>
                    </div>

                    <p style={{ fontSize: "12px", color: "#999999", marginBottom: "0px" }}>{this.props.t("USER:FIELD_CONFIRM_PASSWORD")}</p>
                    <Form.Item name="verify_new_password" rules={rules.CONFIRM_PASSWORD} style={{ marginBottom: "0px"}}>
                      <Input.Password className={style.Input} placeholder={this.props.t("USER:HELP_CONFIRM_PASSWORD")} />
                    </Form.Item>
                    <div>
                      <p>{this.props.t("COMMON:PASSWORD_MIN_CHAR")}<br></br>{this.props.t("COMMON:PASSWORD_CONTAIN")}</p>
                    </div>

                    <Form.Item>
                      <Button type="primary" htmlType="submit" style={{ width: "100%", backgroundColor: "#3399FF", borderColor: "#3399FF", marginTop: "30px" }} disabled={this.props.auth_loading}>
                        {this.props.t("COMMON:SET_NEW_PASSWORD")}
                      </Button>
                    </Form.Item>

                  {/* <Form.Item>
                    <Button type="primary" htmlType="submit" style={{ backgroundColor: "#2DC0F4", borderColor: "#2DC0F4" }}>
                      {this.props.t("COMMON:BUTTON_SEND")}
                    </Button>
                    <Button style={{ marginLeft: 4 }} onClick={() => this.props.history.push(ROUTER_CONSTANT.LOGIN)}>
                      {this.props.t("COMMON:BUTTON_CANCEL")}
                    </Button>
                  </Form.Item> */}
                </Form>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { language: state.authentication.language };
};

const mapDispatchToProps = (dispatch) => {
  return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(ResetPassword));
