import { Button, Form, Input } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { X_NEXTOKEN } from "../../../Enum/PropertiesConstant";
import { ROUTER_CONSTANT } from "../../../Enum/RouterConstant";
import { doForgetPassword } from "../../../Service/Admin/UserService";
import Label from "../../Common/Label/Label";
import style from "./ForgetPassword.module.css";
import { ReactComponent as ReactLogo } from '../../../Asset/nc-dummy-logo.svg';

class ForgetPassword extends Component {
  state = {
    type: "email",
  };

  _onFinish = async (value, type) => {
    let data = {};

    if (type === "email") {
      data = {
        email: value.email,
      };
    } else {
      // data = {
      //   phone: `+${value.country_code}-${value.phone}`,
      // };
    }

    console.log("data", data);
    await doForgetPassword("X-NEXTOKEN", type, X_NEXTOKEN, data, this.props.dispatch, () =>
      this.props.history.push({ pathname: ROUTER_CONSTANT.RESET_PASSWORD, state: { type, data } })
    );
  };

  render() {
    return (
      <div className={style.Container}>
        <div className={style.Wrapper}>
          <div className={style.Body}>
            <div className={style.Logo}>
              <ReactLogo />
            </div>
            {/* <div className={style.Title}>{this.props.t("COMMON:RESET_PASSWORD")}</div>
            <div className={style.Paragraph}>Enter your email address and we'll send you <br/> a link to reset your password.</div>
            <div className={style.Content}>
              <p style={{fontSize:"12px", color:"#999999"}}>{this.props.t("COMMON:FIELD_EMAIL")}</p>
              <Form layout="vertical" ref={this._formRef} onFinish={(value) => this._onFinish(value, this.state.type)}>
                  <Form.Item
                    name="email"
                    rules={[
                      {
                        required: true,
                        message: <Label text={this.props.t("COMMON:REQUIRE", { field: this.props.t("USER:FIELD_EMAIL") })} size="smaller" />,
                      },
                    ]}
                  >
                  <Input className={style.Input} placeholder="email@email.com" />
                  </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit" style={{ width: "100%", backgroundColor: "#3399FF", borderColor: "#3399FF", marginTop:"40px" }} disabled={this.props.auth_loading}>
                    {this.props.t("COMMON:RESET_PASSWORD")}
                  </Button>
                </Form.Item>
              </Form>
            </div> */}
            <div className={style.Title}>{this.props.t("COMMON:RESET_PASSWORD")}</div>
            <div className={style.Paragraph}>Enter your email address and we'll send you <br /> a link to reset your password.</div>
            <div className={style.Content}>
              <p style={{ fontSize: "12px", color: "#999999" }}>{this.props.t("COMMON:FIELD_EMAIL")}</p>
              <Form layout="vertical" ref={this._formRef} onFinish={(value) => this._onFinish(value, this.state.type)}>
                <Form.Item
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: <Label text={this.props.t("COMMON:REQUIRE", { field: this.props.t("USER:FIELD_EMAIL") })} size="smaller" />,
                    },
                  ]}
                >
                  <Input className={style.Input} placeholder="email@email.com" />
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit" style={{ width: "100%", backgroundColor: "#3399FF", borderColor: "#3399FF", marginTop: "40px" }} disabled={this.props.auth_loading}>
                    {this.props.t("COMMON:RESET_PASSWORD")}
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { language: state.authentication.language };
};

const mapDispatchToProps = (dispatch) => {
  return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(ForgetPassword));
