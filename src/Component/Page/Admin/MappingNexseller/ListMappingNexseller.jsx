import React, { Component } from 'react';
import ButtonCollapseList from '../../../Common/Button/ButtonCollapseList';
import style from "./ListMappingNexseller.module.css";
import ButtonAdd from "../../../Common/Button/ButtonAdd";
import { propTable, RANDOM_ID } from "../../../../Enum/PropertiesConstant";
import { ROUTER_CONSTANT } from "../../../../Enum/RouterConstant";
import { withTranslation } from "react-i18next";
import * as MappingNexsellerService from "../../../../Service/Admin/MappingNexsellerService";
import { message, Pagination } from 'antd';
import { connect } from 'react-redux';
import { stateRedux } from '../../../../Util/ReduxState';
import ModalNPWP from '../../../Common/Modal/ModalNPWP';
import Filter from '../../../Common/Filter/Filter';
import * as NexchiefAccountService from "../../../../Service/Admin/NexchiefAccountService";
import moment from 'moment';
import { Link } from 'react-router-dom';

class ListMappingNexseller extends Component {
    state = {
        // collapsed: [true, true, true],
        collapsed: [],
        ...propTable,
        initiate_list: null,
        initiate_list_nexchief_account: null,
        list: [],
        list_nexchief_account: [],
        modalOpen: false,
    };

    async componentDidMount() {
        await this._getNexchiefAccount(this.state.page, this.state.limit, this.state.order, this.state.filter);
    }

    _getNexchiefAccount = async (page, limit, order, filter) => {
        const { token, refresh_token, dispatch } = this.props;

        if (this.props.permission.view || this.props.permission.view_own) {
            const response = await NexchiefAccountService.doGetList(token, refresh_token, page, limit, order, filter, dispatch);
            console.log("responsenc", response);
            
            if (response) this.setState({ initiate_list_nexchief_account: response.initiate_list, list_nexchief_account: response.list }, () => {
                if (this.state.list_nexchief_account !== null) {
                    let temp = [];
                    for (let index = 0; index < this.state.list_nexchief_account.length; index++) {
                        temp.push(true);
                    }
                    this.setState({ collapsed: temp });
                }
            })
        }
    };

    _getData = async (page, limit, order, filter) => {
        const { token, refresh_token, dispatch } = this.props;

        if (this.props.permission.view || this.props.permission.view_own) {
            console.log("tes");
            const response = await MappingNexsellerService.doGetList(token, refresh_token, page, limit, order, filter, dispatch);
            if (response) this.setState({ initiate_list: response.initiate_list, list: response.list });
            console.log("list", this.state.list);
        }
    };

    _resetTable = () => {
        const { page, limit, search_by, operator, search_key, filter, order, order_by, order_params } = propTable;
        this.setState({ page, limit, search_by, operator, search_key, filter, order, order_by, order_params }, () =>
            this._getNexchiefAccount(this.state.page, this.state.limit, this.state.order, this.state.filter)
        );
    };

    _onChange = (key, value) => {
        this.setState({ [key]: value });
    };

    _onPaging = (page, limit) => {
        this.setState({ page, limit }, () => this._getData(this.state.page, this.state.limit, this.state.order, this.state.filter));
    };

    _onOrder = (order_by, order_params) => {
        this.setState({ order_by, order_params }, () => {
            if (order_by && order_params) {
                this.setState({ order: `${order_by} ${order_params}` }, () => this._getData(this.state.page, this.state.limit, this.state.order, this.state.filter));
            } else {
                this._resetTable();
                message.error({ content: this.props.t("COMMON:REQUIRE_SEARCH") });
            }
        });
    };

    _onFilter = (value) => {
        console.log("value", value.target.innerText);
        console.log("onfilter");
        // const { search_by, operator, search_key } = this.state;

        if (value) {
            // const filter = 'name like ' + value.target.innerText + ', code like ' + value.target.innerText;
            const filter = 'name like ' + value.target.innerText;
            this.setState({ filter }, () => this._getData(propTable.page, propTable.limit, propTable.order, filter));
        } else {
            this._resetTable();
            message.error({ content: this.props.t("COMMON:REQUIRE_SEARCH") });
        }
    };

    _onChangePageSizeOptions = (event) => {
        this.setState({ limit: event.target.value }, () => {
            this._onPaging(this.state.page, this.state.limit);
        });
    }

    _openModal = () => {
        this.setState({ modalOpen: true });
    }

    _closeModal = () => {
        this.setState({ modalOpen: false });
    }

    _onCollapse = async (i, id) => {
        this.setState(state => {
            const collapsed = state.collapsed.map((item, index) => {
                if (index === i) {
                    return !item;
                } else {
                    return true;
                }
            });

            return {
                collapsed,
            };
        });

        await this._getData(this.state.page, this.state.limit, this.state.order, "nexchief_account_id eq " + id);
    };

    render() {

        const ModalNPWP2 = () => (
            <div>
                <ModalNPWP
                    key={RANDOM_ID(8)}
                    modalOpen={this.state.modalOpen}
                    closeModal={() => this._closeModal()}
                    router={ROUTER_CONSTANT.MAPPING_NEXSELLER.ADD}
                    title={this.props.t("MAPPING_NEXSELLER:TITLE")}
                    labelNPWP={this.props.t("MAPPING_NEXSELLER:FIELD_NPWP")}
                />
            </div>
        )

        return (
            <div style={{ marginBottom: "20px", paddingLeft: "55px", paddingRight: "55px", paddingBottom: "55px", position: "absolute", width: "100%" }}>
                <div className={style.Title}>
                    <div style={{ marginBottom: "10px" }}>{this.props.t("MAPPING_NEXSELLER:TITLE")}</div>
                    <div className={style.Filter} style={{ display: "contents" }}>
                        <Filter
                            {...this.props}
                            state={this.state}
                            permission={this.props.permission}
                            initiateList={this.state.initiate_list}
                            function={{
                                onOrder: this._onOrder,
                                onChange: this._onChange,
                                onReset: this._resetTable,
                            }}
                            namespace="mappingnexseller"
                            placeholder="Search code or Nexseller"
                            changeFilter={this._onFilter}
                        />
                    </div>
                    <ButtonAdd
                        key={RANDOM_ID(8)}
                        text={this.props.t("MAPPING_NEXSELLER:TITLE")}
                        disabled={this.props.permission.insert ? false : !this.props.permission.insert_own}
                        openModal={this._openModal}
                        router=""
                    />
                    <ModalNPWP2 />
                </div>

                <ul style={{ listStyleType: "none", width: "100%", paddingInlineStart: "0px" }}>
                    {this.state.list_nexchief_account == null && "Tidak Ada Data"}
                    {this.state.list_nexchief_account !== null && this.state.list_nexchief_account.map((item, index) => (
                        <li key={index}>
                            <div style={{ marginBottom: "20px" }}>
                                <div className={style.List}>
                                    <div style={{ display: "flex", alignItems: "center", marginTop: "3px", marginBottom: "auto" }}>
                                        <div style={{ marginRight: "10px", textAlign: "right" }}>
                                            <div className={style.Id}>{item.code}</div>
                                            <div className={style.NexchiefAccount}>{item.company_profile_name}</div>
                                        </div>
                                        <div style={{ marginTop: "auto", marginLeft: "15px" }}>
                                            <button className={style.ButtonStatus}>{item.deleted ? "NonActive" : "Active"}</button>
                                        </div>
                                        <ButtonCollapseList collapsed={this.state.collapsed[index]} onCollapse={() => this._onCollapse(index, item.id)} />
                                    </div>
                                </div>
                                <div className={style.ListBody} style={this.state.collapsed[index] ? { height: "0px" } : { height: "fit-content", borderTop: "inset rgb(0 0 0 / 12%)", borderWidth: "thin", padding: "20px" }}>
                                    <div className={style.HeadTable}>
                                        <div className={style.GridContainerHead}>
                                            <div className={style.GridItemHead}>{this.props.t("MAPPING_NEXSELLER:FIELD_ID")}</div>
                                            <div className={style.GridItemHead}>{this.props.t("MAPPING_NEXSELLER:FIELD_NEXSELLER_NAME")}</div>
                                            <div className={style.GridItemHead}>{this.props.t("MAPPING_NEXSELLER:FIELD_JOIN_DATE")}</div>
                                            <div className={style.GridItemHead}>{this.props.t("MAPPING_NEXSELLER:FIELD_STATUS")}</div>
                                        </div>
                                    </div>

                                    {this.state.list == null && "Tidak Ada Data"}
                                    {this.state.list !== null && this.state.list.map((item2, index2) => (
                                        <div key={index2} className={style.HeadTable}>
                                            <div className={style.GridContainer}>
                                                <div className={style.GridItem}>{item2.code}</div>
                                                <div className={style.GridItem}>{item2.company_name}</div>
                                                <div className={style.GridItem}>{moment(item2.activation_date).format(this.props.language === "id-ID" ? 'LL' : 'MMMM Do YYYY')}</div>
                                                <div className={style.GridItemStatus}>{item2.deleted ? "NonActive" : "Active"}</div>
                                                <div className={style.GridItem} style={{ justifyContent: "right" }}>
                                                    <Link to={ROUTER_CONSTANT.MAPPING_NEXSELLER.DETAIL + "/" + item2.id}>
                                                        <div className={style.LinkViewDetail}>
                                                            View Detail
                                                        </div>
                                                    </Link>

                                                </div>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>

                <div style={{ display: "flex" }}>
                    <div style={{ marginRight: "5px", textAlign: "right", width: "100%" }}>Rows Per Page</div>

                    <select id="changePageOptions" onChange={this._onChangePageSizeOptions} style={{ marginLeft: "auto" }} defaultValue="10">
                        {this.state.initiate_list_nexchief_account && this.state.initiate_list_nexchief_account.valid_limit.map((item, index) => (
                            <option key={index} value={this.state.initiate_list_nexchief_account && this.state.initiate_list_nexchief_account.valid_limit[index]}>{this.state.initiate_list_nexchief_account && this.state.initiate_list_nexchief_account.valid_limit[index]}</option>
                        ))}

                    </select>

                    <div style={{ marginRight: "0px", display: "contents" }}>
                        <Pagination
                            size="small"
                            current={this.state.page}
                            defaultCurrent={this.state.page}
                            pageSize={this.state.limit}
                            total={this.state.initiate_list_nexchief_account && this.state.initiate_list_nexchief_account.count_data}
                            onChange={this._onPaging}
                            style={{ display: "contents" }}
                        />
                    </div>

                </div>

            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        ...stateRedux(state, "mapping_nexseller"),
        language: state.authentication.language
    };

};

const mapDispatchToProps = (dispatch) => {
    return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(ListMappingNexseller));