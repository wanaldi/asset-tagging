import React, { Component } from 'react';
import style from "./DetailMappingNexseller.module.css";
import { ROUTER_CONSTANT } from "../../../../Enum/RouterConstant";
import { withTranslation } from "react-i18next";
import { connect } from 'react-redux';
import * as MappingNexsellerService from "../../../../Service/Admin/MappingNexsellerService";
import { propTable, RANDOM_ID } from "../../../../Enum/PropertiesConstant";
import { stateRedux } from '../../../../Util/ReduxState';
import ButtonDelete from "../../../Common/Button/ButtonDelete";
import ButtonEdit from "../../../Common/Button/ButtonEdit";
import CustomImage from "../../../Common/Image/CustomImage";
import moment from 'moment';
import ModalDelete from '../../../Common/Modal/ModalDelete';
import ModalSuccessDelete from '../../../Common/Modal/ModalSuccessDelete';

class DetailMappingNexseller extends Component {

    state = {
        comp: 1,
        detail: null,
        modalOpen: false,
        ...propTable,
        responseSuccess: null,
        modalSuccessDeleteOpen: false,
        mapping_field_1: null,
        mapping_field_2: null,
        mapping_field_3: null,
        mapping_field_4: null,
        mapping_field_5: null,
        mapping_field_6: null,
        mapping_field_7: null,
        mapping_field_8: null,
        mapping_field_9: null,
        mapping_field_10: null,
    };

    _onChangeComp = i => {
        this.setState({ comp: i });
    };

    async componentDidMount() {
        await this._getDetail(this.props.match.params.id);
    }

    _getDetail = async (id) => {
        const { token, refresh_token, dispatch } = this.props;

        const response = await MappingNexsellerService.doGetDetail(token, refresh_token, id, dispatch);
        if (response) this.setState({ detail: response.detail }, () => {
            console.log("detail", this.state.detail);

            //isi mapping field
            for (let index = 0; index < this.state.detail.additional_info.length; index++) {
                switch (this.state.detail.additional_info[index].key) {
                    case "mapping_field_1":
                        this.setState({ mapping_field_1 : this.state.detail.additional_info[index].value})
                        break;
                    
                    case "mapping_field_2":
                        this.setState({ mapping_field_2: this.state.detail.additional_info[index].value })
                        break;
                    
                    case "mapping_field_3":
                        this.setState({ mapping_field_3: this.state.detail.additional_info[index].value })
                        break;
                    
                    case "mapping_field_4":
                        this.setState({ mapping_field_4: this.state.detail.additional_info[index].value })
                        break;
                    
                    case "mapping_field_5":
                        this.setState({ mapping_field_5: this.state.detail.additional_info[index].value })
                        break;
                    
                    case "mapping_field_6":
                        this.setState({ mapping_field_6: this.state.detail.additional_info[index].value })
                        break;
                    
                    case "mapping_field_7":
                        this.setState({ mapping_field_7: this.state.detail.additional_info[index].value })
                        break;
                    
                    case "mapping_field_8":
                        this.setState({ mapping_field_8: this.state.detail.additional_info[index].value })
                        break;
                    
                    case "mapping_field_9":
                        this.setState({ mapping_field_9: this.state.detail.additional_info[index].value })
                        break;
                    
                    case "mapping_field_10":
                        this.setState({ mapping_field_10: this.state.detail.additional_info[index].value })
                        break;
                
                    default:
                        break;
                }
            }
            
        })
        // if (response) this.setState({ detail: response.detail });
        // console.log("detail", this.state.detail);
        // console.log("created at", this.state.detail.created_at);
    };

    _onDelete = async (id, updated_at) => {
        const { token, refresh_token, dispatch } = this.props;

        if (this.props.permission.delete || this.props.permission.delete_own) {
            const response = await MappingNexsellerService.doDelete(token, refresh_token, id, { updated_at }, dispatch);
            this.setState({ responseSuccess: response });
            this.setState({ modalOpen: false });
            this.setState({ modalSuccessDeleteOpen: true });
        } else {
            console.log("else");
        }
    };

    _openModal = () => {
        this.setState({ modalOpen: true });
    }

    _closeModal = () => {
        console.log("oke");
        this.setState({ modalOpen: false });
    }

    _closeModalSuccessDelete = () => {
        this.setState({ modalSuccessDeleteOpen: false });
    }

    render() {

        const DataManagementHistory = () => (
            <div className={style.GridItemLuar}>
                <div className={style.DataManagementContainer}>
                    <div className={style.DataManagement}>
                        {this.props.t("MAPPING_NEXSELLER:FIELD_DATA_MANAGEMENT_HISTORY")}
                    </div>
                    <div className={style.CreatedModified}>
                        {this.props.t("MAPPING_NEXSELLER:FIELD_CREATED_ON")} {this.state.detail !== null && this.state.detail.created_at !== '' ? moment(this.state.detail.created_at).format('DD-MMM-YYYY') : "-"}
                    </div>
                    <div className={style.CreatedModified}>
                        {this.props.t("MAPPING_NEXSELLER:FIELD_CREATED_BY")} {this.state.detail !== null && this.state.detail.created_by !== '' ? this.state.detail.created_by : "-"}
                    </div>
                    <br />
                    <div className={style.CreatedModified}>
                        {this.props.t("MAPPING_NEXSELLER:FIELD_MODIFIED_ON")} {this.state.detail !== null && this.state.detail.updated_at !== '' ? moment(this.state.detail.updated_at).format('DD-MMM-YYYY') : "-"}
                    </div>
                    <div className={style.CreatedModified}>
                        {this.props.t("MAPPING_NEXSELLER:FIELD_MODIFIED_BY")} {this.state.detail !== null && this.state.detail.updated_by !== '' ? this.state.detail.updated_by : "-"}
                    </div>
                </div>
            </div>
        );

        const CompanyProfile = () => (
            <div className={style.GridContainerLuar}>
                <div className={style.GridItemLuar}>
                    <div className={style.SubTitle}>
                        <div>
                            {this.props.t("MAPPING_NEXSELLER:FIELD_COMPANY_PROFILE")}
                        </div>
                    </div>
                    <div className={style.GridContainerDalam}>
                        <div className={style.GridItemDalam}>
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_NEXCHIEF_ACCOUNT_NAME")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.nexchief_account_name !== '' ? this.state.detail.nexchief_account_name : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_COMPANY_TYPE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.company_title !== '' ? this.state.detail.company_profile.company_title : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_COMPANY_NAME")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.name !== '' ? this.state.detail.company_profile.name : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_CORPORATE_PHONE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.phone !== '' ? this.state.detail.company_profile.phone : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_FAX_NUMBER")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.fax !== '' ? this.state.detail.company_profile.fax : "-"}</div>

                        </div>
                        <div className={style.GridItemDalam}>
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_CORPORATE_EMAIL")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.email !== '' ? this.state.detail.company_profile.email : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_NPWP")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.npwp !== '' ? this.state.detail.company_profile.npwp : "-"}</div>

                            <div className={style.LabelForm} style={{marginBottom: "5px"}}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_COMPANY_LOGO")}
                            </div>
                            <CustomImage logo={this.state.detail && this.state.detail.company_profile.logo && this.state.detail.company_profile.logo[0].host + this.state.detail.company_profile.logo[0].path} />

                        </div>
                        <div className={style.SubTitle}>
                            <div>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_ADDRESS")}
                            </div>
                        </div>

                    </div>

                    <div className={style.GridContainerDalam}>
                        <div className={style.GridItemDalam}>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_ADDRESS")}
                            </div>
                            <div className={style.DetailItem}>
                                {this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.address_1 !== '' ? this.state.detail.company_profile.address_1 : "-"}
                                {this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.address_2 !== '' && <div>{this.state.detail.company_profile.address_2}</div>}
                                {this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.address_3 !== '' && <div>{this.state.detail.company_profile.address_3}</div>}
                            </div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_COUNTRY")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.country_name !== '' ? this.state.detail.company_profile.country_name : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_DISTRICT")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.district !== '' ? this.state.detail.company_profile.district : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_SUB_DISTRICT")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.sub_district !== '' ? this.state.detail.company_profile.sub_district : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_URBAN_VILLAGE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.urban_village !== '' ? this.state.detail.company_profile.urban_village : "-"}</div>

                        </div>
                        <div className={style.GridItemDalam}>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_HAMLET")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.hamlet !== '' ? this.state.detail.company_profile.hamlet : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_NEIGHBOURHOOD")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.neighbourhood !== '' ? this.state.detail.company_profile.neighbourhood : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_POSTAL_CODE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.postal_code !== '' ? this.state.detail.company_profile.postal_code : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_ISLAND")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.island !== '' ? this.state.detail.company_profile.island : "-"}</div>

                        </div>

                    </div>
                </div>
                <DataManagementHistory />
            </div>
        );

        const Account = () => (
            <div className={style.GridContainerLuar}>
                <div className={style.GridItemLuar}>
                    <div className={style.SubTitle}>
                        <div>
                            {this.props.t("MAPPING_NEXSELLER:FIELD_ACCOUNT")}
                        </div>
                    </div>
                    <div className={style.GridContainerDalam2}>
                        <div className={style.GridItemDalam}>
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_NEXSELLER_CODE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.code !== '' ? this.state.detail.code : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_NEXSELLER_PARENT_CODE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.parent_code !== '' ? this.state.detail.parent_code : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_ACTIVATION_DATE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.activation_date !== '' ? moment(this.state.detail.activation_date).format("DD-MM-YYYY") : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_RESIGNATION_DATE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.resignation_date !== '' ? moment(this.state.detail.resignation_date).format("DD-MM-YYYY") : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_GEO_TREE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.geo_tree_name !== '' ? this.state.detail.geo_tree_name : "-"}</div>

                        </div>
                        <div className={style.GridItemDalam}>
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_PRICE_GROUP")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.price_group_name !== '' ? this.state.detail.price_group_name : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_PRODUCT_CATEGORY")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.product_category_name !== '' ? this.state.detail.product_category_name : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_PRODUCT_CLASS_FROM")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.product_class_from_id !== '' ? this.state.detail.product_class_from_id : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_PRODUCT_CLASS_THRU")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.product_class_thru_id !== '' ? this.state.detail.product_class_thru_id : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_SEND_SYNC_DATA_TO_EMAIL")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.send_sync_data_to_email !== '' ? this.state.detail.send_sync_data_to_email : "-"}</div>

                        </div>
                        <div className={style.GridItemDalam}>
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_ND6_SYNC_METHOD")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.nd6_sync_method !== '' ? (this.state.detail.nd6_sync_method === 'B' ? "Inbound and Outbound" : "Inbound Only (From ND6)") : "-"}</div>

                        </div>
                        <div className={style.SubTitle}>
                            <div>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_ACCOUNT_CONTROL")}
                            </div>
                        </div>

                    </div>

                    <div className={style.GridContainerDalam2}>
                        <div className={style.GridItemDalam}>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_PRODUCT_MAPPING")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.product_mapping !== '' ? (this.state.detail.product_mapping ? "Yes" : "No") : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_HOSTING_ONLY")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.hosting_only !== '' ? (this.state.detail.hosting_only ? "Yes" : "No") : "-"}</div>

                        </div>
                        <div className={style.GridItemDalam}>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_SALESMAN_MAPPING")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.salesman_mapping !== '' ? (this.state.detail.salesman_mapping ? "Yes" : "No") : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_SYNC_DELIVERY_NOTE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.sync_delivery_note !== '' ? (this.state.detail.sync_delivery_note ? "Yes" : "No") : "-"}</div>

                        </div>
                        <div className={style.GridItemDalam}>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_CUSTOMER_MAPPING")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.customer_mapping !== '' ? (this.state.detail.customer_mapping ? "Yes" : "No") : "-"}</div>

                        </div>

                        <div className={style.SubTitle}>
                            <div>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING")}
                            </div>
                        </div>

                    </div>

                    <div className={style.GridContainerDalam2}>
                        <div className={style.GridItemDalam}>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_ND6_COMPANY_ID")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.nd6_company_id !== '' ? this.state.detail.nd6_company_id : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_ND6_BRANCH_ID")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.nd6_branch_id !== '' ? this.state.detail.nd6_branch_id : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_NEXMILE_VERSION")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.nexmile_version !== '' ? this.state.detail.nexmile_version : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_ND6_VERSION")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.nd6_version !== '' ? this.state.detail.nd6_version : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_PRODUCT_MASTER_VERSION")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.product_master_version !== '' ? this.state.detail.product_master_version : "-"}</div>

                        </div>
                        <div className={style.GridItemDalam}>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_1")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.mapping_field_1 !== null ? this.state.mapping_field_1 : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_2")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.mapping_field_2 !== null ? this.state.mapping_field_2 : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_3")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.mapping_field_3 !== null ? this.state.mapping_field_3 : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_4")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.mapping_field_4 !== null ? this.state.mapping_field_4 : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_5")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.mapping_field_5 !== null ? this.state.mapping_field_5 : "-"}</div>


                        </div>
                        <div className={style.GridItemDalam}>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_6")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.mapping_field_6 !== null ? this.state.mapping_field_6 : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_7")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.mapping_field_7 !== null ? this.state.mapping_field_7 : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_8")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.mapping_field_8 !== null ? this.state.mapping_field_8 : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_9")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.mapping_field_9 !== null ? this.state.mapping_field_9 : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_10")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.mapping_field_10 !== null ? this.state.mapping_field_10 : "-"}</div>

                        </div>

                    </div>
                </div>
                <DataManagementHistory />
            </div>
        );

        const ContactPerson = () => (
            <div className={style.GridContainerLuar}>
                <div className={style.GridItemLuar}>
                    <div className={style.SubTitle}>
                        <div>
                            {this.props.t("MAPPING_NEXSELLER:FIELD_CONTACT_PERSON")}
                        </div>
                    </div>
                    <div className={style.GridContainerDalam2}>
                        <div className={style.GridItemDalam}>
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_NIK")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.nik !== "" ? this.state.detail.contact_person.nik : "-"}</div>
                            
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_TITLE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.person_title !== "" ? this.state.detail.contact_person.person_title : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_FIRST_NAME")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.first_name !== "" ? this.state.detail.contact_person.first_name : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_LAST_NAME")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.last_name !== "" ? this.state.detail.contact_person.last_name : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_ADDRESS")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.address !== "" ? this.state.detail.contact_person.address : "-"}</div>

                            
                        </div>
                        <div className={style.GridItemDalam}>
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_COUNTRY")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.country !== "" ? this.state.detail.contact_person.country : "-"}</div>
                            
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_EMAIL")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.email !== "" ? this.state.detail.contact_person.email : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_PHONE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.phone !== "" ? this.state.detail.contact_person.phone : "-"}</div>
                            
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_POSITION")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.position !== "" ? this.state.detail.contact_person.position : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_JOIN_DATE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.join_date !== "" ? moment(this.state.detail.contact_person.join_date).format("DD-MM-YYYY") : "-"}</div>

                        </div>
                        <div className={style.GridItemDalam}>
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_RESIGN_DATE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.resign_date !== "" ? moment(this.state.detail.contact_person.resign_date).format("DD-MM-YYYY") : "-"}</div>

                        </div>
                    </div>

                </div>
                <DataManagementHistory />
            </div>
        );

        const Socket = () => (
            <div className={style.GridContainerLuar}>
                <div className={style.GridItemLuar}>
                    <div className={style.SubTitle}>
                        <div>
                            {this.props.t("MAPPING_NEXSELLER:FIELD_SOCKET")}
                        </div>
                    </div>

                    <div className={style.GridContainerDalam}>
                        <div className={style.GridItemDalam}>
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_SOCKET_USER_ID")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.socket_user_id !== "" ? this.state.detail.socket_user_id : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_SOCKET_PASSWORD")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.socket_password !== "" ? this.state.detail.socket_password : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_SOCKET_STATUS")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.socket_status !== "" ? moment(this.state.detail.socket_status).format("DD-MM-YYYY") : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_LAST_DMS_SYNC")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.last_dms_sync !== "" ? moment(this.state.detail.last_dms_sync).format("DD-MM-YYYY") : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_LAST_SFA_SYNC")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.last_sfa_sync !== "" ? moment(this.state.detail.last_sfa_sync).format("DD-MM-YYYY") : "-"}</div>

                        </div>
                        <div className={style.GridItemDalam}>
                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_GROMART_MERCHANT_ID")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.gromart_merchant_id !== "" ? this.state.detail.gromart_merchant_id : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_PREFIX_DELETED")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.prefix_deleted !== "" ? moment(this.state.detail.prefix_deleted).format("DD-MM-YYYY") : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_ND6_CLOSE_DATE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.nd6_close_date !== "" ? moment(this.state.detail.nd6_close_date).format("DD-MM-YYYY") : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_EMAIL_TO")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.email_to !== "" ? moment(this.state.detail.email_to).format("DD-MM-YYYY") : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("MAPPING_NEXSELLER:FIELD_EMAIL_CC_TO")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.email_cc_to !== "" ? moment(this.state.detail.email_cc_to).format("DD-MM-YYYY") : "-"}</div>

                        </div>
                    </div>

                </div>
                <DataManagementHistory />
            </div>
        );

        const ModalDelete2 = () => (
            <div>
                {
                    this.state.detail &&
                    <ModalDelete
                        key={RANDOM_ID(8)}
                        modalOpen={this.state.modalOpen}
                        menu={this.props.t("MAPPING_NEXSELLER:TITLE_DELETE")}
                        onDelete={() => this._onDelete(this.state.detail.id, this.state.detail.updated_at)}
                        closeModal={() => this._closeModal()}
                        okDelete={this.props.t("MAPPING_NEXSELLER:TITLE_HEADER")}
                    />
                }
            </div>
        )

        const ModalSuccessDelete2 = () => (
            <div>
                {
                    this.state.responseSuccess &&
                    <ModalSuccessDelete
                        key={RANDOM_ID(8)}
                        modalSuccessDeleteOpen={this.state.modalSuccessDeleteOpen}
                        response={this.state.responseSuccess}
                        closeModal={() => this._closeModalSuccessDelete()}
                        router={ROUTER_CONSTANT.MAPPING_NEXSELLER.LIST}
                    />
                }
            </div>
        )

        const ButtonDelete2 = () => (
            <div style={{ display: "contents" }}>
                {
                    this.state.detail &&
                    <ButtonDelete
                        key={RANDOM_ID(8)}
                        detail={this.state.detail}
                        openModal={this._openModal}
                        disabled={this.props.permission.delete ? false : this.props.permission.delete_own ? this.props.detail.created_by !== this.props.user : true}
                    />
                }

                <ModalDelete2 />
                <ModalSuccessDelete2 />
            </div>
        );

        const ButtonEdit2 = () => (
            <div style={{ display: "contents" }}>
                {
                    this.state.detail &&
                    <ButtonEdit
                        key={RANDOM_ID(8)}
                        router={ROUTER_CONSTANT.MAPPING_NEXSELLER.EDIT + "/" + this.props.match.params.id}
                        text=""
                        detail={this.state.detail}
                        disabled={this.props.permission.update ? false : this.props.permission.update_own ? this.state.detail.created_by !== this.props.user : true}
                    />
                }
            </div>
        );

        // console.log("2", this.state.detail.created_by !== this.props.user);

        return (
            <div>
                <div className={style.MenuTitle}>{this.props.t("MAPPING_NEXSELLER:TITLE")} &#62; Detail</div>
                <div className={style.Title}>
                    <div>{this.props.t("MAPPING_NEXSELLER:DETAIL.TITLE")}</div>
                    <ButtonDelete2 />
                    <ButtonEdit2 />
                </div>

                {/* container luar */}
                <div style={{ marginBottom: "20px", paddingLeft: "55px", paddingRight: "55px", paddingBottom: "55px", position: "absolute", width: "100%" }}>
                    <div className={style.List}>
                        {/* container luar */}
                        <div style={{ alignItems: "center" }}>
                            <div>
                                <button className={this.state.comp === 1 ? style.ButtonComponentActive : style.ButtonComponent} onClick={() => this._onChangeComp(1)}>
                                    {this.props.t("MAPPING_NEXSELLER:FIELD_COMPANY_PROFILE")}
                                </button>
                                <button className={this.state.comp === 2 ? style.ButtonComponentActive : style.ButtonComponent} onClick={() => this._onChangeComp(2)}>
                                    {this.props.t("MAPPING_NEXSELLER:FIELD_ACCOUNT")}
                                </button>
                                <button className={this.state.comp === 3 ? style.ButtonComponentActive : style.ButtonComponent} onClick={() => this._onChangeComp(3)}>
                                    {this.props.t("MAPPING_NEXSELLER:FIELD_SOCKET")}
                                </button>
                                <button className={this.state.comp === 4 ? style.ButtonComponentActive : style.ButtonComponent} onClick={() => this._onChangeComp(4)}>
                                    {this.props.t("MAPPING_NEXSELLER:FIELD_CONTACT_PERSON")}
                                </button>
                            </div>

                            {this.state.comp === 1 && <CompanyProfile />}
                            {this.state.comp === 2 && <Account />}
                            {this.state.comp === 3 && <Socket />}
                            {this.state.comp === 4 && <ContactPerson />}

                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { ...stateRedux(state, "mapping_nexseller") };
};

const mapDispatchToProps = (dispatch) => {
    return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(DetailMappingNexseller));