import React, { Component } from 'react';
import style from "./AddMappingNexseller.module.css";
import { withTranslation } from "react-i18next";
import { rulesMappingNexseller } from '../../../../Util/FormRules';
import { DatePicker, Form, Input, Select, Switch } from "antd";
import ButtonCancel from "../../../Common/Button/ButtonCancel";
import ButtonNext from "../../../Common/Button/ButtonNext";
import { geotree, pricegroup, productcategory, propTable, country, mappingnexseller, nexchiefaccount } from "../../../../Enum/PropertiesConstant";
import { concatPrefix, generatePrefix, getInitiate, getInitiateFromMasterData, getList, getListFromMasterData } from "../../../../Service/Util";
import { prepareComponent } from '../../../../Action/CommonAction';
import { connect } from 'react-redux';
import ChevronDown from '../../../Icon/ChevronDown';
import CustomImage from '../../../Common/Image/CustomImage';
import { ROUTER_CONSTANT } from '../../../../Enum/RouterConstant';
import DatePickerLogo from '../../../Icon/DatePickerLogo';
import ButtonBack from '../../../Common/Button/ButtonBack';
import ButtonSubmit from '../../../Common/Button/ButtonSubmit';
import ButtonCheck from '../../../Common/Button/ButtonCheck';
import ButtonAddContactPerson from '../../../Common/Button/ButtonAddContactPerson';
import ButtonContactPersonDst from '../../../Common/Button/ButtonContactPersonDst';
import * as PersonProfileService from "../../../../Service/Admin/PersonProfileService";
import { stateRedux } from '../../../../Util/ReduxState';
import { dropdownCustom, LoadingOption, selectOption } from "../../../Common/Select/CustomSelect";

const { Option } = Select;

const children = [];
for (let i = 10; i < 36; i++) {
    children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}

class AddMappingNexseller extends Component {

    state = {
        ...propTable,
        comp: 1,
        loading: false,
        jumlahContactPerson: 1,
        indexContactPersonActive: 1,

        contactList: [],

        values: [null, null, null, null],

        is_pbf: null,

        ...country,

        ...nexchiefaccount,
        ...mappingnexseller,
        ...geotree,
        ...pricegroup,
        ...productcategory,
    };

    _nextComp = i => {
        this.setState({ comp: i + 1 });
    };

    _prevComp = i => {
        this.setState({ comp: i - 1 });
    };

    _onCheck = async (i) => {
        var id = "nik" + i;
        var nikValue = document.getElementById(id).value;
        console.log("id", id);
        console.log("nik value", nikValue);

        const data2 = {
            page: this.state.page,
            limit: this.state.limit,
            order_by: this.state.order_by,
            nik: nikValue,
        }

        this._getData(data2);
    };

    _getData = async (value) => {
        const { token, refresh_token, dispatch } = this.props;

        // const today = moment().format("DD-MM-YYYY");

        if (this.props.permission.view || this.props.permission.view_own) {
            const response = await PersonProfileService.doGetList(token, refresh_token, value, dispatch);
            console.log("res 123", response);
            // if (response) {
            //     this.setState({ first_name: response.first_name });
            //     this.setState({ last_name: response.last_name });
            //     this.setState({ position: response.position });
            //     this.setState({ phone: response.phone });
            //     this.setState({ email: response.email });
            //     this.setState({ join_date: response.join_date });
            //     this.setState({ resign_date: response.resign_date });
            // } else {
            //     this.setState({ first_name: '' });
            //     this.setState({ last_name: '' });
            //     this.setState({ position: '' });
            //     this.setState({ phone: '' });
            //     this.setState({ email: '' });
            //     this.setState({ join_date: moment(moment(today, "DD-MM-YYYY")) });
            //     this.setState({ resign_date: moment(moment(today, "DD-MM-YYYY")) });
            // }

        }
    };

    _initiateCountry = async (page, limit, filter) => {
        const currentCountry = [...this.state.dataCountry];
        const { token, refresh_token, dispatch } = this.props;

        const initiateCountry = await getInitiateFromMasterData(token, refresh_token, "country", filter, dispatch);
        const previousCountry = await getListFromMasterData(token, refresh_token, "country", page, limit, null, filter, dispatch);

        if (initiateCountry || previousCountry) {
            if (!previousCountry) {
                this.setState({ lastPageCountry: true });
            } else {
                if (previousCountry.length < limit) {
                    this.setState({ lastPageCountry: true });
                } else {
                    this.setState({ lastPageCountry: false });
                }

                this.setState({
                    initiateCountry,
                    dataCountry: selectOption(currentCountry, previousCountry, "value"),
                    prefixCountry: generatePrefix(initiateCountry),
                });
            }

            this.setState({ loading: false });
        }
    };

    _initiateNexchiefAccount = async (page, limit, filter) => {
        const currentNexchiefAccount = [...this.state.dataNexchiefAccount];
        const { token, refresh_token, dispatch } = this.props;

        const initiateNexchiefAccount = await getInitiate(token, refresh_token, "admin/nexchiefaccount", filter, dispatch);
        const previousNexchiefAccount = await getList(token, refresh_token, "admin/nexchiefaccount/search", page, limit, null, filter, dispatch);

        if (initiateNexchiefAccount || previousNexchiefAccount) {
            if (!previousNexchiefAccount) {
                this.setState({ lastPageNexchiefAccount: true });
            } else {
                if (previousNexchiefAccount.length < limit) {
                    this.setState({ lastPageNexchiefAccount: true });
                } else {
                    this.setState({ lastPageNexchiefAccount: false });
                }

                this.setState({
                    initiateNexchiefAccount,
                    dataNexchiefAccount: selectOption(currentNexchiefAccount, previousNexchiefAccount, "value"),
                    prefixNexchiefAccount: generatePrefix(initiateNexchiefAccount),
                });
            }

            this.setState({ loading: false });
        }
    };

    _initiateMappingNexseller = async (page, limit, filter) => {
        const currentMappingNexseller = [...this.state.dataMappingNexseller];
        const { token, refresh_token, dispatch } = this.props;

        // console.log("nc id", this.state.values[0].nexchief_account_name);

        if (filter) {
            filter = "nexchief_account_id eq " + this.state.values[0].nexchief_account_name + ", " + filter;
        } else {
            filter = "nexchief_account_id eq " + this.state.values[0].nexchief_account_name;
        }

        const initiateMappingNexseller = await getInitiate(token, refresh_token, "admin/mappingnexseller", filter, dispatch);
        const previousMappingNexseller = await getList(token, refresh_token, "admin/mappingnexseller/search", page, limit, null, filter, dispatch);

        if (initiateMappingNexseller || previousMappingNexseller) {
            if (!previousMappingNexseller) {
                this.setState({ lastPageMappingNexseller: true });
            } else {
                if (previousMappingNexseller.length < limit) {
                    this.setState({ lastPageMappingNexseller: true });
                } else {
                    this.setState({ lastPageMappingNexseller: false });
                }

                this.setState({
                    initiateMappingNexseller,
                    dataMappingNexseller: selectOption(currentMappingNexseller, previousMappingNexseller, "value"),
                    prefixMappingNexseller: generatePrefix(initiateMappingNexseller),
                });
            }

            this.setState({ loading: false });
        }
    };

    _initiateGeoTree = async (page, limit, filter) => {
        const currentGeoTree = [...this.state.dataGeoTree];
        const { token, refresh_token, dispatch } = this.props;

        if (filter) {
            filter = "nexchief_account_id eq " + this.state.values[0].nexchief_account_name + ", " + filter;
        } else {
            filter = "nexchief_account_id eq " + this.state.values[0].nexchief_account_name;
        }

        const initiateGeoTree = await getInitiate(token, refresh_token, "geotree", filter, dispatch);
        const previousGeoTree = await getList(token, refresh_token, "geotree/search", page, limit, null, filter, dispatch);
        console.log("prevgeotree", previousGeoTree);

        if (initiateGeoTree || previousGeoTree) {
            if (!previousGeoTree) {
                this.setState({ lastPageGeoTree: true });
            } else {
                if (previousGeoTree.length < limit) {
                    this.setState({ lastPageGeoTree: true });
                } else {
                    this.setState({ lastPageGeoTree: false });
                }

                this.setState({
                    initiateGeoTree,
                    dataGeoTree: selectOption(currentGeoTree, previousGeoTree, "value"),
                    prefixGeoTree: generatePrefix(initiateGeoTree),
                });
            }

            this.setState({ loading: false });
        }
    };

    _initiatePriceGroup = async (page, limit, filter) => {
        const currentPriceGroup = [...this.state.dataPriceGroup];
        const { token, refresh_token, dispatch } = this.props;

        if (filter) {
            filter = "nexchief_account_id eq " + this.state.values[0].nexchief_account_name + ", " + filter;
        } else {
            filter = "nexchief_account_id eq " + this.state.values[0].nexchief_account_name;
        }

        const initiatePriceGroup = await getInitiate(token, refresh_token, "pricegroup", filter, dispatch);
        const previousPriceGroup = await getList(token, refresh_token, "pricegroup/search", page, limit, null, filter, dispatch);

        if (initiatePriceGroup || previousPriceGroup) {
            if (!previousPriceGroup) {
                this.setState({ lastPagePriceGroup: true });
            } else {
                if (previousPriceGroup.length < limit) {
                    this.setState({ lastPagePriceGroup: true });
                } else {
                    this.setState({ lastPagePriceGroup: false });
                }

                this.setState({
                    initiatePriceGroup,
                    dataPriceGroup: selectOption(currentPriceGroup, previousPriceGroup, "value"),
                    prefixPriceGroup: generatePrefix(initiatePriceGroup),
                });
            }

            this.setState({ loading: false });
        }
    };

    _initiateProductCategory = async (page, limit, filter) => {
        const currentProductCategory = [...this.state.dataProductCategory];
        const { token, refresh_token, dispatch } = this.props;

        if (filter) {
            filter = "nexchief_account_id eq " + this.state.values[0].nexchief_account_name + ", " + filter;
        } else {
            filter = "nexchief_account_id eq " + this.state.values[0].nexchief_account_name;
        }

        const initiateProductCategory = await getInitiate(token, refresh_token, "productcategory", filter, dispatch);
        const previousProductCategory = await getList(token, refresh_token, "productcategory/search", page, limit, null, filter, dispatch);

        if (initiateProductCategory || previousProductCategory) {
            if (!previousProductCategory) {
                this.setState({ lastPageProductCategory: true });
            } else {
                if (previousProductCategory.length < limit) {
                    this.setState({ lastPageProductCategory: true });
                } else {
                    this.setState({ lastPageProductCategory: false });
                }

                this.setState({
                    initiateProductCategory,
                    dataProductCategory: selectOption(currentProductCategory, previousProductCategory, "value"),
                    prefixProductCategory: generatePrefix(initiateProductCategory),
                });
            }

            this.setState({ loading: false });
        }
    };

    _getDataInitiateNexchiefAccount = async () => {
        if (this.state.dataNexchiefAccount.length === 0) await this._initiateNexchiefAccount(this.state.pageNexchiefAccount, this.state.limitNexchiefAccount, this.state.filterNexchiefAccount);
    };

    _getDataInitiate = async () => {
        if (this.state.dataCountry.length === 0) await this._initiateCountry(this.state.pageCountry, this.state.limitCountry, this.state.filterCountry);
        if (this.state.dataMappingNexseller.length === 0) await this._initiateMappingNexseller(this.state.pageMappingNexseller, this.state.limitMappingNexseller, this.state.filterMappingNexseller);
        if (this.state.dataGeoTree.length === 0) await this._initiateGeoTree(this.state.pageGeoTree, this.state.limitGeoTree, this.state.filterGeoTree);
        if (this.state.dataPriceGroup.length === 0) await this._initiatePriceGroup(this.state.pagePriceGroup, this.state.limitPriceGroup, this.state.filterPriceGroup);
        if (this.state.dataProductCategory.length === 0) await this._initiateProductCategory(this.state.pageProductCategory, this.state.limitProductCategory, this.state.filterProductCategory);
    };

    async componentDidMount() {
        prepareComponent(this.props.dispatch, true);
        await this._getDataInitiateNexchiefAccount();

        setTimeout(() => prepareComponent(this.props.dispatch, false), 128);
    }

    _onScroll = (event, namespace) => {
        let target = event.target;

        // console.log("aaa loadingnya", this.state.loading);
        // console.log("aaa hitungan1", target.scrollTop + target.offsetHeight);
        // console.log("aaa hitungan2", target.scrollHeight);

        if (!this.state.loading && target.scrollTop + target.offsetHeight === target.scrollHeight) {
            switch (namespace) {
                case "country":
                    if (!this.state.lastPageCountry) {
                        this.setState({ loading: true, pageCountry: this.state.pageCountry + 1 }, () => {
                            target.scrollTo(0, target.scrollHeight);
                            setTimeout(() => this._initiateCountry(this.state.pageCountry, this.state.limitCountry, this.state.filterCountry), 256);
                        });
                    }
                    break;

                case "nexchiefaccount":
                    if (!this.state.lastPageNexchiefAccount) {
                        this.setState({
                            loading: true,
                            pageNexchiefAccount: this.state.pageNexchiefAccount + 1
                        },
                            () => {
                                target.scrollTo(0, target.scrollHeight);
                                setTimeout(() => this._initiateNexchiefAccount(this.state.pageNexchiefAccount, this.state.limitNexchiefAccount, this.state.filterNexchiefAccount), 256);
                            }
                        );
                    }
                    break;

                case "mappingnexseller":
                    if (!this.state.lastPageMappingNexseller) {
                        this.setState({
                            loading: true,
                            pageMappingNexseller: this.state.pageMappingNexseller + 1
                        },
                            () => {
                                target.scrollTo(0, target.scrollHeight);
                                setTimeout(() => this._initiateMappingNexseller(this.state.pageMappingNexseller, this.state.limitMappingNexseller, this.state.filterMappingNexseller), 256);
                            }
                        );
                    }
                    break;

                case "geotree":
                    if (!this.state.lastPageGeoTree) {
                        this.setState({
                            loading: true,
                            pageGeoTree: this.state.pageGeoTree + 1
                        },
                            () => {
                                target.scrollTo(0, target.scrollHeight);
                                setTimeout(() => this._initiateGeoTree(this.state.pageGeoTree, this.state.limitGeoTree, this.state.filterGeoTree), 256);
                            }
                        );
                    }
                    break;

                case "pricegroup":
                    if (!this.state.lastPagePriceGroup) {
                        this.setState({
                            loading: true,
                            pagePriceGroup: this.state.pagePriceGroup + 1
                        },
                            () => {
                                target.scrollTo(0, target.scrollHeight);
                                setTimeout(() => this._initiatePriceGroup(this.state.pagePriceGroup, this.state.limitPriceGroup, this.state.filterPriceGroup), 256);
                            }
                        );
                    }
                    break;

                case "productcategory":
                    if (!this.state.lastPageProductCategory) {
                        this.setState({
                            loading: true,
                            pageProductCategory: this.state.pageProductCategory + 1
                        },
                            () => {
                                target.scrollTo(0, target.scrollHeight);
                                setTimeout(() => this._initiateProductCategory(this.state.pageProductCategory, this.state.limitProductCategory, this.state.filterProductCategory), 256);
                            }
                        );
                    }
                    break;

                default:
                    break;
            }
        }
    };

    _onChange = (event, namespace) => {
        const { value } = event.target;

        const resetPage = 1;
        const resetFilter = null;

        switch (namespace) {
            case "country":
                clearTimeout(this.inputTimer);

                if (value.length >= 3) {
                    // console.log("prefixCountry", this.state.prefixCountry);
                    this.setState({ loading: true, dataCountry: [], filterCountry: value, pageCountry: resetPage }, () => {
                        this.inputTimer = setTimeout(() => this._initiateCountry(this.state.pageCountry, this.state.limitCountry, this.state.filterCountry), 256);
                    });
                    // console.log("state", this.state.prefixCountry);
                } else if (!value) {
                    this.setState({ loading: true, dataCountry: [], filterCountry: resetFilter, pageCountry: resetPage }, () => {
                        setTimeout(() => this._initiateCountry(this.state.pageCountry, this.state.limitCountry, this.state.filterCountry), 256);
                    });
                }
                break;

            case "nexchiefaccount":
                clearTimeout(this.inputTimer);
                if (value.length >= 3) {
                    this.setState({ loading: true, dataNexchiefAccount: [], filterNexchiefAccount: "name like " + value, pageNexchiefAccount: resetPage }, () => {
                        this.inputTimer = setTimeout(() => this._initiateNexchiefAccount(this.state.pageNexchiefAccount, this.state.limitNexchiefAccount, this.state.filterNexchiefAccount), 256);
                    });
                } else if (!value) {
                    this.setState({ loading: true, dataNexchiefAccount: [], filterNexchiefAccount: resetFilter, pageNexchiefAccount: resetPage }, () => {
                        setTimeout(() => this._initiateNexchiefAccount(this.state.pageNexchiefAccount, this.state.limitNexchiefAccount, this.state.filterNexchiefAccount), 256);
                    });
                }
                break;

            case "mappingnexseller":
                clearTimeout(this.inputTimer);
                if (value.length >= 3) {
                    this.setState({ loading: true, dataMappingNexseller: [], filterMappingNexseller: "name like " + value, pageMappingNexseller: resetPage }, () => {
                        this.inputTimer = setTimeout(() => this._initiateMappingNexseller(this.state.pageMappingNexseller, this.state.limitMappingNexseller, this.state.filterMappingNexseller), 256);
                    });
                } else if (!value) {
                    this.setState({ loading: true, dataMappingNexseller: [], filterMappingNexseller: resetFilter, pageMappingNexseller: resetPage }, () => {
                        setTimeout(() => this._initiateMappingNexseller(this.state.pageMappingNexseller, this.state.limitMappingNexseller, this.state.filterMappingNexseller), 256);
                    });
                }
                break;

            case "geotree":
                clearTimeout(this.inputTimer);
                if (value.length >= 3) {
                    this.setState({ loading: true, dataGeoTree: [], filterGeoTree: "name like " + value, pageGeoTree: resetPage }, () => {
                        this.inputTimer = setTimeout(() => this._initiateGeoTree(this.state.pageGeoTree, this.state.limitGeoTree, this.state.filterGeoTree), 256);
                    });
                } else if (!value) {
                    this.setState({ loading: true, dataGeoTree: [], filterGeoTree: resetFilter, pageGeoTree: resetPage }, () => {
                        setTimeout(() => this._initiateGeoTree(this.state.pageGeoTree, this.state.limitGeoTree, this.state.filterGeoTree), 256);
                    });
                }
                break;

            case "pricegroup":
                clearTimeout(this.inputTimer);

                if (value.length >= 3) {
                    this.setState({ loading: true, dataPriceGroup: [], filterPriceGroup: "name like " + value, pagePriceGroup: resetPage }, () => {
                        this.inputTimer = setTimeout(() => this._initiatePriceGroup(this.state.pagePriceGroup, this.state.limitPriceGroup, this.state.filterPriceGroup), 256);
                    });
                } else if (!value) {
                    this.setState({ loading: true, dataPriceGroup: [], filterPriceGroup: resetFilter, pagePriceGroup: resetPage }, () => {
                        setTimeout(() => this._initiatePriceGroup(this.state.pagePriceGroup, this.state.limitPriceGroup, this.state.filterPriceGroup), 256);
                    });
                }
                break;

            case "productcategory":
                clearTimeout(this.inputTimer);

                if (value.length >= 3) {
                    this.setState({ loading: true, dataProductCategory: [], filterProductCategory: "name like " + value, pageProductCategory: resetPage }, () => {
                        this.inputTimer = setTimeout(() => this._initiateProductCategory(this.state.pageProductCategory, this.state.limitProductCategory, this.state.filterProductCategory), 256);
                    });
                } else if (!value) {
                    this.setState({ loading: true, dataProductCategory: [], filterProductCategory: resetFilter, pageProductCategory: resetPage }, () => {
                        setTimeout(() => this._initiateProductCategory(this.state.pageProductCategory, this.state.limitProductCategory, this.state.filterProductCategory), 256);
                    });
                }
                break;

            default:
                break;
        }
    };

    _initiate = async () => {
        prepareComponent(this.props.dispatch, true);
        await this.setState({
            dataMappingNexseller: [],
            dataGeoTree: [],
            dataPriceGroup: [],
            dataProductCategory: [],
        });

        await this._getDataInitiate();

        setTimeout(() => prepareComponent(this.props.dispatch, false), 128);
        console.log("masuk12345");
    }

    _onFinish = async (value) => {
        //untuk country
        // const data = {
        //     country_id: parseInt(value.country_id),
        //     code: value.code,
        //     name: value.name,
        // };

        console.log("values", value);

        await this.setState(state => {
            const values = state.values.map((item, index) => {
                if (index === this.state.comp - 1) {
                    return value;
                } else {
                    return item;
                }
            });

            return {
                values,
            };
        });

        if (this.state.comp === 1) {
            await this._initiate();
        }

        // pengolahan contact person

        let contactPersonList = [];

        if (this.state.comp === 4) {
            let result = Object.keys(value)
                .map(key => ({ name: key, data: value[key] }));

            console.log(result);

            let k = 0;
            for (let j = 0; j < this.state.jumlahContactPerson; j++) {

                contactPersonList[j] = {
                    "nik": result[k++].data,
                    "title": result[k++].data,
                    "first_name": result[k++].data,
                    "last_name": result[k++].data,
                    "address": result[k++].data,
                    "country": result[k++].data,
                    "email": result[k++].data,
                    "phone_country_code": result[k++].data,
                    "phone": result[k++].data,
                    "position_id": result[k++].data,
                    "join_date": result[k++].data,
                    "resign_date": result[k++].data,
                }

            }

            console.log(contactPersonList);
        }

        // -------------------------------------------------------------------

        console.log("state values", this.state.values);
        if (this.state.comp !== 4) {
            this._nextComp(this.state.comp);
        }
        else {
            const data = {
                // nexchief_account_id:
                // company_profile.id:

                // nexseller_code:
                // nexseller_parent_code:
                // active_date
                // resign_date
                // geo_tree_id
                // price_group_id
                // product_category_id
                // product_class_from_id
                // product_class_thru_id
                // send_sync_data_to_email
                // nd6_sync_method
                // product_mapping:
                // salesman_mapping:
                // customer_mapping:
                // hosting_only:
                // sync_delivery_note:
                // nd6_company_id
                // nd6_branch_id
                // nexmile_version
                // nd6_version
                // product_master_version
                // additional_info -> mungkin utk mapping field

                // socket_user_id
                // socket_password
                // socket_status
                // last_dms_sync
                // last_sfa_sync
                // gromart_merchant_id
                // prefix_deleted
                // nd6_closed_date
                // email_to
                // email_cc_to

                // person_profile_id
                // nik
                // first_name
                // last_name
                // address_1
                // country_id
                // email
                // phone_country_code
                // phone
                // position_id
                // join_date
                // resign_date

            }
        }
    }

    _onAddContact = () => {
        this.setState({ jumlahContactPerson: this.state.jumlahContactPerson + 1 });
    }

    _changeIndexContactPersonActive = (i) => {
        this.setState({ indexContactPersonActive: i });
    }

    // _onFinish = async (value) => {
    //     // const data = {
    //     //     urban_village_id: parseInt(value.urbanvillage_id),
    //     //     code: value.code,
    //     // };

    //     // const { token, refresh_token, dispatch, history } = this.props;
    //     // if (this.props.permission.insert || this.props.permission.insert_own) {
    //     //     await PostalCodeService.doInsert(token, refresh_token, data, dispatch, () => history.push(ROUTER_CONSTANT.POSTAL_CODE.LIST));
    //     // }
    //     console.log("tes");
    // };

    render() {
        // console.log("comp profile", this.props.location.state.company_profile);
        console.log("state loading", this.state.loading);

        const rules = rulesMappingNexseller(this.props.t);

        const AutoFillNotification = () => (
            <div className={style.GridItemLuar}>
                <div className={style.AutoFillContainer}>
                    <div className={style.AutoFill}>
                        {this.props.t("MAPPING_NEXSELLER:FIELD_ADD_FORM")}
                    </div>
                    <div className={style.AutoFillNotif}>
                        {this.props.t("MAPPING_NEXSELLER:FIELD_AUTO_FILL_NPWP")}
                    </div>
                </div>
            </div>
        );

        const ContactPerson = props => (
            <div hidden={this.state.indexContactPersonActive === props.iContact ? false : true}>
                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_NIK")}</label>
                <Form.Item className={style.FormItem} name={"nik" + [props.iContact]} rules={rules.nik}>
                    <Input className={style.Input} id={"nik" + props.iContact} suffix={<ButtonCheck onCheck={() => this._onCheck(props.iContact)} />} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("USER:FIELD_TITLE")}</label>
                {/* TODO: Ambil dari API */}
                <Form.Item className={style.FormItem} name={"title" + props.iContact}>
                    <Select className={style.Select} defaultValue="Mr" suffixIcon={<ChevronDown />}   >
                        <Option value="Mr">Mr</Option>
                        <Option value="Mrs">Mrs</Option>
                        <Option value="Ms">Ms</Option>
                    </Select>
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_FIRST_NAME")}</label>
                <Form.Item className={style.FormItem} name={"first_name" + props.iContact} rules={rules.first_name}>
                    <Input className={style.Input} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_LAST_NAME")}</label>
                <Form.Item className={style.FormItem} name={"last_name" + props.iContact} rules={rules.last_name}>
                    <Input className={style.Input} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_ADDRESS")}</label>
                <Form.Item className={style.FormItem} name={"address" + props.iContact} rules={rules.address}>
                    <Input.TextArea className={style.Input} autoSize={{ minRows: 1, maxRows: 6 }} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_COUNTRY")}</label>
                <Form.Item className={style.FormItem} name={"country" + props.iContact} rules={rules.country_id}>
                    <Select className={style.Select} suffixIcon={<ChevronDown />}
                        onPopupScroll={(event) => this._onScroll(event, "country")}
                        dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "country"))}
                        allowClear={true}
                    >
                        {!this.state.loading ? this.state.dataCountry : [...this.state.dataCountry, LoadingOption()]}
                    </Select>
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_EMAIL")}</label>
                <Form.Item className={style.FormItem} name={"email" + props.iContact} rules={rules.email}>
                    <Input className={style.Input} />
                </Form.Item>

                <Form.Item className={style.FormItem} required={true}>
                    <div style={{ display: "grid", width: "60%", gridTemplateColumns: "30% 70%" }}>
                        <div style={{ display: "inline-block" }}>
                            <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_COUNTRY_CODE")}</label>
                            <Form.Item name={"phone_country_code" + props.iContact} rules={rules.phone_country_code}>
                                <Input className={style.Input} style={{ width: "90%" }} />
                            </Form.Item>
                        </div>

                        <div>
                            <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_CORPORATE_PHONE")}</label>
                            <Form.Item name={"phone" + props.iContact} rules={rules.phone}>
                                <Input className={style.InputPhone} />
                            </Form.Item>
                        </div>
                    </div>
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_POSITION")}</label>
                <Form.Item className={style.FormItem} name={"position_id" + props.iContact} rules={rules.position}>
                    <Select className={style.Select} suffixIcon={<ChevronDown />}
                    // onPopupScroll={(event) => this._onScroll(event, "country")}
                    // dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "country"))}
                    // allowClear={true}
                    >
                        {/* {!this.state.loading ? this.state.dataCountry : [...this.state.dataCountry, LoadingOption()]} */}
                    </Select>
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_JOIN_DATE")}</label>
                <Form.Item className={style.FormItem} name={"join_date" + props.iContact} rules={rules.join_date}>
                    <DatePicker
                        format={"DD-MM-YYYY"}
                        suffixIcon={<DatePickerLogo />}
                        className={style.Input}
                        allowClear={false}
                    />
                    {/* <Input className={style.Input} defaultValue={this.state.join_date}/> */}
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_RESIGN_DATE")}</label>
                <Form.Item className={style.FormItem} name={"resign_date" + props.iContact} rules={rules.resign_date}>
                    <DatePicker
                        format={"DD-MM-YYYY"}
                        suffixIcon={<DatePickerLogo />}
                        className={style.Input}
                        allowClear={false}
                    />
                </Form.Item>

            </div>
        );

        return (
            <div>
                <div className={style.MenuTitle}>{this.props.t("MAPPING_NEXSELLER:TITLE")} &#62; {this.props.t("MAPPING_NEXSELLER:ADD.TITLE")}</div>
                <div className={style.Title}>
                    <div>{this.props.t("MAPPING_NEXSELLER:ADD.TITLE")}</div>
                </div>

                {/* container luar */}
                <div style={{ marginBottom: "20px", paddingLeft: "55px", paddingRight: "55px", paddingBottom: "55px", position: "absolute", width: "100%" }}>
                    <div className={style.List}>
                        {/* container luar */}
                        <div style={{ alignItems: "center" }}>
                            <div className={style.HeadTable}>
                                <div className={style.GridContainerHead}>
                                    <div className={this.state.comp === 1 || this.state.comp === 2 || this.state.comp === 3 || this.state.comp === 4 ? style.GridItemHeadActive : style.GridItemHead} style={{ borderRight: "1px solid #CCCCCC" }}>
                                        <div className={this.state.comp === 1 || this.state.comp === 2 || this.state.comp === 3 || this.state.comp === 4 ? style.NumberHeadActive : style.NumberHead}>1</div>
                                        <div className={style.Step}>{this.props.t("MAPPING_NEXSELLER:FIELD_COMPANY_PROFILE")}</div>
                                    </div>

                                    <div className={this.state.comp === 2 || this.state.comp === 3 || this.state.comp === 4 ? style.GridItemHeadActive : style.GridItemHead} style={{ borderRight: "1px solid #CCCCCC" }}>
                                        <div className={this.state.comp === 2 || this.state.comp === 3 || this.state.comp === 4 ? style.NumberHeadActive : style.NumberHead}>2</div>
                                        <div className={style.Step}>Account</div>
                                    </div>

                                    <div className={this.state.comp === 3 || this.state.comp === 4 ? style.GridItemHeadActive : style.GridItemHead} style={{ borderRight: "1px solid #CCCCCC" }}>
                                        <div className={this.state.comp === 3 || this.state.comp === 4 ? style.NumberHeadActive : style.NumberHead}>3</div>
                                        <div className={style.Step}>Socket</div>
                                    </div>

                                    <div className={this.state.comp === 4 ? style.GridItemHeadActive : style.GridItemHead}>
                                        <div className={this.state.comp === 4 ? style.NumberHeadActive : style.NumberHead}>4</div>
                                        <div className={style.Step}>Contact Person</div>
                                    </div>

                                </div>
                            </div>

                            <div className={style.SubTitle}>
                                <div>
                                    {this.state.comp === 1 && this.props.t("MAPPING_NEXSELLER:FIELD_COMPANY_PROFILE")}
                                    {this.state.comp === 2 && this.props.t("MAPPING_NEXSELLER:FIELD_ACCOUNT")}
                                    {this.state.comp === 3 && this.props.t("MAPPING_NEXSELLER:FIELD_SOCKET")}
                                    {this.state.comp === 4 && this.props.t("MAPPING_NEXSELLER:FIELD_CONTACT_PERSON")}
                                </div>
                            </div>

                            <Form style={{ marginTop: "30px" }} id="form" onFinish={this._onFinish} colon={false}
                                initialValues={{
                                    product_mapping: false,
                                    salesman_mapping: false,
                                    customer_mapping: false,
                                    hosting_only: false,
                                    sync_delivery_note: false,
                                    company_type: this.props.location.state.company_profile.company_title_name ? this.props.location.state.company_profile.company_title_name : "-",
                                    company_name: this.props.location.state.company_profile.name ? this.props.location.state.company_profile.name : "-",
                                    phone_country_code: this.props.location.state.company_profile.phone ? this.props.location.state.company_profile.phone.substring(0, 3) : "-",
                                    phone: this.props.location.state.company_profile.phone ? this.props.location.state.company_profile.phone.substring(4) : "-",
                                    fax_country_code: this.props.location.state.company_profile.fax ? this.props.location.state.company_profile.fax.substring(0, 3) : "-",
                                    fax: this.props.location.state.company_profile.fax ? this.props.location.state.company_profile.fax.substring(4) : "-",
                                    email: this.props.location.state.company_profile.email ? this.props.location.state.company_profile.email : "-",
                                    npwp: this.props.location.state.company_profile.npwp ? this.props.location.state.company_profile.npwp : "-",
                                    address: this.props.location.state.company_profile.address_1 ? this.props.location.state.company_profile.address_1 : "-",
                                    country: this.props.location.state.company_profile.country_name ? this.props.location.state.company_profile.country_name : "-",
                                    district: this.props.location.state.company_profile.district_name ? this.props.location.state.company_profile.district_name : "-",
                                    sub_district: this.props.location.state.company_profile.sub_district_name ? this.props.location.state.company_profile.sub_district_name : "-",
                                    urban_village: this.props.location.state.company_profile.urban_village_name ? this.props.location.state.company_profile.urban_village_name : "-",
                                    hamlet: this.props.location.state.company_profile.hamlet ? this.props.location.state.company_profile.hamlet : "-",
                                    neighbourhood: this.props.location.state.company_profile.neighbourhood ? this.props.location.state.company_profile.neighbourhood : "-",
                                    postal_code: this.props.location.state.company_profile.postal_code ? this.props.location.state.company_profile.postal_code : "-",
                                    island: this.props.location.state.company_profile.island_name ? this.props.location.state.company_profile.island_name : "-",
                                    company_logo: this.props.location.state.company_profile.logo
                                }}
                            >

                                {this.state.comp === 1 &&
                                    <div>
                                        <div className={style.GridContainerLuar}>
                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_NEXCHIEF_ACCOUNT_NAME")}</label>
                                                <Form.Item className={style.FormItem} name="nexchief_account_name" rules={rules.nexchief_account_name}>
                                                    <Select className={style.Select2} suffixIcon={<ChevronDown />}
                                                        onPopupScroll={(event) => this._onScroll(event, "nexchiefaccount")}
                                                        dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "nexchiefaccount"))}
                                                        allowClear={true}
                                                    >
                                                        {!this.state.loading ? this.state.dataNexchiefAccount : [...this.state.dataNexchiefAccount, LoadingOption()]}
                                                    </Select>
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_COMPANY_TYPE")}</label>
                                                <Form.Item className={style.FormItem} name="company_type">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_COMPANY_NAME")}</label>
                                                <Form.Item className={style.FormItem} name="company_name">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <Form.Item required={true}>
                                                    <div style={{ display: "grid", width: "75%", gridTemplateColumns: "30% 70%" }}>
                                                        <div style={{ display: "inline-block" }}>
                                                            <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_COUNTRY_CODE")}</label>
                                                            <Form.Item name="phone_country_code">
                                                                <Input className={style.InputDisabled} style={{ width: "90%" }} disabled />
                                                            </Form.Item>
                                                        </div>

                                                        <div>
                                                            <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_CORPORATE_PHONE")}</label>
                                                            <Form.Item name="phone">
                                                                <Input className={style.InputPhoneDisabled} disabled />
                                                            </Form.Item>
                                                        </div>
                                                    </div>
                                                </Form.Item>

                                                <Form.Item required={true}>
                                                    <div style={{ display: "grid", width: "75%", gridTemplateColumns: "30% 70%" }}>
                                                        <div style={{ display: "inline-block" }}>
                                                            <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_COUNTRY_CODE")}</label>
                                                            <Form.Item name="fax_country_code">
                                                                <Input className={style.InputDisabled} style={{ width: "90%" }} disabled />
                                                            </Form.Item>
                                                        </div>

                                                        <div style={{ display: "inline-block" }}>
                                                            <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_FAX_NUMBER")}</label>
                                                            <Form.Item name="fax" >
                                                                <Input className={style.InputPhoneDisabled} disabled />
                                                            </Form.Item>
                                                        </div>
                                                    </div>

                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_EMAIL")}</label>
                                                <Form.Item className={style.FormItem} name="email">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_NPWP")}</label>
                                                <Form.Item className={style.FormItem} name="npwp">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_COMPANY_LOGO")}</label>
                                                <Form.Item className={style.FormItem} name="company_logo">
                                                    <CustomImage logo={this.props.location.state.company_profile && this.props.location.state.company_profile.logo} />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_ADDRESS")}</label>
                                                <Form.Item className={style.FormItem} name="address">
                                                    <Input.TextArea className={style.InputDisabled} autoSize={{ minRows: 1, maxRows: 6 }} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_COUNTRY")}</label>
                                                <Form.Item className={style.FormItem} name="country">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_DISTRICT")}</label>
                                                <Form.Item className={style.FormItem} name="district">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_SUB_DISTRICT")}</label>
                                                <Form.Item className={style.FormItem} name="sub_district">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_URBAN_VILLAGE")}</label>
                                                <Form.Item className={style.FormItem} name="urban_village">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_HAMLET")}</label>
                                                <Form.Item className={style.FormItem} name="hamlet">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_NEIGHBOURHOOD")}</label>
                                                <Form.Item className={style.FormItem} name="neighbourhood">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_POSTAL_CODE")}</label>
                                                <Form.Item className={style.FormItem} name="postal_code">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_ISLAND")}</label>
                                                <Form.Item className={style.FormItem} name="island">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>
                                            </div>
                                            <AutoFillNotification />
                                        </div>

                                        <Form.Item className={style.ButtonArea}>
                                            <ButtonCancel router={ROUTER_CONSTANT.MAPPING_NEXSELLER.LIST} />
                                            <div style={{ display: "contents" }}>
                                                <ButtonNext />
                                            </div>
                                        </Form.Item>
                                    </div>
                                }

                                {/* Account */}
                                {this.state.comp === 2 &&
                                    <div>
                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_NEXSELLER_CODE")}</label>
                                        <Form.Item className={style.FormItem} name="nexseller_code" rules={rules.nexseller_code}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_NEXSELLER_PARENT_CODE")}</label>
                                        <Form.Item className={style.FormItem} name="nexseller_parent_code" rules={rules.nexseller_parent_code}>
                                            <Select className={style.Select} suffixIcon={<ChevronDown />}
                                                onPopupScroll={(event) => this._onScroll(event, "mappingnexseller")}
                                                dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "mappingnexseller"))}
                                                allowClear={true}
                                            >
                                                {!this.state.loading ? this.state.dataMappingNexseller : [...this.state.dataMappingNexseller, LoadingOption()]}
                                            </Select>
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_ACTIVATION_DATE")}</label>
                                        <Form.Item className={style.FormItem} name="activation_date" rules={rules.activation_date}>
                                            <DatePicker
                                                // defaultValue={moment(moment(this.state.join_date).format("DD-MM-YYYY"), "DD-MM-YYYY")}
                                                format={"DD-MM-YYYY"}
                                                suffixIcon={<DatePickerLogo />}
                                                className={style.Input}
                                                allowClear={false}
                                            />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_RESIGNATION_DATE")}</label>
                                        <Form.Item className={style.FormItem} name="resignation_date" rules={rules.resignantion_date}>
                                            <DatePicker
                                                // defaultValue={moment(moment(this.state.join_date).format("DD-MM-YYYY"), "DD-MM-YYYY")}
                                                format={"DD-MM-YYYY"}
                                                suffixIcon={<DatePickerLogo />}
                                                className={style.Input}
                                                allowClear={false}
                                            />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_GEO_TREE")}</label>
                                        <Form.Item className={style.FormItem} name="geo_tree" rules={rules.geo_tree}>
                                            <Select className={style.Select} suffixIcon={<ChevronDown />}
                                                onPopupScroll={(event) => this._onScroll(event, "geotree")}
                                                dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "geotree"))}
                                                allowClear={true}
                                            >
                                                {!this.state.loading ? this.state.dataGeoTree : [...this.state.dataGeoTree, LoadingOption()]}
                                            </Select>
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_PRICE_GROUP")}</label>
                                        <Form.Item className={style.FormItem} name="price_group_id" rules={rules.price_group_id}>
                                            <Select className={style.Select} suffixIcon={<ChevronDown />}
                                                onPopupScroll={(event) => this._onScroll(event, "pricegroup")}
                                                dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "pricegroup"))}
                                                allowClear={true}
                                            >
                                                {!this.state.loading ? this.state.dataPriceGroup : [...this.state.dataPriceGroup, LoadingOption()]}
                                            </Select>
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_PRODUCT_CATEGORY")}</label>
                                        <Form.Item className={style.FormItem} name="product_category_id" rules={rules.product_category_id}>
                                            <Select className={style.Select} suffixIcon={<ChevronDown />}
                                                onPopupScroll={(event) => this._onScroll(event, "productcategory")}
                                                dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "productcategory"))}
                                                allowClear={true}
                                            >
                                                {!this.state.loading ? this.state.dataProductCategory : [...this.state.dataProductCategory, LoadingOption()]}
                                            </Select>
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_PRODUCT_CLASS_FROM")}</label>
                                        <Form.Item className={style.FormItem} name="product_class_from_id" rules={rules.product_class_from_id}>
                                            <Select className={style.Select} suffixIcon={<ChevronDown />}>
                                                <Option value="1">1</Option>
                                                <Option value="2">2</Option>
                                                <Option value="3">3</Option>
                                                <Option value="4">4</Option>
                                                <Option value="5">5</Option>
                                                <Option value="6">6</Option>
                                                <Option value="7">7</Option>
                                                <Option value="8">8</Option>
                                                <Option value="9">9</Option>
                                            </Select>
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_PRODUCT_CLASS_THRU")}</label>
                                        <Form.Item className={style.FormItem} name="product_class_thru_id" rules={rules.product_class_thru_id}>
                                            <Select className={style.Select} suffixIcon={<ChevronDown />}>
                                                <Option value="1">1</Option>
                                                <Option value="2">2</Option>
                                                <Option value="3">3</Option>
                                                <Option value="4">4</Option>
                                                <Option value="5">5</Option>
                                                <Option value="6">6</Option>
                                                <Option value="7">7</Option>
                                                <Option value="8">8</Option>
                                                <Option value="9">9</Option>
                                            </Select>
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_SEND_SYNC_DATA_TO_EMAIL")}</label>
                                        <Form.Item className={style.FormItem} name="send_sync_data_to_email" rules={rules.send_sync_data_to_email}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_ND6_SYNC_METHOD")}</label>
                                        <Form.Item className={style.FormItem} name="nd6_sync_method" rules={rules.nd6_sync_method}>
                                            <Select className={style.Select} suffixIcon={<ChevronDown />} placeholder={this.props.t("MAPPING_NEXSELLER:FIELD_ND6_SYNC_METHOD")}
                                            >
                                                <Option value="B">Inbound and Outbound</Option>
                                                <Option value="I">Inbound Only (From ND6)</Option>
                                            </Select>
                                        </Form.Item>

                                        <div className={style.SubTitle} style={{ marginBottom: "30px" }}>{this.props.t("MAPPING_NEXSELLER:FIELD_ACCOUNT_CONTROL")}</div>

                                        <div className={style.GridAccountControl}>
                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_PRODUCT_MAPPING")}</label>
                                                <Form.Item className={style.FormItem} name="product_mapping" rules={rules.product_mapping}>
                                                    <Switch
                                                        checkedChildren="Yes"
                                                        unCheckedChildren="No"
                                                        style={{ height: "32px", borderRadius: "4px", width: "60px", alignItems: "center", marginTop: "5px" }}
                                                    />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_SALESMAN_MAPPING")}</label>
                                                <Form.Item className={style.FormItem} name="salesman_mapping" rules={rules.salesman_mapping}>
                                                    <Switch
                                                        checkedChildren="Yes"
                                                        unCheckedChildren="No"
                                                        style={{ height: "32px", borderRadius: "4px", width: "60px", alignItems: "center", marginTop: "5px" }}
                                                    />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_CUSTOMER_MAPPING")}</label>
                                                <Form.Item className={style.FormItem} name="customer_mapping" rules={rules.customer_mapping}>
                                                    <Switch
                                                        checkedChildren="Yes"
                                                        unCheckedChildren="No"
                                                        style={{ height: "32px", borderRadius: "4px", width: "60px", alignItems: "center", marginTop: "5px" }}
                                                    />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_HOSTING_ONLY")}</label>
                                                <Form.Item className={style.FormItem} name="hosting_only" rules={rules.hosting_only}>
                                                    <Switch
                                                        checkedChildren="Yes"
                                                        unCheckedChildren="No"
                                                        style={{ height: "32px", borderRadius: "4px", width: "60px", alignItems: "center", marginTop: "5px" }}
                                                    />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_SYNC_DELIVERY_NOTE")}</label>
                                                <Form.Item className={style.FormItem} name="sync_delivery_note" rules={rules.sync_delivery_note}>
                                                    <Switch
                                                        checkedChildren="Yes"
                                                        unCheckedChildren="No"
                                                        style={{ height: "32px", borderRadius: "4px", width: "60px", alignItems: "center", marginTop: "5px" }}
                                                    />
                                                </Form.Item>
                                            </div>
                                        </div>

                                        <div className={style.SubTitle} style={{ marginBottom: "30px" }}>{this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING")}</div>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_ND6_COMPANY_ID")}</label>
                                        <Form.Item className={style.FormItem} name="nd6_company_id" rules={rules.nd6_company_id}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_ND6_BRANCH_ID")}</label>
                                        <Form.Item className={style.FormItem} name="nd6_branch_id" rules={rules.nd6_branch_id}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_NEXMILE_VERSION")}</label>
                                        <Form.Item className={style.FormItem} name="nexmile_version" rules={rules.nexmile_version}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_ND6_VERSION")}</label>
                                        <Form.Item className={style.FormItem} name="nd6_version" rules={rules.nd6_version}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_PRODUCT_MASTER_VERSION")}</label>
                                        <Form.Item className={style.FormItem} name="product_master_version" rules={rules.product_master_version}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <div className={style.GridAccountControl}>
                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_1")}</label>
                                                <Form.Item className={style.FormItem} name="mapping_field_1" rules={rules.nexmile_version}>
                                                    <Input className={style.Input} />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_2")}</label>
                                                <Form.Item className={style.FormItem} name="mapping_field_2" rules={rules.nexmile_version}>
                                                    <Input className={style.Input} />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_3")}</label>
                                                <Form.Item className={style.FormItem} name="mapping_field_3" rules={rules.nexmile_version}>
                                                    <Input className={style.Input} />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_4")}</label>
                                                <Form.Item className={style.FormItem} name="mapping_field_4" rules={rules.nexmile_version}>
                                                    <Input className={style.Input} />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_5")}</label>
                                                <Form.Item className={style.FormItem} name="mapping_field_5" rules={rules.nexmile_version}>
                                                    <Input className={style.Input} />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_6")}</label>
                                                <Form.Item className={style.FormItem} name="mapping_field_6" rules={rules.nexmile_version}>
                                                    <Input className={style.Input} />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_7")}</label>
                                                <Form.Item className={style.FormItem} name="mapping_field_7" rules={rules.nexmile_version}>
                                                    <Input className={style.Input} />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_8")}</label>
                                                <Form.Item className={style.FormItem} name="mapping_field_8" rules={rules.nexmile_version}>
                                                    <Input className={style.Input} />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_9")}</label>
                                                <Form.Item className={style.FormItem} name="mapping_field_9" rules={rules.nexmile_version}>
                                                    <Input className={style.Input} />
                                                </Form.Item>
                                            </div>

                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_MAPPING_FIELD_10")}</label>
                                                <Form.Item className={style.FormItem} name="mapping_field_10" rules={rules.nexmile_version}>
                                                    <Input className={style.Input} />
                                                </Form.Item>
                                            </div>
                                        </div>
                                        <Form.Item className={style.ButtonArea}>
                                            <div onClick={() => this._prevComp(this.state.comp)} style={{ display: "contents" }}>
                                                <ButtonBack />
                                            </div>
                                            <div style={{ display: "contents" }}>
                                                <ButtonNext />
                                            </div>
                                        </Form.Item>
                                    </div>
                                }

                                {/* Socket */}
                                {this.state.comp === 3 &&
                                    <div>
                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_SOCKET_USER_ID")}</label>
                                        <Form.Item className={style.FormItem} name="socket_user_id" rules={rules.socket_user_id}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_SOCKET_PASSWORD")}</label>
                                        <Form.Item className={style.FormItem} name="socket_password" rules={rules.socket_password}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_SOCKET_STATUS")}</label>
                                        <Form.Item name="socket_status" rules={rules.socket_status}>
                                            <DatePicker
                                                // defaultValue={moment(moment(this.state.join_date).format("DD-MM-YYYY"), "DD-MM-YYYY")}
                                                format={"DD-MM-YYYY"}
                                                suffixIcon={<DatePickerLogo />}
                                                className={style.Input}
                                                allowClear={false}
                                            />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_LAST_DMS_SYNC")}</label>
                                        <Form.Item className={style.FormItem} name="last_dms_sync" rules={rules.last_dms_sync}>
                                            <DatePicker
                                                // defaultValue={moment(moment(this.state.join_date).format("DD-MM-YYYY"), "DD-MM-YYYY")}
                                                format={"DD-MM-YYYY"}
                                                suffixIcon={<DatePickerLogo />}
                                                className={style.Input}
                                                allowClear={false}
                                            />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_LAST_SFA_SYNC")}</label>
                                        <Form.Item className={style.FormItem} name="last_sfa_sync" rules={rules.last_sfa_sync}>
                                            <DatePicker
                                                // defaultValue={moment(moment(this.state.join_date).format("DD-MM-YYYY"), "DD-MM-YYYY")}
                                                format={"DD-MM-YYYY"}
                                                suffixIcon={<DatePickerLogo />}
                                                className={style.Input}
                                                allowClear={false}
                                            />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_GROMART_MERCHANT_ID")}</label>
                                        <Form.Item className={style.FormItem} name="gromart_merchant_id" rules={rules.gromart_merchant_id}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_PREFIX_DELETED")}</label>
                                        <Form.Item className={style.FormItem} name="prefix_deleted" rules={rules.prefix_deleted}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_ND6_CLOSE_DATE")}</label>
                                        <Form.Item className={style.FormItem} name="nd6_close_date" rules={rules.nd6_close_date}>
                                            <DatePicker
                                                // defaultValue={moment(moment(this.state.join_date).format("DD-MM-YYYY"), "DD-MM-YYYY")}
                                                format={"DD-MM-YYYY"}
                                                suffixIcon={<DatePickerLogo />}
                                                className={style.Input}
                                                allowClear={false}
                                            />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_EMAIL_TO")}</label>
                                        <Form.Item className={style.FormItem} name="email_to" rules={rules.email_to}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_EMAIL_CC_TO")}</label>
                                        <Form.Item className={style.FormItem} name="email_cc_to" rules={rules.email_cc_to}>
                                            <Input className={style.Input} />
                                        </Form.Item>

                                        <Form.Item className={style.ButtonArea}>
                                            <div onClick={() => this._prevComp(this.state.comp)} style={{ display: "contents" }}>
                                                <ButtonBack />
                                            </div>
                                            <div style={{ display: "contents" }}>
                                                <ButtonNext />
                                            </div>
                                        </Form.Item>
                                    </div>
                                }
                                {this.state.comp === 4 &&
                                    <div>
                                        {/* <ButtonContactPerson1 /> */}
                                        {/* {Array.from({ length: this.state.jumlahContactPerson }, (_, i) => 
                                        <div><ButtonContactPersonDst index={i + 1} /> <ContactPerson /></div>)} */}
                                        {Array.from({ length: this.state.jumlahContactPerson }, (_, i) =>
                                            <ButtonContactPersonDst index={i + 1} indexActive={this.state.indexContactPersonActive} onChangeActive={() => this._changeIndexContactPersonActive(i + 1)} />)}
                                        <ButtonAddContactPerson onAddContact={() => this._onAddContact()} />

                                        {Array.from({ length: this.state.jumlahContactPerson }, (_, i) =>
                                            <ContactPerson iContact={i + 1} />
                                        )}

                                        <Form.Item className={style.ButtonArea}>
                                            <div onClick={() => this._prevComp(this.state.comp)} style={{ display: "contents" }}>
                                                <ButtonBack />
                                            </div>
                                            <div style={{ display: "contents" }}>
                                                <ButtonSubmit />
                                            </div>
                                        </Form.Item>
                                    </div>
                                }

                            </Form>

                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ...stateRedux(state, "mapping_nexseller"),
    };

};

const mapDispatchToProps = (dispatch) => {
    return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(AddMappingNexseller));