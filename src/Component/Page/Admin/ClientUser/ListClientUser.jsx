import React, { Component } from 'react';
import ButtonCollapseList from '../../../Common/Button/ButtonCollapseList';
import style from "./ListClientUser.module.css";
import ButtonAdd from "../../../Common/Button/ButtonAdd";
import { RANDOM_ID } from "../../../../Enum/PropertiesConstant";
import { ROUTER_CONSTANT } from "../../../../Enum/RouterConstant";
import { withTranslation } from "react-i18next";

class ListClientUser extends Component {
    state = {
        collapsed: [true, true, true],
    };

    _onCollapse = i => {
        this.setState(state => {
            const collapsed = state.collapsed.map((item, index) => {
                if (index === i) {
                    return !item;
                } else {
                    return true;
                }
            });

            return {
                collapsed,
            };
        });
    };

    render() {
        console.log("state", this.state);
        return (
            <div style={{ marginBottom: "20px", paddingLeft: "55px", paddingRight: "55px", paddingBottom: "55px", position: "absolute", width: "100%" }}>
                <div className={style.Title}>
                    <div>{this.props.t("USER:TITLE")}</div>
                    <ButtonAdd
                        key={RANDOM_ID(8)}
                        router={ROUTER_CONSTANT.CLIENT_USER.ADD}
                        text={this.props.t("USER:TITLE")}
                    // disabled={this.props.permission.insert ? false : !this.props.permission.insert_own}
                    />
                </div>

                {/* <Filter
                    {...this.props}
                    state={this.state}
                    permission={this.props.permission}
                    initiateList={this.state.initiate_list}
                    withHelp
                    function={{
                        onOrder: this._onOrder,
                        onChange: this._onChange,
                        onFilter: this._onFilter,
                        onReset: this._resetTable,
                    }}
                /> */}

                <ul style={{ listStyleType: "none", paddingLeft: "55px", paddingRight: "55px", position:"absolute", width: "100%"}}>
                    {this.state.collapsed.map((item, index) => (
                        <li key={index}>
                            <div style={{ marginBottom: "20px" }}>
                                <div className={style.List}>
                                    <div style={{ display: "flex", alignItems: "center", marginTop: "3px", marginBottom: "auto" }}>
                                        <div style={{ marginRight: "10px", textAlign: "right" }}>
                                            <div className={style.Id}>123456</div>
                                            <div className={style.NexchiefAccount}>PT. Nexchief Account</div>
                                        </div>
                                        <div style={{marginTop:"auto", marginLeft:"15px"}}>
                                            <button className={style.ButtonStatus}>Active</button>
                                        </div>
                                        <ButtonCollapseList collapsed={item} onCollapse={() => this._onCollapse(index)} />
                                    </div>
                                </div>
                                <div className={style.ListBody} style={item ? { height: "0px" } : { height: "200px", borderTop: "inset rgb(0 0 0 / 12%)", borderWidth:"thin", padding:"20px"}}>
                                    <div className={style.HeadTable}>
                                        <div className={style.GridContainerHead}>
                                            <div className={style.GridItemHead}>ID</div>
                                            <div className={style.GridItemHead}>User Name</div>
                                            <div className={style.GridItemHead}>Name</div>
                                            <div className={style.GridItemHead}>Data Group</div>
                                        </div>
                                    </div>

                                    <div className={style.HeadTable}>
                                        <div className={style.GridContainer}>
                                            <div className={style.GridItem}>123456</div>
                                            <div className={style.GridItem}>Jane</div>
                                            <div className={style.GridItem}>setup:view</div>
                                            <div className={style.GridItem}></div>
                                            <div className={style.GridItem} style={{ justifyContent: "right" }}><a className={style.LinkViewDetail}>View Detail</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>

            </div>
            
        );
    }
}

export default (withTranslation()(ListClientUser));