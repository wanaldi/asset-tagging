import React, { Component } from 'react';
import style from "./AddClientUser.module.css";
import { withTranslation } from "react-i18next";
import { rulesUser } from '../../../../Util/FormRules';
import { Form, Input, Select, Upload } from "antd";
import ButtonCancel from "../../../Common/Button/ButtonCancel";
import ButtonNext from "../../../Common/Button/ButtonNext";
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { postalcode, subdistrict, urbanvillage } from "../../../../Enum/PropertiesConstant";
import { concatPrefix, generatePrefix, getInitiate, getList } from "../../../../Service/Util";
import { prepareComponent } from '../../../../Action/CommonAction';
import { connect } from 'react-redux';
import Label from '../../../Common/Label/Label';
import ChevronDown from '../../../Icon/ChevronDown';
const { Option } = Select;

const children = [];
for (let i = 10; i < 36; i++) {
    children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

class AddClientUser extends Component {

    state = {
        comp: 1,
        loading: false,

        ...postalcode,

        disableUpload: false,
        selectedFile: null,
        selectedFileList: [],

        is_pbf: null,
    };

    _onChangeComp = i => {
        console.log("i", i);
        this.setState({ comp : i});
    };

    _receiveFile = (event) => {
        this.setState({ selectedFileList: event.fileList });

        if (Array.isArray(event)) return event;
        return event && event.fileList;
    };

    _dummyRequest = ({ file, onSuccess }) => {
        setTimeout(() => {
            onSuccess("ok");
        }, 0);
    };

    handleChange = info => {
        if (info.file.status === 'uploading') {
            this.setState({ loading: true });
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            getBase64(info.file.originFileObj, imageUrl =>
                this.setState({
                    imageUrl,
                    loading: false,
                }),
            );
        }
    };

    // _initiatePostalCode = async (page, limit, filter) => {
    //     const currentPostalCode = [...this.state.dataPostalCode];
    //     const { token, refresh_token, dispatch } = this.props;

    //     const initiatePostalCode = await getInitiate(token, refresh_token, "postalcode", filter, dispatch);
    //     const previousPostalCode = await getList(token, refresh_token, "postalcode", page, limit, null, filter, dispatch);

    //     if (initiatePostalCode || previousPostalCode) {
    //         if (!previousPostalCode) {
    //             this.setState({ lastPagePostalCode: true });
    //         } else {
    //             if (previousPostalCode.length < limit) {
    //                 this.setState({ lastPagePostalCode: true });
    //             } else {
    //                 this.setState({ lastPagePostalCode: false });
    //             }

    //             this.setState({
    //                 initiatePostalCode,
    //                 dataPostalCode: selectOption(currentPostalCode, previousPostalCode, "value"),
    //                 prefixPostalCode: generatePrefix(initiatePostalCode),
    //             });
    //         }

    //         this.setState({ loading: false });
    //     }
    // };

    _getDataInitiate = async () => {
        // if (this.state.dataCompanyTitle.length === 0) await this._initiateCompanyTitle(this.state.pageCompanyTitle, this.state.limitCompanyTitle, this.state.filterCompanyTitle);
        // if (this.state.dataCountry.length === 0) await this._initiateCountry(this.state.pageCountry, this.state.limitCountry, this.state.filterCountry);
        // if (this.state.dataDistrict.length === 0) await this._initiateDistrict(this.state.pageDistrict, this.state.limitDistrict, this.state.filterDistrict);
        // if (this.state.dataSubDistrict.length === 0) await this._initiateSubDistrict(this.state.pageSubDistrict, this.state.limitSubDistrict, this.state.filterSubDistrict);
        // if (this.state.dataUrbanVillage.length === 0) await this._initiateUrbanVillage(this.state.pageUrbanVillage, this.state.limitUrbanVillage, this.state.filterUrbanVillage);
        if (this.state.dataPostalCode.length === 0) await this._initiatePostalCode(this.state.pagePostalCode, this.state.limitPostalCode, this.state.filterPostalCode);
        // if (this.state.dataIsland.length === 0) await this._initiateIsland(this.state.pageIsland, this.state.limitIsland, this.state.filterIsland);
    };

    // async componentDidMount() {
    //     prepareComponent(this.props.dispatch, true);
    //     await this._getDataInitiate();

    //     setTimeout(() => prepareComponent(this.props.dispatch, false), 128);
    // }

    _onScroll = (event, namespace) => {
        let target = event.target;

        if (!this.state.loading && target.scrollTop + target.offsetHeight === target.scrollHeight) {
            switch (namespace) {
                // case "companytitle":
                //     if (!this.state.lastPageCompanyTitle) {
                //         this.setState({ loading: true, pageCompanyTitle: this.state.pageCompanyTitle + 1 }, () => {
                //             target.scrollTo(0, target.scrollHeight);
                //             setTimeout(() => this._initiateCompanyTitle(this.state.pageCompanyTitle, this.state.limitCompanyTitle, this.state.filterCompanyTitle), 256);
                //         });
                //     }
                //     break;

                // case "country":
                //     if (!this.state.lastPageCountry) {
                //         this.setState({ loading: true, pageCountry: this.state.pageCountry + 1 }, () => {
                //             target.scrollTo(0, target.scrollHeight);
                //             setTimeout(() => this._initiateCountry(this.state.pageCountry, this.state.limitCountry, this.state.filterCountry), 256);
                //         });
                //     }
                //     break;

                // case "district":
                //     if (!this.state.lastPageDistrict) {
                //         this.setState({ loading: true, pageDistrict: this.state.pageDistrict + 1 }, () => {
                //             target.scrollTo(0, target.scrollHeight);
                //             setTimeout(() => this._initiateDistrict(this.state.pageDistrict, this.state.limitDistrict, this.state.filterDistrict), 256);
                //         });
                //     }
                //     break;

                // case "subdistrict":
                //     if (!this.state.lastPageSubDistrict) {
                //         this.setState({ loading: true, pageSubDistrict: this.state.pageSubDistrict + 1 }, () => {
                //             target.scrollTo(0, target.scrollHeight);
                //             setTimeout(() => this._initiateSubDistrict(this.state.pageSubDistrict, this.state.limitSubDistrict, this.state.filterSubDistrict), 256);
                //         });
                //     }
                //     break;

                // case "urbanvillage":
                //     if (!this.state.lastPageUrbanVillage) {
                //         this.setState({ loading: true, pageUrbanVillage: this.state.pageUrbanVillage + 1 }, () => {
                //             target.scrollTo(0, target.scrollHeight);
                //             setTimeout(() => this._initiateUrbanVillage(this.state.pageUrbanVillage, this.state.limitUrbanVillage, this.state.filterUrbanVillage), 256);
                //         });
                //     }
                //     break;

                case "postalcode":
                    if (!this.state.lastPagePostalCode) {
                        this.setState({ loading: true, pagePostalCode: this.state.pagePostalCode + 1 }, () => {
                            target.scrollTo(0, target.scrollHeight);
                            setTimeout(() => this._initiatePostalCode(this.state.pagePostalCode, this.state.limitPostalCode, this.state.filterPostalCode), 256);
                        });
                    }
                    break;

                // case "island":
                //     if (!this.state.lastPageIsland) {
                //         this.setState({ loading: true, pageIsland: this.state.pageIsland + 1 }, () => {
                //             target.scrollTo(0, target.scrollHeight);
                //             setTimeout(() => this._initiateIsland(this.state.pageIsland, this.state.limitIsland, this.state.filterIsland), 256);
                //         });
                //     }
                //     break;

                default:
                    break;
            }
        }
    };

    _onChange = (event, namespace) => {
        const { value } = event.target;

        const resetPage = 1;
        const resetFilter = null;

        switch (namespace) {
            // case "companytitle":
            //     clearTimeout(this.inputTimer);

            //     if (value.length >= 3) {
            //         this.setState({ loading: true, dataCompanyTitle: [], filterCompanyTitle: concatPrefix(this.state.prefixCompanyTitle, value), pageCompanyTitle: resetPage }, () => {
            //             this.inputTimer = setTimeout(() => this._initiateCompanyTitle(this.state.pageCompanyTitle, this.state.limitCompanyTitle, this.state.filterCompanyTitle), 256);
            //         });
            //     } else if (!value) {
            //         this.setState({ loading: true, dataCompanyTitle: [], filterCompanyTitle: resetFilter, pageCompanyTitle: resetPage }, () => {
            //             setTimeout(() => this._initiateCompanyTitle(this.state.pageCompanyTitle, this.state.limitCompanyTitle, this.state.filterCompanyTitle), 256);
            //         });
            //     }
            //     break;

            // case "country":
            //     clearTimeout(this.inputTimer);

            //     if (value.length >= 3) {
            //         this.setState({ loading: true, dataCountry: [], filterCountry: concatPrefix(this.state.prefixCountry, value), pageCountry: resetPage }, () => {
            //             this.inputTimer = setTimeout(() => this._initiateCountry(this.state.pageCountry, this.state.limitCountry, this.state.filterCountry), 256);
            //         });
            //     } else if (!value) {
            //         this.setState({ loading: true, dataCountry: [], filterCountry: resetFilter, pageCountry: resetPage }, () => {
            //             setTimeout(() => this._initiateCountry(this.state.pageCountry, this.state.limitCountry, this.state.filterCountry), 256);
            //         });
            //     }
            //     break;

            // case "district":
            //     clearTimeout(this.inputTimer);

            //     if (value.length >= 3) {
            //         this.setState({ loading: true, dataDistrict: [], filterDistrict: concatPrefix(this.state.prefixDistrict, value), pageDistrict: resetPage }, () => {
            //             this.inputTimer = setTimeout(() => this._initiateDistrict(this.state.pageDistrict, this.state.limitDistrict, this.state.filterDistrict), 256);
            //         });
            //     } else if (!value) {
            //         this.setState({ loading: true, dataDistrict: [], filterDistrict: resetFilter, pageDistrict: resetPage }, () => {
            //             setTimeout(() => this._initiateDistrict(this.state.pageDistrict, this.state.limitDistrict, this.state.filterDistrict), 256);
            //         });
            //     }
            //     break;

            // case "subdistrict":
            //     clearTimeout(this.inputTimer);

            //     if (value.length >= 3) {
            //         this.setState({ loading: true, dataSubDistrict: [], filterSubDistrict: concatPrefix(this.state.prefixSubDistrict, value), pageSubDistrict: resetPage }, () => {
            //             this.inputTimer = setTimeout(() => this._initiateSubDistrict(this.state.pageSubDistrict, this.state.limitSubDistrict, this.state.filterSubDistrict), 256);
            //         });
            //     } else if (!value) {
            //         this.setState({ loading: true, dataSubDistrict: [], filterSubDistrict: resetFilter, pageSubDistrict: resetPage }, () => {
            //             setTimeout(() => this._initiateSubDistrict(this.state.pageSubDistrict, this.state.limitSubDistrict, this.state.filterSubDistrict), 256);
            //         });
            //     }
            //     break;

            // case "urbanvillage":
            //     clearTimeout(this.inputTimer);

            //     if (value.length >= 3) {
            //         this.setState({ loading: true, dataUrbanVillage: [], filterUrbanVillage: concatPrefix(this.state.prefixUrbanVillage, value), pageUrbanVillage: resetPage }, () => {
            //             this.inputTimer = setTimeout(() => this._initiateUrbanVillage(this.state.pageUrbanVillage, this.state.limitUrbanVillage, this.state.filterUrbanVillage), 256);
            //         });
            //     } else if (!value) {
            //         this.setState({ loading: true, dataUrbanVillage: [], filterUrbanVillage: resetFilter, pageUrbanVillage: resetPage }, () => {
            //             setTimeout(() => this._initiateUrbanVillage(this.state.pageUrbanVillage, this.state.limitUrbanVillage, this.state.filterUrbanVillage), 256);
            //         });
            //     }
            //     break;

            case "postalcode":
                clearTimeout(this.inputTimer);

                if (value.length >= 3) {
                    this.setState({ loading: true, dataPostalCode: [], filterPostalCode: concatPrefix(this.state.prefixPostalCode, value), pagePostalCode: resetPage }, () => {
                        this.inputTimer = setTimeout(() => this._initiatePostalCode(this.state.pagePostalCode, this.state.limitPostalCode, this.state.filterPostalCode), 256);
                    });
                } else if (!value) {
                    this.setState({ loading: true, dataPostalCode: [], filterPostalCode: resetFilter, pagePostalCode: resetPage }, () => {
                        setTimeout(() => this._initiatePostalCode(this.state.pagePostalCode, this.state.limitPostalCode, this.state.filterPostalCode), 256);
                    });
                }
                break;

            // case "island":
            //     clearTimeout(this.inputTimer);

            //     if (value.length >= 3) {
            //         this.setState({ loading: true, dataIsland: [], filterIsland: concatPrefix(this.state.prefixIsland, value), pageIsland: resetPage }, () => {
            //             this.inputTimer = setTimeout(() => this._initiateIsland(this.state.pageIsland, this.state.limitIsland, this.state.filterIsland), 256);
            //         });
            //     } else if (!value) {
            //         this.setState({ loading: true, dataIsland: [], filterIsland: resetFilter, pageIsland: resetPage }, () => {
            //             setTimeout(() => this._initiateIsland(this.state.pageIsland, this.state.limitIsland, this.state.filterIsland), 256);
            //         });
            //     }
            //     break;

            default:
                break;
        }
    };


    render() {
        const rules = rulesUser(this.props.t);

        const onFinish = (values) => {
            console.log('Success:', values);
        };

        const { loading, imageUrl } = this.state;

        const uploadButton = (
            <div>
                {loading ? <LoadingOutlined /> : <PlusOutlined />}
                <div style={{ marginTop: 8 }}>Upload</div>
            </div>
        );

        const UserProfile = () => (
            <div className={style.SubTitle} style={{ display: "block"}}>
                {this.props.t("USER:ADD.USER_PROFLE")}

                <Form style={{marginTop:"30px"}}>
                    <Form.Item className={style.FormItem} name="nik" rules={rules.nik}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_NIK")}</label>
                        <Input className={style.Input}/>
                    </Form.Item>

                    <Form.Item name="title">
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_TITLE")}</label>
                        <Select className={style.Select} defaultValue="Mr"  suffixIcon={<ChevronDown />}   >
                            <Option value="Mr">Mr</Option>
                            <Option value="Mrs">Mrs</Option>
                            <Option value="Ms">Ms</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item name="first_name" rules={rules.first_name}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_FIRST_NAME")}</label>
                        <Input className={style.Input}/>
                    </Form.Item>

                    <Form.Item name="last_name" rules={rules.last_name}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_LAST_NAME")}</label>
                        <Input className={style.Input} />
                    </Form.Item>

                    <Form.Item name="email" rules={rules.email}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_EMAIL")}</label>
                        <Input className={style.Input} />
                    </Form.Item>

                    <Form.Item name="npwp" rules={rules.npwp}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_NPWP")}</label>
                        <Input className={style.Input} placeholder="__.___.___._-___.___"/>
                    </Form.Item>

                    <Form.Item required={true} style={{ display:"flex", width: "60%"}}>
                        <Form.Item name="phone_country_code" style={{ display: "inline-block", width:"30%", marginRight:"2%"}} rules={rules.phone_country_code}>
                            <label className={style.LabelForm}>{this.props.t("USER:FIELD_COUNTRY_CODE")}</label>
                            <Select className={style.SelectPhoneCountryCode} defaultValue="+62"  suffixIcon={<ChevronDown />}>
                                <Option value="+62">+62</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item name="phone" style={{ display: "inline-block", width:"68%"}} rules={rules.phone}>
                            <label className={style.LabelForm}>{this.props.t("USER:FIELD_PHONE")}</label>
                            <Input className={style.InputPhone}/>
                        </Form.Item>
                    </Form.Item>

                    <Form.Item name="address" rules={rules.address}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_ADDRESS_1")}</label>
                        <Input.TextArea className={style.Input} autoSize={{ minRows: 1, maxRows: 6 }}/>
                    </Form.Item>

                    <Form.Item name="country_id" rules={rules.country_id}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_COUNTRY")}</label>
                        <Select className={style.Select}  suffixIcon={<ChevronDown />}
                            // onPopupScroll={(event) => this._onScroll(event, "country")}
                            // dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "country"))}
                            // allowClear={true}
                        >
                            {/* {!this.state.loading ? this.state.dataCountry : [...this.state.dataCountry, LoadingOption()]} */}
                        </Select>
                    </Form.Item>

                    <Form.Item name="island_id" rules={rules.island_id}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_ISLAND")}</label>
                        <Select className={style.Select}  suffixIcon={<ChevronDown />}
                        // onPopupScroll={(event) => this._onScroll(event, "country")}
                        // dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "country"))}
                        // allowClear={true}
                        >
                            {/* {!this.state.loading ? this.state.dataCountry : [...this.state.dataCountry, LoadingOption()]} */}
                        </Select>
                    </Form.Item>

                    <Form.Item name="district_id" rules={rules.district_id}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_DISTRICT")}</label>
                        <Select className={style.Select}  suffixIcon={<ChevronDown />}
                        // onPopupScroll={(event) => this._onScroll(event, "country")}
                        // dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "country"))}
                        // allowClear={true}
                        >
                            {/* {!this.state.loading ? this.state.dataCountry : [...this.state.dataCountry, LoadingOption()]} */}
                        </Select>
                    </Form.Item>

                    <Form.Item name="urban_village_id" rules={rules.urban_village_id}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_URBAN_VILLAGE")}</label>
                        <Select className={style.Select}  suffixIcon={<ChevronDown />}
                        // onPopupScroll={(event) => this._onScroll(event, "country")}
                        // dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "country"))}
                        // allowClear={true}
                        >
                            {/* {!this.state.loading ? this.state.dataCountry : [...this.state.dataCountry, LoadingOption()]} */}
                        </Select>
                    </Form.Item>

                    <Form.Item name="postal_code_id" rules={rules.postal_code_id}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_POSTAL_CODE")}</label>
                        <Select className={style.Select}  suffixIcon={<ChevronDown />}
                        // onPopupScroll={(event) => this._onScroll(event, "postal_code")}
                        // dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "postal_code"))}
                        // allowClear={true}
                        >
                            {/* {!this.state.loading ? this.state.dataPostalCode : [...this.state.dataPostalCode, LoadingOption()]} */}
                        </Select>
                    </Form.Item>

                    
                    {/* <Upload
                    >
                        
                    </Upload> */}

                    <Form.Item
                        name="photo"
                        getValueFromEvent={this._receiveFile}
                        tooltip={<Label text=".png .jpg .jpeg" size="smaller" />}
                        help={<Label text={this.props.t("COMMON:MAX_FIELD", { field: "500kb" })} size="x-small" />}
                    >
                        <label className={style.LabelForm} style={{ color: "black", fontWeight: "bold", marginBottom: "10px" }}>{this.props.t("USER:FIELD_PROFILE_PHOTO")}</label>
                        <Upload
                            action="/upload.do"
                            listType="picture-card"
                            customRequest={this._dummyRequest}
                            accept={"image/png, image/jpeg"}
                            className="avatar-uploader"
                            showUploadList={false}
                            onChange={this.handleChange}
                        >
                            {/* <Button icon={<UploadOutlined />}>
                                {this.props.t("COMMON:BUTTON_UPLOAD")}
                            </Button> */}

                            {imageUrl ? <img src={imageUrl} alt="photo" style={{ width: '100%' }} /> : uploadButton}

                        </Upload>
                    </Form.Item>

                    <Form.Item className={style.ButtonArea}>
                        <ButtonCancel />
                        <ButtonNext />
                    </Form.Item>

                </Form>

            </div>
        );

        const Preferences = () => (
            <div className={style.SubTitle} style={{ display: "block" }}>
                {this.props.t("USER:ADD.PREFERENCES")}

                <Form style={{ marginTop: "30px" }}>
                    <Form.Item name="locale">
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_LOCALE")}</label>
                        <Select className={style.Select} defaultValue="en-US" >
                            <Option value="en-US">EN</Option>
                            <Option value="id-ID">ID</Option>
                        </Select>
                    </Form.Item>

                    <Form.Item className={style.FormItem} name="approval_group_id" rules={rules.approval_group_id}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_APPROVAL_GROUP")}</label>
                        <Input className={style.Input} />
                    </Form.Item>
                    
                    <Form.Item name="signature_key" rules={rules.signature_key}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_SIGNATURE_KEY")}</label>
                        <Input className={style.Input} />
                    </Form.Item>

                    <Form.Item name="ip_whitelist" rules={rules.ip_whitelist}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_IP_WHITE_LIST")}</label>
                        <Input className={style.Input} />
                    </Form.Item>

                    <Form.Item name="device" rules={rules.device}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_DEVICE")}</label>
                        <Input className={style.Input} />
                    </Form.Item>

                    <Form.Item name="user_auth_id" rules={rules.user_auth_id}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_USER_AUTH_ID")}</label>
                        <Input className={style.Input} />
                    </Form.Item>

                    <Form.Item name="role_id" rules={rules.role_id}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_ID_ROLE")}</label>
                        <Select mode="tags" className={style.SelectTagsMode}  tokenSeparators={[',']}
                        // onPopupScroll={(event) => this._onScroll(event, "country")}
                        // dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "country"))}
                        // allowClear={true}
                        >
                            {/* {!this.state.loading ? this.state.dataCountry : [...this.state.dataCountry, LoadingOption()]} */}
                            {children}
                        </Select>
                    </Form.Item>

                    <Form.Item name="datagroup_id" rules={rules.datagroup_id}>
                        <label className={style.LabelForm}>{this.props.t("USER:FIELD_ID_DATAGROUP")}</label>
                        <Select mode="tags" className={style.SelectTagsMode}  tokenSeparators={[',']}
                        // onPopupScroll={(event) => this._onScroll(event, "country")}
                        // dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "country"))}
                        // allowClear={true}
                        >
                            {/* {!this.state.loading ? this.state.dataCountry : [...this.state.dataCountry, LoadingOption()]} */}
                        </Select>
                    </Form.Item>

                    <Form.Item className={style.ButtonArea}>
                        <ButtonCancel />
                        <ButtonNext />
                    </Form.Item>

                </Form>

            </div>
        );

        // _onFinish = async (value) => {
        //     // const data = {
        //     //     urban_village_id: parseInt(value.urbanvillage_id),
        //     //     code: value.code,
        //     // };

        //     // const { token, refresh_token, dispatch, history } = this.props;
        //     // if (this.props.permission.insert || this.props.permission.insert_own) {
        //     //     await PostalCodeService.doInsert(token, refresh_token, data, dispatch, () => history.push(ROUTER_CONSTANT.POSTAL_CODE.LIST));
        //     // }
        //     console.log("tes");
        // };

        return (
            <div>
                <div className={style.MenuTitle}>User &#62; Add User</div>
                <div className={style.Title}>
                    <div>{this.props.t("USER:ADD.TITLE")}</div>
                </div>

                {/* container luar */}
                <div style={{ marginBottom: "20px", paddingLeft: "55px", paddingRight: "55px", paddingBottom:"55px", position: "absolute", width:"100%"}}>
                    <div className={style.List}>
                        {/* container luar */}
                        <div style={{  alignItems: "center" }}>
                            <div className={style.HeadTable}>
                                <div className={style.GridContainerHead}>
                                    <div className={this.state.comp === 1 || this.state.comp === 2 || this.state.comp === 3 ? style.GridItemHeadActive : style.GridItemHead} style={{ borderRight: "1px solid #CCCCCC" }} onClick={() => this._onChangeComp(1)}>
                                        <div className={this.state.comp === 1 || this.state.comp === 2 || this.state.comp === 3 ? style.NumberHeadActive : style.NumberHead}>1</div>
                                        <div className={style.Step}>User Profile</div>
                                    </div>


                                    <div className={this.state.comp === 2 || this.state.comp === 3 ? style.GridItemHeadActive : style.GridItemHead} style={{ borderRight: "1px solid #CCCCCC" }} onClick={() => this._onChangeComp(2)}>
                                        <div className={this.state.comp === 2 || this.state.comp === 3 ? style.NumberHeadActive : style.NumberHead}>2</div>
                                        <div className={style.Step}>Login</div>
                                    </div>


                                    <div className={this.state.comp === 3 ? style.GridItemHeadActive : style.GridItemHead} onClick={() => this._onChangeComp(3)}>
                                        <div className={this.state.comp === 3 ? style.NumberHeadActive : style.NumberHead}>3</div>
                                        <div className={style.Step}>Preferences</div>
                                    </div>
                                </div>
                            </div>

                            {this.state.comp === 1 && <UserProfile />}

                            {this.state.comp === 3 && <Preferences />}

                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return { dispatch };
};

export default connect(null, mapDispatchToProps)(withTranslation()(AddClientUser));