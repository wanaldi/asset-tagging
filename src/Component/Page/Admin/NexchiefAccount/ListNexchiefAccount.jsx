import React, { Component } from 'react';
import style from "./ListNexchiefAccount.module.css";
import { propTable, RANDOM_ID } from "../../../../Enum/PropertiesConstant";
import { ROUTER_CONSTANT } from "../../../../Enum/RouterConstant";
import { withTranslation } from "react-i18next";
import { connect } from 'react-redux';
import * as NexchiefAccountService from "../../../../Service/Admin/NexchiefAccountService";
import { message, Pagination } from 'antd';
import { resourcePagination } from "../../../../Util/Pagination";
import { stateRedux } from '../../../../Util/ReduxState';
import { Link } from 'react-router-dom';
import moment from 'moment';
import 'moment/locale/id';
import Filter from '../../../Common/Filter/Filter';

class ListNexchiefAccount extends Component {
    state = {
        ...propTable,
        initiate_list: null,
        list: [],
    };

    async componentDidMount() {
        await this._getData(this.state.page, this.state.limit, this.state.order, this.state.filter);
        // if(this.props.language==="id-ID") moment.locale('id');
    }

    _getData = async (page, limit, order, filter) => {
        const { token, refresh_token, dispatch } = this.props;

        if (this.props.permission.view || this.props.permission.view_own) {
            const response = await NexchiefAccountService.doGetList(token, refresh_token, page, limit, order, filter, dispatch);
            if (response) this.setState({ initiate_list: response.initiate_list, list: response.list });

        }
    };

    _resetTable = () => {
        const { page, limit, search_by, operator, search_key, filter, order, order_by, order_params } = propTable;
        this.setState({ page, limit, search_by, operator, search_key, filter, order, order_by, order_params }, () =>
            this._getData(this.state.page, this.state.limit, this.state.order, this.state.filter)
        );
    };

    _onChange = (key, value) => {
        this.setState({ [key]: value });
    };

    _onPaging = (page, limit) => {
        this.setState({ page, limit }, () => this._getData(this.state.page, this.state.limit, this.state.order, this.state.filter));
    };

    _onOrder = (order_by, order_params) => {
        this.setState({ order_by, order_params }, () => {
            if (order_by && order_params) {
                this.setState({ order: `${order_by} ${order_params}` }, () => this._getData(this.state.page, this.state.limit, this.state.order, this.state.filter));
            } else {
                this._resetTable();
                message.error({ content: this.props.t("COMMON:REQUIRE_SEARCH") });
            }
        });
    };

    _onFilter = (value) => {
        console.log("value", value.target.innerText);
        console.log("onfilter");
        // const { search_by, operator, search_key } = this.state;

        if (value) {
            // const filter = 'name like ' + value.target.innerText + ', code like ' + value.target.innerText;
            const filter = 'name like ' + value.target.innerText ;
            this.setState({ filter }, () => this._getData(propTable.page, propTable.limit, propTable.order, filter));
        } else {
            this._resetTable();
            message.error({ content: this.props.t("COMMON:REQUIRE_SEARCH") });
        }
    };

    _onChangePageSizeOptions = (event) => {
        this.setState({ limit: event.target.value }, () => {
            this._onPaging(this.state.page, this.state.limit);
        });
    }

    render() {

        const pagination = resourcePagination(this.state, this._onPaging);

        return (
            <div style={{ marginBottom: "20px", paddingLeft: "55px", paddingRight: "55px", paddingBottom: "55px", position: "absolute", width: "100%" }}>
                <div className={style.Title}>
                    <div style={{ marginBottom: "10px" }}>{this.props.t("NEXCHIEF_ACCOUNT:TITLE")}</div>
                    <div className={style.Filter} style={{ display: "contents" }}>
                        <Filter
                            {...this.props}
                            state={this.state}
                            permission={this.props.permission}
                            initiateList={this.state.initiate_list}
                            function={{
                                onOrder: this._onOrder,
                                onChange: this._onChange,
                                onReset: this._resetTable,
                            }}
                            namespace="nexchiefaccount"
                            placeholder="Search code or Nexchief Account"
                            changeFilter={() => this._onFilter}
                        />
                    </div>
                </div>

                <div style={{overflowX:"auto"}}>

                <div className={style.HeadTable}>
                    <div className={style.GridContainer}>
                        <div className={style.GridItemHead}>ID</div>
                        <div className={style.GridItemHead}>Nexchief Account</div>
                        <div className={style.GridItemHead}>Join Date</div>
                        <div className={style.GridItemHead}>License</div>
                    </div>
                </div>

                <ul style={{ listStyleType: "none", width: "100%", paddingInlineStart: "0px" }}>
                    {this.state.list !== null && this.state.list.map((item, index) => (
                        <li key={index}>
                            <div style={{ marginBottom: "20px" }}>
                                <div className={style.HeadTable}>
                                    <div className={style.GridContainer}>
                                        <div className={style.GridItem}>{item.code}</div>
                                        <div className={style.GridItem}>{item.company_profile_name}</div>
                                        <div className={style.GridItem}>{moment(item.created_at).format(this.props.language === "id-ID" ? 'LL' : 'MMMM Do YYYY')}</div>
                                        <div className={style.GridItemStatus}>{item.deleted ? "NonActive" : "Active"}</div>
                                        <div className={style.GridItem} style={{ justifyContent: "right" }}>
                                            <Link to={ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.DETAIL + "/" + item.id}>
                                                <div className={style.LinkViewDetail}>
                                                    View Detail
                                                </div>
                                            </Link>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    ))}
                    {this.state.list == null && "Tidak Ada Data"}
                    </ul>
                </div>

                <div style={{ display: "flex" }}>
                    <div style={{ marginRight: "5px", textAlign: "right", width: "100%" }}>Rows Per Page</div>

                    <select id="changePageOptions" onChange={this._onChangePageSizeOptions} style={{ marginLeft: "auto" }} defaultValue="10">
                        {this.state.initiate_list && this.state.initiate_list.valid_limit.map((item, index) => (
                            <option key={index} value={this.state.initiate_list && this.state.initiate_list.valid_limit[index]}>{this.state.initiate_list && this.state.initiate_list.valid_limit[index]}</option>
                        ))}

                    </select>

                    <div style={{ marginRight: "0px", display: "contents" }}>
                        <Pagination
                            size="small"
                            current={this.state.page}
                            defaultCurrent={this.state.page}
                            pageSize={this.state.limit}
                            total={this.state.initiate_list && this.state.initiate_list.count_data}
                            onChange={this._onPaging}
                            style={{ display: "contents" }}
                        />
                    </div>

                </div>
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        ...stateRedux(state, "nexchief_account"),
        language: state.authentication.language
    };

};

const mapDispatchToProps = (dispatch) => {
    return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(ListNexchiefAccount));