import React, { Component } from 'react';
import style from "./DetailNexchiefAccount.module.css";
import { ROUTER_CONSTANT } from "../../../../Enum/RouterConstant";
import { withTranslation } from "react-i18next";
import { connect } from 'react-redux';
import * as NexchiefAccountService from "../../../../Service/Admin/NexchiefAccountService";
import { propTable, RANDOM_ID } from "../../../../Enum/PropertiesConstant";
import { stateRedux } from '../../../../Util/ReduxState';
import ButtonDelete from "../../../Common/Button/ButtonDelete";
import ButtonEdit from "../../../Common/Button/ButtonEdit";
import CustomImage from "../../../Common/Image/CustomImage";
import moment from 'moment';
import ModalDelete from '../../../Common/Modal/ModalDelete';
import ModalSuccessDelete from '../../../Common/Modal/ModalSuccessDelete';

class DetailNexchiefAccount extends Component {

    state = {
        comp: 1,
        detail: null,
        modalOpen: false,
        ...propTable,
        responseSuccess: null,
        modalSuccessDeleteOpen: false,
    };

    _onChangeComp = i => {
        this.setState({ comp: i });
    };

    async componentDidMount() {
        await this._getDetail(this.props.match.params.id);
    }

    _getDetail = async (id) => {
        const { token, refresh_token, dispatch } = this.props;

        const response = await NexchiefAccountService.doGetDetail(token, refresh_token, id, dispatch);
        if (response) this.setState({ detail: response.detail });

        console.log("detail", this.state.detail);
    };

    _onDelete = async (id, updated_at) => {
        console.log("masuk delete");
        const { token, refresh_token, dispatch } = this.props;

        if (this.props.permission.delete || this.props.permission.delete_own) {
            const response = await NexchiefAccountService.doDelete(token, refresh_token, id, { updated_at }, dispatch);
            this.setState({ responseSuccess: response });
            this.setState({ modalOpen: false });
            this.setState({ modalSuccessDeleteOpen: true });
        } else {
            console.log("else");
        }
    };

    _openModal = () => {
        this.setState({ modalOpen: true });
    }

    _closeModal = () => {
        console.log("oke");
        this.setState({ modalOpen: false });
    }

    _closeModalSuccessDelete = () => {
        this.setState({ modalSuccessDeleteOpen: false });
    }

    render() {

        const DataManagementHistory = () => (
            <div className={style.GridItemLuar}>
                <div className={style.DataManagementContainer}>
                    <div className={style.DataManagement}>
                        {this.props.t("NEXCHIEF_ACCOUNT:FIELD_DATA_MANAGEMENT_HISTORY")}
                    </div>
                    <div className={style.CreatedModified}>
                        {this.props.t("NEXCHIEF_ACCOUNT:FIELD_CREATED_ON")} {this.state.detail !== null && this.state.detail.created_at !== '' ? moment(this.state.detail.created_at).format('DD-MMM-YYYY') : "-"}
                    </div>
                    <div className={style.CreatedModified}>
                        {this.props.t("NEXCHIEF_ACCOUNT:FIELD_CREATED_BY")} {this.state.detail !== null && this.state.detail.created_by !== '' ? this.state.detail.created_by : "-"}
                    </div>
                    <br />
                    <div className={style.CreatedModified}>
                        {this.props.t("NEXCHIEF_ACCOUNT:FIELD_MODIFIED_ON")} {this.state.detail !== null && this.state.detail.updated_at !== '' ? moment(this.state.detail.updated_at).format('DD-MMM-YYYY') : "-"}
                    </div>
                    <div className={style.CreatedModified}>
                        {this.props.t("NEXCHIEF_ACCOUNT:FIELD_MODIFIED_BY")} {this.state.detail !== null && this.state.detail.updated_by !== '' ? this.state.detail.updated_by : "-"}
                    </div>
                </div>
            </div>
        );

        const CompanyProfile = () => (
            <div className={style.GridContainerLuar}>
                <div className={style.GridItemLuar}>
                    <div className={style.SubTitle}>
                        <div>
                            {this.props.t("NEXCHIEF_ACCOUNT:FIELD_COMPANY_PROFILE")}
                        </div>
                    </div>
                    <div className={style.GridContainerDalam}>
                        <div className={style.GridItemDalam}>
                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_ID")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile!==null && this.state.detail.code !== '' ? this.state.detail.code : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_COMPANY_TYPE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.company_title !== '' ? this.state.detail.company_profile.company_title : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_COMPANY_NAME")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.name !== '' ? this.state.detail.company_profile.name : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_CORPORATE_PHONE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.phone !== '' ? this.state.detail.company_profile.phone : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_CORPORATE_EMAIL")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.email !== '' ? this.state.detail.company_profile.email : "-"}</div>
                            
                        </div>
                        <div className={style.GridItemDalam}>
                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_NPWP")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.npwp !== '' ? this.state.detail.company_profile.npwp : "-"}</div>

                            <div className={style.LabelForm} style={{marginBottom: "5px"}}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_COMPANY_LOGO")}
                            </div>
                            <CustomImage logo={this.state.detail && this.state.detail.company_profile.logo && this.state.detail.company_profile.logo[0].host + this.state.detail.company_profile.logo[0].path} />
                       
                        </div>
                        <div className={style.SubTitle}>
                            <div>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_ADDRESS")}
                            </div>
                        </div>
                        
                    </div>

                    <div className={style.GridContainerDalam}>
                        <div className={style.GridItemDalam}>
                            
                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_ADDRESS")}
                            </div>
                            <div className={style.DetailItem}>
                                {this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.address_1 !== '' ? this.state.detail.company_profile.address_1 : "-"}
                                {this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.address_2 !== '' && <div>this.state.detail.company_profile.address_2</div>}
                                {this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.address_3 !== '' && <div>this.state.detail.company_profile.address_3</div>}
                            </div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_COUNTRY")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.country_name !== '' ? this.state.detail.company_profile.country_name : "-"}</div>
                            
                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_DISTRICT")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.district !== '' ? this.state.detail.company_profile.district : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_SUB_DISTRICT")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.sub_district !== '' ? this.state.detail.company_profile.sub_district : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_URBAN_VILLAGE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.urban_village !== '' ? this.state.detail.company_profile.urban_village : "-"}</div>

                        </div>
                        <div className={style.GridItemDalam}>
                        
                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_HAMLET")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.hamlet !== '' ? this.state.detail.company_profile.hamlet : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_NEIGHBOURHOOD")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.neighbourhood !== '' ? this.state.detail.company_profile.neighbourhood : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_POSTAL_CODE")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.postal_code !== '' ? this.state.detail.company_profile.postal_code : "-"}</div>

                            <div className={style.LabelForm}>
                                {this.props.t("NEXCHIEF_ACCOUNT:FIELD_ISLAND")}
                            </div>
                            <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.company_profile !== null && this.state.detail.company_profile.island !== '' ? this.state.detail.company_profile.island : "-"}</div>

                        </div>

                    </div>
                </div>
                <DataManagementHistory />
            </div>
        );

        const ContactPerson = () => (
            <div className={style.GridContainerLuar}>
                <div className={style.GridItemLuar}>
                    <div className={style.SubTitle}>
                        <div>
                            {this.props.t("NEXCHIEF_ACCOUNT:FIELD_CONTACT_PERSON")}
                        </div>
                    </div>
                    {this.state.detail.contact_person == null ?
                        "Tidak Ada Data"
                        :
                        <div className={style.GridContainerDalam}>
                            <div className={style.GridItemDalam}>
                                <div className={style.LabelForm}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_NIK")}
                                </div>
                                <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.nik !== "" ? this.state.detail.contact_person.nik : "-"}</div>

                                <div className={style.LabelForm}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_FIRST_NAME")}
                                </div>
                                <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.first_name !== "" ? this.state.detail.contact_person.first_name : "-"}</div>

                                <div className={style.LabelForm}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_LAST_NAME")}
                                </div>
                                <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.last_name !== "" ? this.state.detail.contact_person.last_name : "-"}</div>

                                <div className={style.LabelForm}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_PHONE")}
                                </div>
                                <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.phone !== "" ? this.state.detail.contact_person.phone : "-"}</div>

                                <div className={style.LabelForm}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_EMAIL")}
                                </div>
                                <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.email !== "" ? this.state.detail.contact_person.email : "-"}</div>

                            </div>
                            <div className={style.GridItemDalam}>
                                <div className={style.LabelForm}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_POSITION")}
                                </div>
                                <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.position !== "" ? this.state.detail.contact_person.position : "-"}</div>

                                <div className={style.LabelForm}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_JOIN_DATE")}
                                </div>
                                <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.join_date !== "" ? moment(this.state.detail.contact_person.join_date).format("DD-MM-YYYY") : "-"}</div>

                                <div className={style.LabelForm}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_RESIGN_DATE")}
                                </div>
                                <div className={style.DetailItem}>{this.state.detail !== null && this.state.detail.contact_person !== null && this.state.detail.contact_person.resign_date !== "" ? moment(this.state.detail.contact_person.resign_date).format("DD-MM-YYYY") : "-"}</div>


                            </div>
                        </div>
                    }

                </div>
                <DataManagementHistory />
            </div>
        );

        const Preferences = () => (
            <div className={style.GridContainerLuar}>
                <div className={style.GridItemLuar}>
                    <div className={style.SubTitle}>
                        <div>
                            {this.props.t("NEXCHIEF_ACCOUNT:FIELD_PREFERENCES")}
                        </div>
                    </div>
                    {this.state.detail.list_parameter == null ?
                        "Tidak Ada Data"
                        :
                        <div className={style.GridContainerDalam}>
                            
                            {this.state.detail.list_parameter.map((item, index) => (
                                <div key={index} className={style.GridItemDalam}>
                                    <div className={style.LabelForm}>
                                        {item.name}
                                    </div>
                                    <div className={style.DetailItem}>{(item.char_value==='Y' && "Yes") || (item.char_value==='N' && "No") || item.char_value}</div>
                                </div>
                            ))}
                            
                        </div>
                    }
                    

                </div>
                <DataManagementHistory />
            </div>
        );

        const ModalDelete2 = () => (
            <div>
            {
                this.state.detail &&
                    <ModalDelete
                        key={RANDOM_ID(8)}
                        modalOpen={this.state.modalOpen}
                        menu={this.props.t("NEXCHIEF_ACCOUNT:TITLE_DELETE")}
                        onDelete={() => this._onDelete(this.state.detail.id, this.state.detail.updated_at)}
                        closeModal={() => this._closeModal()}
                        okDelete={this.props.t("NEXCHIEF_ACCOUNT:TITLE_HEADER")}
                    />
                }
            </div>
        )

        const ModalSuccessDelete2 = () => (
            <div>
                {
                    this.state.responseSuccess &&
                    <ModalSuccessDelete
                        key={RANDOM_ID(8)}
                        modalSuccessDeleteOpen={this.state.modalSuccessDeleteOpen}
                        response={this.state.responseSuccess}
                        closeModal={() => this._closeModalSuccessDelete()}
                        router={ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.LIST}
                    />
                }
            </div>
        )

        const ButtonDelete2 = () => (
            <div style={{display:"contents"}}>
                {
                this.state.detail &&
                    <ButtonDelete
                        key={RANDOM_ID(8)}
                        detail={this.state.detail}
                        openModal = {this._openModal}
                        disabled={this.props.permission.delete ? false : this.props.permission.delete_own ? this.props.detail.created_by !== this.props.user : true}
                    />
                }

                <ModalDelete2 />
                <ModalSuccessDelete2 />
            </div>
        );

        const ButtonEdit2 = () => (
            <div style={{ display: "contents" }}>
                {
                    this.state.detail &&
                    <ButtonEdit
                        key={RANDOM_ID(8)}
                        router={ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.EDIT + "/" + this.props.match.params.id}
                        text=""
                        detail={this.state.detail}
                        disabled={this.props.permission.update ? false : this.props.permission.update_own ? this.state.detail.created_by !== this.props.user : true}
                    />
                }
            </div>
        );

        // console.log("2", this.state.detail.created_by !== this.props.user);

        return (
            <div>
                <div className={style.MenuTitle}>{this.props.t("NEXCHIEF_ACCOUNT:TITLE")} &#62; Detail</div>
                <div className={style.Title}>
                    <div>{this.props.t("NEXCHIEF_ACCOUNT:DETAIL.TITLE")}</div>
                    <ButtonDelete2 />
                    <ButtonEdit2 />
                </div>

                {/* container luar */}
                <div style={{ marginBottom: "20px", paddingLeft: "55px", paddingRight: "55px", paddingBottom: "55px", position: "absolute", width: "100%" }}>
                    <div className={style.List}>
                        {/* container luar */}
                        <div style={{ alignItems: "center" }}>
                            <div>
                                <button className={this.state.comp === 1 ? style.ButtonComponentActive : style.ButtonComponent } onClick={() => this._onChangeComp(1)}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_COMPANY_PROFILE")}
                                </button>
                                <button className={this.state.comp === 2 ? style.ButtonComponentActive : style.ButtonComponent} onClick={() => this._onChangeComp(2)}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_CONTACT_PERSON")}
                                </button>
                                <button className={this.state.comp === 3 ? style.ButtonComponentActive : style.ButtonComponent} onClick={() => this._onChangeComp(3)}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_PREFERENCES")}
                                </button>
                            </div>

                            {this.state.comp === 1 && <CompanyProfile />}
                            {this.state.comp === 2 && <ContactPerson />}
                            {this.state.comp === 3 && <Preferences />}

                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return { ...stateRedux(state, "nexchief_account") };
};

const mapDispatchToProps = (dispatch) => {
    return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(DetailNexchiefAccount));