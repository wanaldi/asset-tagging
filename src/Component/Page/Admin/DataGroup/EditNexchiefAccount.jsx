import { DatePicker, Form, Input, Select, Switch } from 'antd';
import moment from 'moment';
import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import ButtonCancel from '../../../Common/Button/ButtonCancel';
import ButtonNext from '../../../Common/Button/ButtonNext';
import ButtonBack from '../../../Common/Button/ButtonBack';
import ButtonSubmit from '../../../Common/Button/ButtonSubmit';
import CustomImage from '../../../Common/Image/CustomImage';
import style from "./EditNexchiefAccount.module.css";
import { rulesNexchiefAccount } from '../../../../Util/FormRules';
import ChevronDown from '../../../Icon/ChevronDown';
import { ROUTER_CONSTANT } from '../../../../Enum/RouterConstant';
import DatePickerLogo from '../../../Icon/DatePickerLogo';
import { prepareComponent } from "../../../../Action/CommonAction";
import ButtonCheck from '../../../Common/Button/ButtonCheck';
import { propTable } from '../../../../Enum/PropertiesConstant';
import * as PersonProfileService from "../../../../Service/Admin/PersonProfileService";
import { connect } from 'react-redux';
import { stateRedux } from '../../../../Util/ReduxState';
import axios from 'axios';

class EditNexchiefAccount extends Component {
    state = {
        ...propTable,

        comp: 1,
        detail: null,

    };

    formRef = React.createRef();

    _onChangeComp = i => {
        this.setState({ comp: i });
    };

    _nextComp = i => {
        this.setState({ comp: i + 1 });
    };

    _prevComp = i => {
        this.setState({ comp: i - 1 });
    };

    _onCheck = async () => {
        var nikValue = document.getElementById("nik").value;
        console.log("nikValue", nikValue);

        const data2 = {
            page: this.state.page,
            limit: this.state.limit,
            order_by: this.state.order_by,
            nik: nikValue,
        }

        this._getData(data2);
    };

    _getData = async (value) => {
        const { token, refresh_token, dispatch } = this.props;

        const today = moment().format("DD-MM-YYYY");

        if (this.props.permission.view || this.props.permission.view_own) {
            const response = await PersonProfileService.doGetList(token, refresh_token, value, dispatch);
            console.log("res 123", response);

            if (response) {
                this.formRef.current.setFieldsValue({
                    first_name: response[0].first_name,
                    last_name: response[0].last_name,
                    position_id: response[0].position,
                    phone: response[0].phone,
                    email: response[0].email,
                    join_date: moment(moment(response[0].join_date).format("DD-MM-YYYY"), "DD-MM-YYYY"),
                    resign_date: moment(moment(response[0].resign_date).format("DD-MM-YYYY"), "DD-MM-YYYY"),
                });
                
            } else {
                this.formRef.current.setFieldsValue({
                    first_name: '',
                    last_name: '',
                    position_id: '',
                    phone: '',
                    email: '',
                    join_date: '',
                    resign_date: '',
                });
            }

        }
    };

    _onFinish = (values) => {
        // const nik = values.nik;
        // const first_name = values.first_name;
        // const last_name = values.last_name;
        // const position_id = values.position_id;
        // const phone = values.phone;
        // const email = values.email;
        // const join_date = values.join_date;
        // const resign_date = values.resign_date;
        // console.log("nik", nik);
        console.log("values", values);
        if (this.state.comp !== 3) {
            this._nextComp(this.state.comp);
        }
    }

    render() {

        const rules = rulesNexchiefAccount(this.props.t);

        const DataManagementHistory = () => (
            <div className={style.GridItemLuar}>
                <div className={style.DataManagementContainer}>
                    <div className={style.DataManagement}>
                        {this.props.t("NEXCHIEF_ACCOUNT:FIELD_DATA_MANAGEMENT_HISTORY")}
                    </div>
                    <div className={style.CreatedModified}>
                        {this.props.t("NEXCHIEF_ACCOUNT:FIELD_CREATED_ON")} {this.props.location.state.detail !== null && this.props.location.state.detail.created_at !== '' ? moment(this.props.location.state.detail.created_at).format('DD-MMM-YYYY') : "-"}
                    </div>
                    <div className={style.CreatedModified}>
                        {this.props.t("NEXCHIEF_ACCOUNT:FIELD_CREATED_BY")} {this.props.location.state.detail !== null && this.props.location.state.detail.created_by !== '' ? this.props.location.state.detail.created_by : "-"}
                    </div>
                    <br />
                    <div className={style.CreatedModified}>
                        {this.props.t("NEXCHIEF_ACCOUNT:FIELD_MODIFIED_ON")} {this.props.location.state.detail !== null && this.props.location.state.detail.updated_at !== '' ? moment(this.props.location.state.detail.updated_at).format('DD-MMM-YYYY') : "-"}
                    </div>
                    <div className={style.CreatedModified}>
                        {this.props.t("NEXCHIEF_ACCOUNT:FIELD_MODIFIED_BY")} {this.props.location.state.detail !== null && this.props.location.state.detail.updated_by !== '' ? this.props.location.state.detail.updated_by : "-"}
                    </div>
                </div>
            </div>
        );

        const CompanyProfile = () => (
            <div>
                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_ID")}</label>
                <Form.Item className={style.FormItem} name="id">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_COMPANY_TYPE")}</label>
                <Form.Item className={style.FormItem} name="company_type">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_COMPANY_NAME")}</label>
                <Form.Item className={style.FormItem} name="company_name">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                <Form.Item required={true}>
                    <div style={{ display: "grid", width: "60%", gridTemplateColumns: "30% 70%" }}>
                        <div style={{ display: "inline-block" }}>
                            <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_COUNTRY_CODE")}</label>
                            <Form.Item name="phone_country_code">
                                <Input className={style.InputDisabled} style={{ width: "90%" }} disabled />
                            </Form.Item>
                        </div>

                        <div>
                            <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_CORPORATE_PHONE")}</label>
                            <Form.Item name="phone">
                                <Input className={style.InputPhoneDisabled} disabled />
                            </Form.Item>
                        </div>
                    </div>
                </Form.Item>

                <Form.Item required={true}>
                    <div style={{ display: "grid", width: "60%", gridTemplateColumns: "30% 70%" }}>
                        <div style={{ display: "inline-block" }}>
                            <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_COUNTRY_CODE")}</label>
                            <Form.Item name="fax_country_code">
                                <Input className={style.InputDisabled} style={{ width: "90%" }} disabled />
                            </Form.Item>
                        </div>

                        <div style={{ display: "inline-block" }}>
                            <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_FAX_NUMBER")}</label>
                            <Form.Item name="fax" >
                                <Input className={style.InputPhoneDisabled} disabled />
                            </Form.Item>
                        </div>
                    </div>

                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_EMAIL")}</label>
                <Form.Item className={style.FormItem} name="email">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_NPWP")}</label>
                <Form.Item className={style.FormItem} name="npwp">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_COMPANY_LOGO")}</label>
                <Form.Item className={style.FormItem} name="company_logo">
                    <CustomImage logo={this.props.location.state.detail && this.props.location.state.detail.company_profile.logo} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_ADDRESS")}</label>
                <Form.Item className={style.FormItem} name="address">
                    <Input.TextArea className={style.InputDisabled} autoSize={{ minRows: 1, maxRows: 6 }} disabled />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_COUNTRY")}</label>
                <Form.Item className={style.FormItem} name="country">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_DISTRICT")}</label>
                <Form.Item className={style.FormItem} name="district">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_SUB_DISTRICT")}</label>
                <Form.Item className={style.FormItem} name="sub_district">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_URBAN_VILLAGE")}</label>
                <Form.Item className={style.FormItem} name="urban_village">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_HAMLET")}</label>
                <Form.Item className={style.FormItem} name="hamlet">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_NEIGHBOURHOOD")}</label>
                <Form.Item className={style.FormItem} name="neighbourhood">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_POSTAL_CODE")}</label>
                <Form.Item className={style.FormItem} name="postal_code">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_ISLAND")}</label>
                <Form.Item className={style.FormItem} name="island">
                    <Input className={style.InputDisabled} disabled />
                </Form.Item>

                {/* <Form.Item className={style.ButtonArea}>
                    <ButtonCancel router={ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.LIST} />
                    <div onClick={() => this._nextComp(this.state.comp)} style={{ display: "contents" }}>
                        <ButtonNext />
                    </div>

                </Form.Item> */}
            </div>
        );

        const ContactPerson = () => (
            <div>
                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_NIK")}</label>
                <Form.Item className={style.FormItem} name="nik" rules={rules.nik}>
                    <Input className={style.Input} id="nik" suffix={<ButtonCheck onCheck={this._onCheck} />} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_FIRST_NAME")}</label>
                <Form.Item name="first_name" rules={rules.first_name}>
                    <Input className={style.Input} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_LAST_NAME")}</label>
                <Form.Item name="last_name" rules={rules.last_name}>
                    <Input className={style.Input} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_POSITION")}</label>
                <Form.Item name="position_id" rules={rules.position_id}>
                    <Select className={style.Select}  suffixIcon={<ChevronDown />}
                    // onPopupScroll={(event) => this._onScroll(event, "country")}
                    // dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "country"))}
                    // allowClear={true}
                    >
                        {/* {!this.state.loading ? this.state.dataCountry : [...this.state.dataCountry, LoadingOption()]} */}
                    </Select>
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_PHONE")}</label>
                <Form.Item name="contact_phone" rules={rules.phone}>
                    <Input className={style.Input} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_EMAIL")}</label>
                <Form.Item name="contact_email" rules={rules.email}>
                    <Input className={style.Input} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_JOIN_DATE")}</label>
                <Form.Item name="join_date" rules={rules.join_date}>
                    <DatePicker
                        format={"DD-MM-YYYY"}
                        suffixIcon={<DatePickerLogo />}
                        className={style.Input}
                        allowClear={false}
                    />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("NEXCHIEF_ACCOUNT:FIELD_RESIGN_DATE")}</label>
                <Form.Item name="resign_date" rules={rules.resign_date}>
                    <DatePicker
                        format={"DD-MM-YYYY"}
                        suffixIcon={<DatePickerLogo />}
                        className={style.Input}
                        allowClear={false}
                    />
                </Form.Item>

                {/* <Form.Item className={style.ButtonArea}>
                    <div onClick={() => this._prevComp(this.state.comp)} style={{ display: "contents" }}>
                        <ButtonBack />
                    </div>
                    <div onClick={() => this._nextComp(this.state.comp)} style={{ display: "contents" }}>
                        <ButtonNext />
                    </div>
                </Form.Item> */}

            </div>
        );

        const Preferences = () => (
            <div>
                {/* <div className={style.InputGroup}>
                                <input className="floating_input" type="text" required />
                                <label className="floating_label">Username</label>
                        </div> */}

                {this.props.location.state.detail.list_parameter.map((item, index) => (
                    <div key={index}>

                        <label className={style.LabelForm}>{item.name}</label>

                        {index === 0 &&
                            <Form.Item name={item.name} >
                                <Select className={style.Select}  suffixIcon={<ChevronDown />} placeholder={item.name} defaultValue={item.char_value}
                                >
                                    {/* TODO: ubah jadi sesuai select list name dari api */}
                                    <option value="B">Inbound and Outbound</option>
                                    <option value="I">Inbound Only (From ND6)</option>
                                </Select>
                            </Form.Item>
                        }

                        {index !== 0 && index !== 9 &&
                            <Form.Item className={style.FormItem} name={item.name}>
                                <Switch
                                    checkedChildren="Yes"
                                    unCheckedChildren="No"
                                    defaultChecked={item.char_value === 'N' ? false : true}
                                    style={{ height: "32px", borderRadius: "4px", width: "60px", alignItems: "center", marginTop: "5px" }}
                                />
                            </Form.Item>
                        }

                        {index === 9 &&
                            <Form.Item className={style.FormItem} name={item.name}>
                                <Input className={style.Input} defaultValue={item.char_value} />
                            </Form.Item>
                        }

                        {index === 10 &&
                            <Form.Item name={item.char_value}>
                                <DatePicker
                                    // defaultValue={moment(moment(this.state.join_date).format("DD-MM-YYYY"), "DD-MM-YYYY")}
                                    format={"DD-MM-YYYY"}
                                    suffixIcon={<DatePickerLogo />}
                                    className={style.Input}
                                    allowClear={false}
                                />
                            </Form.Item>
                        }

                    </div>
                ))}
            </div>
        );

        return (
            <div>
                <div className={style.MenuTitle}>{this.props.t("NEXCHIEF_ACCOUNT:TITLE")} &#62; {this.props.t("NEXCHIEF_ACCOUNT:EDIT.TITLE")}</div>
                <div className={style.Title}>
                    <div>{this.props.t("NEXCHIEF_ACCOUNT:EDIT.TITLE")}</div>
                </div>

                {/* container luar */}
                <div style={{ marginBottom: "20px", paddingLeft: "55px", paddingRight: "55px", paddingBottom: "55px", position: "absolute", width: "100%" }}>
                    <div className={style.List}>
                        {/* container luar */}
                        <div style={{ alignItems: "center" }}>
                            <div>
                                <button className={this.state.comp === 1 ? style.ButtonComponentActive : style.ButtonComponent} onClick={() => this._onChangeComp(1)}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_COMPANY_PROFILE")}
                                </button>
                                <button className={this.state.comp === 2 ? style.ButtonComponentActive : style.ButtonComponent} onClick={() => this._onChangeComp(2)}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_CONTACT_PERSON")}
                                </button>
                                <button className={this.state.comp === 3 ? style.ButtonComponentActive : style.ButtonComponent} onClick={() => this._onChangeComp(3)}>
                                    {this.props.t("NEXCHIEF_ACCOUNT:FIELD_PREFERENCES")}
                                </button>
                            </div>

                            <div className={style.GridContainerLuar}>
                                <div className={style.GridItemLuar}>
                                    <div className={style.SubTitle}>
                                        <div>
                                            {this.props.t("NEXCHIEF_ACCOUNT:FIELD_COMPANY_PROFILE")}
                                        </div>
                                    </div>
                                    <Form style={{ marginTop: "30px" }} onFinish={this._onFinish} ref={this.formRef}
                                        initialValues={{
                                            nik: this.props.location.state.detail.contact_person.nik,
                                            first_name: this.props.location.state.detail.contact_person.first_name,
                                            last_name: this.props.location.state.detail.contact_person.last_name,
                                            position_id: this.props.location.state.detail.contact_person.position,
                                            contact_phone: this.props.location.state.detail.contact_person.phone,
                                            contact_email: this.props.location.state.detail.contact_person.email,
                                            join_date: moment(moment(this.props.location.state.detail.contact_person.join_date).format("DD-MM-YYYY"), "DD-MM-YYYY"),
                                            resign_date: moment(moment(this.props.location.state.detail.contact_person.resign_date).format("DD-MM-YYYY"), "DD-MM-YYYY"),
                                            company_type: this.props.location.state.detail.company_profile.company_title ? this.props.location.state.detail.company_profile.company_title : "-",
                                            id: this.props.location.state.detail.code ? this.props.location.state.detail.company_profile.code : "-",
                                            company_name: this.props.location.state.detail.company_profile.name ? this.props.location.state.detail.company_profile.name : "-",
                                            phone_country_code: this.props.location.state.detail.company_profile.phone ? this.props.location.state.detail.company_profile.phone.substring(0, 3) : "-",
                                            phone: this.props.location.state.detail.company_profile.phone ? this.props.location.state.detail.company_profile.phone.substring(4) : "-",
                                            fax_country_code: this.props.location.state.detail.company_profile.fax ? this.props.location.state.detail.company_profile.fax.substring(0, 3) : "-",
                                            fax: this.props.location.state.detail.company_profile.fax ? this.props.location.state.detail.company_profile.fax.substring(4) : "-",
                                            email: this.props.location.state.detail.company_profile.email ? this.props.location.state.detail.company_profile.email : "-",
                                            npwp: this.props.location.state.detail.company_profile.npwp ? this.props.location.state.detail.company_profile.npwp : "-",
                                            address: this.props.location.state.detail.company_profile.address ? this.props.location.state.detail.company_profile.address : "-",
                                            country: this.props.location.state.detail.company_profile.country ? this.props.location.state.detail.company_profile.country : "-",
                                            district: this.props.location.state.detail.company_profile.district ? this.props.location.state.detail.company_profile.district : "-",
                                            subdistrict: this.props.location.state.detail.company_profile.subdistrict ? this.props.location.state.detail.company_profile.subdistrict : "-",
                                            urban_village: this.props.location.state.detail.company_profile.urban_village ? this.props.location.state.detail.company_profile.urban_village : "-",
                                            hamlet: this.props.location.state.detail.company_profile.hamlet ? this.props.location.state.detail.company_profile.hamlet : "-",
                                            neighbourhood: this.props.location.state.detail.company_profile.neighbourhood ? this.props.location.state.detail.company_profile.neighbourhood : "-",
                                            postal_code: this.props.location.state.detail.company_profile.postal_code ? this.props.location.state.detail.company_profile.postal_code : "-",
                                            island: this.props.location.state.detail.company_profile.island ? this.props.location.state.detail.company_profile.island : "-",
                                        }}>
                                        {this.state.comp === 1 &&
                                            <div>
                                                <CompanyProfile />
                                                <Form.Item className={style.ButtonArea}>
                                                <ButtonCancel router={ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.LIST} />
                                                <div style={{ display: "contents" }}>
                                                    <ButtonNext />
                                                </div>
                                                </Form.Item>
                                            </div>
                                        }
                                        {this.state.comp === 2 &&
                                            <div>
                                                <ContactPerson />
                                                <Form.Item className={style.ButtonArea}>
                                                <div onClick={() => this._prevComp(this.state.comp)} style={{ display: "contents" }}>
                                                    <ButtonBack />
                                                </div>
                                                <div style={{ display: "contents" }}>
                                                    <ButtonNext />
                                                </div>
                                                </Form.Item>
                                            </div>
                                        }
                                        {this.state.comp === 3 &&
                                            <div>
                                                <Preferences />
                                                <Form.Item className={style.ButtonArea}>
                                                    <div onClick={() => this._prevComp(this.state.comp)} style={{ display: "contents" }}>
                                                        <ButtonBack />
                                                    </div>
                                                    <div style={{ display: "contents" }}>
                                                        <ButtonSubmit />
                                                    </div>

                                                </Form.Item>
                                            </div>
                                        }
                                    </Form>
                                </div>
                                <DataManagementHistory />
                            </div>


                        </div>
                    </div>
                </div>

            </div>
        )

    }

}

const mapStateToProps = (state) => {
    return {
        ...stateRedux(state, "nexchief_account"),
    };

};

const mapDispatchToProps = (dispatch) => {
    return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(EditNexchiefAccount));