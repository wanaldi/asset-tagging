import React, { Component } from 'react';
import style from "./AddMappingPrincipal.module.css";
import { withTranslation } from "react-i18next";
import { rulesMappingPrincipal } from '../../../../Util/FormRules';
import { Checkbox, DatePicker, Form, Input, Select } from "antd";
import ButtonCancel from "../../../Common/Button/ButtonCancel";
import ButtonNext from "../../../Common/Button/ButtonNext";
import { postalcode, propTable, country, nexchiefaccount } from "../../../../Enum/PropertiesConstant";
import { concatPrefix, generatePrefix, getInitiate, getInitiateFromMasterData, getList, getListFromMasterData } from "../../../../Service/Util";
import { prepareComponent } from '../../../../Action/CommonAction';
import { connect } from 'react-redux';
import ChevronDown from '../../../Icon/ChevronDown';
import CustomImage from '../../../Common/Image/CustomImage';
import { ROUTER_CONSTANT } from '../../../../Enum/RouterConstant';
import DatePickerLogo from '../../../Icon/DatePickerLogo';
import ButtonBack from '../../../Common/Button/ButtonBack';
import ButtonSubmit from '../../../Common/Button/ButtonSubmit';
import ButtonCheck from '../../../Common/Button/ButtonCheck';
import ButtonAddContactPerson from '../../../Common/Button/ButtonAddContactPerson';
import ButtonContactPersonDst from '../../../Common/Button/ButtonContactPersonDst';
import * as PersonProfileService from "../../../../Service/Admin/PersonProfileService";
import { stateRedux } from '../../../../Util/ReduxState';
import { dropdownCustom, LoadingOption, selectOption } from "../../../Common/Select/CustomSelect";
import ButtonOfficialPrincipal from '../../../Common/Button/ButtonOfficialPrincipal';

const { Option } = Select;

const children = [];
for (let i = 10; i < 36; i++) {
    children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
}

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

class AddMappingPrincipal extends Component {

    state = {
        ...propTable,
        comp: 1,
        loading: false,
        jumlahContactPerson: 1,
        indexContactPersonActive: 1,

        disableUpload: false,
        selectedFile: null,
        selectedFileList: [],

        contactList: [],

        values: [null, null, null, null],

        is_pbf: null,

        ...country,

        ...nexchiefaccount,
    };

    // _onChangeComp = i => {
    //     this.setState({ comp: i });
    // };

    _nextComp = i => {
        this.setState({ comp: i + 1 });
    };

    _prevComp = i => {
        this.setState({ comp: i - 1 });
    };

    _receiveFile = (event) => {
        this.setState({ selectedFileList: event.fileList });

        if (Array.isArray(event)) return event;
        return event && event.fileList;
    };

    _dummyRequest = ({ file, onSuccess }) => {
        setTimeout(() => {
            onSuccess("ok");
        }, 0);
    };

    handleChange = info => {
        if (info.file.status === 'uploading') {
            this.setState({ loading: true });
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            getBase64(info.file.originFileObj, imageUrl =>
                this.setState({
                    imageUrl,
                    loading: false,
                }),
            );
        }
    };

    _onCheck = async (i) => {
        var id = "nik" + i;
        var nikValue = document.getElementById(id).value;
        console.log("id", id);
        console.log("nik value", nikValue);
        // this.setState({ nik: nikValue });

        const data2 = {
            page: this.state.page,
            limit: this.state.limit,
            order_by: this.state.order_by,
            nik: nikValue,
        }

        this._getData(data2);
    };

    _getData = async (value) => {
        const { token, refresh_token, dispatch } = this.props;

        // const today = moment().format("DD-MM-YYYY");

        if (this.props.permission.view || this.props.permission.view_own) {
            const response = await PersonProfileService.doGetList(token, refresh_token, value, dispatch);
            console.log("res 123", response);
            // if (response) {
            //     this.setState({ first_name: response.first_name });
            //     this.setState({ last_name: response.last_name });
            //     this.setState({ position: response.position });
            //     this.setState({ phone: response.phone });
            //     this.setState({ email: response.email });
            //     this.setState({ join_date: response.join_date });
            //     this.setState({ resign_date: response.resign_date });
            // } else {
            //     this.setState({ first_name: '' });
            //     this.setState({ last_name: '' });
            //     this.setState({ position: '' });
            //     this.setState({ phone: '' });
            //     this.setState({ email: '' });
            //     this.setState({ join_date: moment(moment(today, "DD-MM-YYYY")) });
            //     this.setState({ resign_date: moment(moment(today, "DD-MM-YYYY")) });
            // }

        }
    };

    _initiateCountry = async (page, limit, filter) => {
        console.log("masuk1");
        const currentCountry = [...this.state.dataCountry];
        const { token, refresh_token, dispatch } = this.props;

        const initiateCountry = await getInitiateFromMasterData(token, refresh_token, "country", filter, dispatch);
        console.log("initiate", initiateCountry);
        const previousCountry = await getListFromMasterData(token, refresh_token, "country", page, limit, null, filter, dispatch);
        console.log("list", previousCountry);

        if (initiateCountry || previousCountry) {
            if (!previousCountry) {
                this.setState({ lastPageCountry: true });
            } else {
                if (previousCountry.length < limit) {
                    this.setState({ lastPageCountry: true });
                } else {
                    this.setState({ lastPageCountry: false });
                }

                this.setState({
                    initiateCountry,
                    dataCountry: selectOption(currentCountry, previousCountry, "value"),
                    // TODO : Prefix country mau diuncomment
                    // prefixCountry: generatePrefix(initiateCountry),
                });
            }

            this.setState({ loading: false });
        }
    };

    _initiateNexchiefAccount = async (page, limit, filter) => {
        const currentNexchiefAccount = [...this.state.dataNexchiefAccount];
        const { token, refresh_token, dispatch } = this.props;

        const initiateNexchiefAccount = await getInitiate(token, refresh_token, "admin/nexchiefaccount", filter, dispatch);
        const previousNexchiefAccount = await getList(token, refresh_token, "admin/nexchiefaccount/search", page, limit, null, filter, dispatch);

        if (initiateNexchiefAccount || previousNexchiefAccount) {
            if (!previousNexchiefAccount) {
                this.setState({ lastPageNexchiefAccount: true });
            } else {
                if (previousNexchiefAccount.length < limit) {
                    this.setState({ lastPageNexchiefAccount: true });
                } else {
                    this.setState({ lastPageNexchiefAccount: false });
                }

                this.setState({
                    initiateNexchiefAccount,
                    dataNexchiefAccount: selectOption(currentNexchiefAccount, previousNexchiefAccount, "value"),
                    prefixNexchiefAccount: generatePrefix(initiateNexchiefAccount),
                });
            }

            this.setState({ loading: false });
        }
    };

    _getDataInitiate = async () => {
        // if (this.state.dataCompanyTitle.length === 0) await this._initiateCompanyTitle(this.state.pageCompanyTitle, this.state.limitCompanyTitle, this.state.filterCompanyTitle);
        if (this.state.dataCountry.length === 0) await this._initiateCountry(this.state.pageCountry, this.state.limitCountry, this.state.filterCountry);
        if (this.state.dataNexchiefAccount.length === 0) await this._initiateNexchiefAccount(this.state.pageNexchiefAccount, this.state.limitNexchiefAccount, this.state.filterNexchiefAccount);
        // if (this.state.dataDistrict.length === 0) await this._initiateDistrict(this.state.pageDistrict, this.state.limitDistrict, this.state.filterDistrict);
        // if (this.state.dataSubDistrict.length === 0) await this._initiateSubDistrict(this.state.pageSubDistrict, this.state.limitSubDistrict, this.state.filterSubDistrict);
        // if (this.state.dataUrbanVillage.length === 0) await this._initiateUrbanVillage(this.state.pageUrbanVillage, this.state.limitUrbanVillage, this.state.filterUrbanVillage);
        // if (this.state.dataPostalCode.length === 0) await this._initiatePostalCode(this.state.pagePostalCode, this.state.limitPostalCode, this.state.filterPostalCode);
        // if (this.state.dataIsland.length === 0) await this._initiateIsland(this.state.pageIsland, this.state.limitIsland, this.state.filterIsland);
    };

    async componentDidMount() {
        prepareComponent(this.props.dispatch, true);
        await this._getDataInitiate();

        setTimeout(() => prepareComponent(this.props.dispatch, false), 128);
    }

    _onScroll = (event, namespace) => {
        let target = event.target;

        if (!this.state.loading && target.scrollTop + target.offsetHeight === target.scrollHeight) {
            switch (namespace) {
                // case "companytitle":
                //     if (!this.state.lastPageCompanyTitle) {
                //         this.setState({ loading: true, pageCompanyTitle: this.state.pageCompanyTitle + 1 }, () => {
                //             target.scrollTo(0, target.scrollHeight);
                //             setTimeout(() => this._initiateCompanyTitle(this.state.pageCompanyTitle, this.state.limitCompanyTitle, this.state.filterCompanyTitle), 256);
                //         });
                //     }
                //     break;

                case "country":
                    if (!this.state.lastPageCountry) {
                        this.setState({ loading: true, pageCountry: this.state.pageCountry + 1 }, () => {
                            target.scrollTo(0, target.scrollHeight);
                            setTimeout(() => this._initiateCountry(this.state.pageCountry, this.state.limitCountry, this.state.filterCountry), 256);
                        });
                    }
                    break;
                
                case "nexchiefaccount":
                    if (!this.state.lastPageNexchiefAccount) {
                        this.setState({
                            loading: true,
                            pageNexchiefAccount: this.state.pageNexchiefAccount + 1
                        },
                            () => {
                                target.scrollTo(0, target.scrollHeight);
                                setTimeout(() => this._initiateNexchiefAccount(this.state.pageNexchiefAccount, this.state.limitNexchiefAccount, this.state.filterNexchiefAccount), 256);
                            }
                        );
                    }
                    break;

                // case "district":
                //     if (!this.state.lastPageDistrict) {
                //         this.setState({ loading: true, pageDistrict: this.state.pageDistrict + 1 }, () => {
                //             target.scrollTo(0, target.scrollHeight);
                //             setTimeout(() => this._initiateDistrict(this.state.pageDistrict, this.state.limitDistrict, this.state.filterDistrict), 256);
                //         });
                //     }
                //     break;

                // case "subdistrict":
                //     if (!this.state.lastPageSubDistrict) {
                //         this.setState({ loading: true, pageSubDistrict: this.state.pageSubDistrict + 1 }, () => {
                //             target.scrollTo(0, target.scrollHeight);
                //             setTimeout(() => this._initiateSubDistrict(this.state.pageSubDistrict, this.state.limitSubDistrict, this.state.filterSubDistrict), 256);
                //         });
                //     }
                //     break;

                // case "urbanvillage":
                //     if (!this.state.lastPageUrbanVillage) {
                //         this.setState({ loading: true, pageUrbanVillage: this.state.pageUrbanVillage + 1 }, () => {
                //             target.scrollTo(0, target.scrollHeight);
                //             setTimeout(() => this._initiateUrbanVillage(this.state.pageUrbanVillage, this.state.limitUrbanVillage, this.state.filterUrbanVillage), 256);
                //         });
                //     }
                //     break;

                // case "island":
                //     if (!this.state.lastPageIsland) {
                //         this.setState({ loading: true, pageIsland: this.state.pageIsland + 1 }, () => {
                //             target.scrollTo(0, target.scrollHeight);
                //             setTimeout(() => this._initiateIsland(this.state.pageIsland, this.state.limitIsland, this.state.filterIsland), 256);
                //         });
                //     }
                //     break;

                default:
                    break;
            }
        }
    };

    _onChange = (event, namespace) => {
        const { value } = event.target;

        const resetPage = 1;
        const resetFilter = null;

        switch (namespace) {
            case "country":
                clearTimeout(this.inputTimer);

                if (value.length >= 3) {
                    console.log("prefixCountry", this.state.prefixCountry);
                    this.setState({ loading: true, dataCountry: [], filterCountry: concatPrefix(this.state.prefixCountry, value), pageCountry: resetPage }, () => {
                        this.inputTimer = setTimeout(() => this._initiateCountry(this.state.pageCountry, this.state.limitCountry, this.state.filterCountry), 256);
                    });
                    console.log("state", this.state.prefixCountry);
                } else if (!value) {
                    this.setState({ loading: true, dataCountry: [], filterCountry: resetFilter, pageCountry: resetPage }, () => {
                        setTimeout(() => this._initiateCountry(this.state.pageCountry, this.state.limitCountry, this.state.filterCountry), 256);
                    });
                }
                break;
            
            case "nexchiefaccount":
                clearTimeout(this.inputTimer);
                if (value.length >= 3) {
                    this.setState({ loading: true, dataNexchiefAccount: [], filterNexchiefAccount: "name like " + value, pageNexchiefAccount: resetPage }, () => {
                        this.inputTimer = setTimeout(() => this._initiateNexchiefAccount(this.state.pageNexchiefAccount, this.state.limitNexchiefAccount, this.state.filterNexchiefAccount), 256);
                    });
                } else if (!value) {
                    this.setState({ loading: true, dataNexchiefAccount: [], filterNexchiefAccount: resetFilter, pageNexchiefAccount: resetPage }, () => {
                        setTimeout(() => this._initiateNexchiefAccount(this.state.pageNexchiefAccount, this.state.limitNexchiefAccount, this.state.filterNexchiefAccount), 256);
                    });
                }
                break;

            default:
                break;
        }
    };

    _onFinish = (value) => {
        //untuk country
        // const data = {
        //     country_id: parseInt(value.country_id),
        //     code: value.code,
        //     name: value.name,
        // };

        console.log("values", value);

        this.setState(state => {
            const values = state.values.map((item, index) => {
                if (index === this.state.comp - 1) {
                    return value;
                } else {
                    return item;
                }
            });

            return {
                values,
            };
        });


        // pengolahan contact person

        let contactPersonList = [];

        if (this.state.comp === 4) {
            let result = Object.keys(value)
                .map(key => ({ name: key, data: value[key] }));

            console.log(result);

            let k = 0;
            for (let j = 0; j < this.state.jumlahContactPerson; j++) {

                contactPersonList[j] = {
                    "nik": result[k++].data,
                    "title": result[k++].data,
                    "first_name": result[k++].data,
                    "last_name": result[k++].data,
                    "address": result[k++].data,
                    "country": result[k++].data,
                    "email": result[k++].data,
                    "phone_country_code": result[k++].data,
                    "phone": result[k++].data,
                    "position_id": result[k++].data,
                    "join_date": result[k++].data,
                    "resign_date": result[k++].data,
                }

            }


            console.log(contactPersonList);
        }

        // -------------------------------------------------------------------

        console.log("state values", this.state.values);
        if (this.state.comp !== 4) {
            this._nextComp(this.state.comp);
        }
        else {
            const data = {
                // nexchief_account_id:
                // company_profile.id:

                // nexseller_code:
                // nexseller_parent_code:
                // active_date
                // resign_date
                // geo_tree_id
                // price_group_id
                // product_category_id
                // product_class_from_id
                // product_class_thru_id
                // send_sync_data_to_email
                // nd6_sync_method
                // product_mapping:
                // salesman_mapping:
                // customer_mapping:
                // hosting_only:
                // sync_delivery_note:
                // nd6_company_id
                // nd6_branch_id
                // nexmile_version
                // nd6_version
                // product_master_version
                // additional_info -> mungkin utk mapping field

                // socket_user_id
                // socket_password
                // socket_status
                // last_dms_sync
                // last_sfa_sync
                // gromart_merchant_id
                // prefix_deleted
                // nd6_closed_date
                // email_to
                // email_cc_to

                // person_profile_id
                // nik
                // first_name
                // last_name
                // address_1
                // country_id
                // email
                // phone_country_code
                // phone
                // position_id
                // join_date
                // resign_date

            }
        }
    }

    _onAddContact = () => {
        this.setState({ jumlahContactPerson: this.state.jumlahContactPerson + 1 });
    }

    _changeIndexContactPersonActive = (i) => {
        this.setState({ indexContactPersonActive: i });
    }

    // _onFinish = async (value) => {
    //     // const data = {
    //     //     urban_village_id: parseInt(value.urbanvillage_id),
    //     //     code: value.code,
    //     // };

    //     // const { token, refresh_token, dispatch, history } = this.props;
    //     // if (this.props.permission.insert || this.props.permission.insert_own) {
    //     //     await PostalCodeService.doInsert(token, refresh_token, data, dispatch, () => history.push(ROUTER_CONSTANT.POSTAL_CODE.LIST));
    //     // }
    //     console.log("tes");
    // };

    render() {
        console.log("comp profile", this.props.location.state.company_profile);

        const rules = rulesMappingPrincipal(this.props.t);

        const AutoFillNotification = () => (
            <div className={style.GridItemLuar}>
                <div className={style.AutoFillContainer}>
                    <div className={style.AutoFill}>
                        {this.props.t("MAPPING_PRINCIPAL:FIELD_ADD_FORM")}
                    </div>
                    <div className={style.AutoFillNotif}>
                        {this.props.t("MAPPING_PRINCIPAL:FIELD_AUTO_FILL_NPWP")}
                    </div>
                </div>
            </div>
        );

        const ContactPerson = props => (
            <div hidden={this.state.indexContactPersonActive === props.iContact ? false : true}>
                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_NIK")}</label>
                <Form.Item className={style.FormItem} name={"nik" + [props.iContact]} rules={rules.nik}>
                    <Input className={style.Input} id={"nik" + props.iContact} suffix={<ButtonCheck onCheck={() => this._onCheck(props.iContact)} />} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("USER:FIELD_TITLE")}</label>
                {/* TODO: Ambil dari API */}
                <Form.Item className={style.FormItem} name={"title" + props.iContact}>
                    <Select className={style.Select} defaultValue="Mr" suffixIcon={<ChevronDown />}   >
                        <Option value="Mr">Mr</Option>
                        <Option value="Mrs">Mrs</Option>
                        <Option value="Ms">Ms</Option>
                    </Select>
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_FIRST_NAME")}</label>
                <Form.Item className={style.FormItem} name={"first_name" + props.iContact} rules={rules.first_name}>
                    <Input className={style.Input} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_LAST_NAME")}</label>
                <Form.Item className={style.FormItem} name={"last_name" + props.iContact} rules={rules.last_name}>
                    <Input className={style.Input} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_ADDRESS")}</label>
                <Form.Item className={style.FormItem} name={"address" + props.iContact} rules={rules.address}>
                    <Input.TextArea className={style.Input} autoSize={{ minRows: 1, maxRows: 6 }} />
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_COUNTRY")}</label>
                <Form.Item className={style.FormItem} name={"country" + props.iContact} rules={rules.country_id}>
                    <Select className={style.Select} suffixIcon={<ChevronDown />}
                        onPopupScroll={(event) => this._onScroll(event, "country")}
                        dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "country"))}
                        allowClear={true}
                    >
                        {!this.state.loading ? this.state.dataCountry : [...this.state.dataCountry, LoadingOption()]}
                    </Select>
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_EMAIL")}</label>
                <Form.Item className={style.FormItem} name={"email" + props.iContact} rules={rules.email}>
                    <Input className={style.Input} />
                </Form.Item>

                <Form.Item className={style.FormItem} required={true}>
                    <div style={{ display: "grid", width: "60%", gridTemplateColumns: "30% 70%" }}>
                        <div style={{ display: "inline-block" }}>
                            <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_COUNTRY_CODE")}</label>
                            <Form.Item name={"phone_country_code" + props.iContact} rules={rules.phone_country_code}>
                                <Input className={style.Input} style={{ width: "90%" }} />
                            </Form.Item>
                        </div>

                        <div>
                            <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_CORPORATE_PHONE")}</label>
                            <Form.Item name={"phone" + props.iContact} rules={rules.phone}>
                                <Input className={style.InputPhone} />
                            </Form.Item>
                        </div>
                    </div>
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_POSITION")}</label>
                <Form.Item className={style.FormItem} name={"position_id" + props.iContact} rules={rules.position}>
                    <Select className={style.Select} suffixIcon={<ChevronDown />}
                    // onPopupScroll={(event) => this._onScroll(event, "country")}
                    // dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "country"))}
                    // allowClear={true}
                    >
                        {/* {!this.state.loading ? this.state.dataCountry : [...this.state.dataCountry, LoadingOption()]} */}
                    </Select>
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_JOIN_DATE")}</label>
                <Form.Item className={style.FormItem} name={"join_date" + props.iContact} rules={rules.join_date}>
                    <DatePicker
                        format={"DD-MM-YYYY"}
                        suffixIcon={<DatePickerLogo />}
                        className={style.Input}
                        allowClear={false}
                    />
                    {/* <Input className={style.Input} defaultValue={this.state.join_date}/> */}
                </Form.Item>

                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_RESIGN_DATE")}</label>
                <Form.Item className={style.FormItem} name={"resign_date" + props.iContact} rules={rules.resign_date}>
                    <DatePicker
                        format={"DD-MM-YYYY"}
                        suffixIcon={<DatePickerLogo />}
                        className={style.Input}
                        allowClear={false}
                    />
                </Form.Item>

            </div>
        );

        return (
            <div>
                <div className={style.MenuTitle}>{this.props.t("MAPPING_PRINCIPAL:TITLE")} &#62; {this.props.t("MAPPING_PRINCIPAL:ADD.TITLE")}</div>
                <div className={style.Title}>
                    <div>{this.props.t("MAPPING_PRINCIPAL:ADD.TITLE")}</div>
                </div>

                {/* container luar */}
                <div style={{ marginBottom: "20px", paddingLeft: "55px", paddingRight: "55px", paddingBottom: "55px", position: "absolute", width: "100%" }}>
                    <div className={style.List}>
                        {/* container luar */}
                        <div style={{ alignItems: "center" }}>
                            <div className={style.HeadTable}>
                                <div className={style.GridContainerHead}>
                                    <div className={this.state.comp === 1 || this.state.comp === 2 || this.state.comp === 3 || this.state.comp === 4 ? style.GridItemHeadActive : style.GridItemHead} style={{ borderRight: "1px solid #CCCCCC" }}>
                                        <div className={this.state.comp === 1 || this.state.comp === 2 ? style.NumberHeadActive : style.NumberHead}>1</div>
                                        <div className={style.Step}>{this.props.t("MAPPING_PRINCIPAL:FIELD_COMPANY_PROFILE")}</div>
                                    </div>

                                    <div className={this.state.comp === 2 ? style.GridItemHeadActive : style.GridItemHead}>
                                        <div className={this.state.comp === 2 ? style.NumberHeadActive : style.NumberHead}>2</div>
                                        <div className={style.Step}>{this.props.t("MAPPING_PRINCIPAL:FIELD_CONTACT_PERSON")}</div>
                                    </div>

                                </div>
                            </div>

                            <div className={style.SubTitle}>
                                <div>
                                    {this.state.comp === 1 && this.props.t("MAPPING_PRINCIPAL:FIELD_COMPANY_PROFILE")}
                                    {this.state.comp === 2 && this.props.t("MAPPING_PRINCIPAL:FIELD_CONTACT_PERSON")}
                                </div>
                            </div>

                            <Form style={{ marginTop: "30px" }} id="form" onFinish={this._onFinish}
                                initialValues={{
                                    product_mapping: false,
                                    salesman_mapping: false,
                                    customer_mapping: false,
                                    hosting_only: false,
                                    sync_delivery_note: false,
                                    company_type: this.props.location.state.company_profile.company_title_name,
                                    company_name: this.props.location.state.company_profile.name,
                                    phone_country_code: this.props.location.state.company_profile.phone.substring(0, 3),
                                    phone: this.props.location.state.company_profile.phone.substring(4),
                                    fax_country_code: this.props.location.state.company_profile.fax.substring(0, 3),
                                    fax: this.props.location.state.company_profile.fax.substring(4),
                                    email: this.props.location.state.company_profile.email,
                                    npwp: this.props.location.state.company_profile.npwp,
                                    address: this.props.location.state.company_profile.address_1,
                                    country: this.props.location.state.company_profile.country_name,
                                    district: this.props.location.state.company_profile.district_name,
                                    sub_district: this.props.location.state.company_profile.sub_district_name,
                                    urban_village: this.props.location.state.company_profile.urban_village_name,
                                    hamlet: this.props.location.state.company_profile.hamlet,
                                    neighbourhood: this.props.location.state.company_profile.neighbourhood,
                                    postal_code: this.props.location.state.company_profile.postal_code,
                                    island: this.props.location.state.company_profile.island_name,
                                    status: "Active"
                                }}
                            >
                                {/* Company Profile */}
                                {this.state.comp === 1 &&
                                    <div>
                                        <div className={style.GridContainerLuar}>
                                            <div>
                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_PRINCIPAL_CODE")}</label>
                                                <Form.Item className={style.FormItem} name="principal_code" rules={rules.principal_code}>
                                                    <Input className={style.Input2} />
                                                </Form.Item>

                                            <label className={style.LabelForm}>{this.props.t("MAPPING_NEXSELLER:FIELD_NEXCHIEF_ACCOUNT_NAME")}</label>
                                            <Form.Item className={style.FormItem} name="nexchief_account_name" rules={rules.nexchief_account_name}>
                                                <Select className={style.Select2} suffixIcon={<ChevronDown />}
                                                    onPopupScroll={(event) => this._onScroll(event, "nexchiefaccount")}
                                                    dropdownRender={(menu) => dropdownCustom(menu, (value) => this._onChange(value, "nexchiefaccount"))}
                                                    allowClear={true}
                                                >
                                                    {!this.state.loading ? this.state.dataNexchiefAccount : [...this.state.dataNexchiefAccount, LoadingOption()]}
                                                </Select>
                                            </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_COMPANY_TYPE")}</label>
                                                <Form.Item className={style.FormItem} name="company_type">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_COMPANY_NAME")}</label>
                                                <Form.Item className={style.FormItem} name="company_name">
                                                    <Input className={style.InputDisabled} disabled suffix={<ButtonOfficialPrincipal />} />
                                                </Form.Item>
                                                {/* <Checkbox style={{background:"red"}}>Official Principal</Checkbox> */}

                                                <Form.Item required={true}>
                                                    <div style={{ display: "grid", width: "75%", gridTemplateColumns: "30% 70%" }}>
                                                        <div style={{ display: "inline-block" }}>
                                                            <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_COUNTRY_CODE")}</label>
                                                            <Form.Item name="phone_country_code">
                                                                <Input className={style.InputDisabled} style={{ width: "90%" }} disabled />
                                                            </Form.Item>
                                                        </div>

                                                        <div>
                                                            <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_CORPORATE_PHONE")}</label>
                                                            <Form.Item name="phone">
                                                                <Input className={style.InputPhoneDisabled} disabled />
                                                            </Form.Item>
                                                        </div>
                                                    </div>
                                                </Form.Item>

                                                <Form.Item required={true}>
                                                    <div style={{ display: "grid", width: "75%", gridTemplateColumns: "30% 70%" }}>
                                                        <div style={{ display: "inline-block" }}>
                                                            <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_COUNTRY_CODE")}</label>
                                                            <Form.Item name="fax_country_code">
                                                                <Input className={style.InputDisabled} style={{ width: "90%" }} disabled />
                                                            </Form.Item>
                                                        </div>

                                                        <div style={{ display: "inline-block" }}>
                                                            <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_FAX_NUMBER")}</label>
                                                            <Form.Item name="fax" >
                                                                <Input className={style.InputPhoneDisabled} disabled />
                                                            </Form.Item>
                                                        </div>
                                                    </div>

                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_EMAIL")}</label>
                                                <Form.Item className={style.FormItem} name="email">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_NPWP")}</label>
                                                <Form.Item className={style.FormItem} name="npwp">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_COMPANY_LOGO")}</label>
                                                <Form.Item className={style.FormItem} name="company_logo">
                                                    <CustomImage logo={this.props.location.state.company_profile && this.props.location.state.company_profile.logo} />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_STATUS")}</label>
                                                <Form.Item className={style.FormItem} name="status">
                                                    <Select className={style.Select2} suffixIcon={<ChevronDown />}   >
                                                        <Option value="Active">Active</Option>
                                                        <Option value="Non Active">Non Active</Option>
                                                    </Select>
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_ACTIVATION_DATE")}</label>
                                                <Form.Item className={style.FormItem} name="activation_date" rules={rules.activation_date}>
                                                    <DatePicker
                                                        // defaultValue={moment(moment(this.state.join_date).format("DD-MM-YYYY"), "DD-MM-YYYY")}
                                                        format={"DD-MM-YYYY"}
                                                        suffixIcon={<DatePickerLogo />}
                                                        className={style.Input2}
                                                        allowClear={false}
                                                    />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_RESIGNATION_DATE")}</label>
                                                <Form.Item className={style.FormItem} name="resignation_date" rules={rules.resignantion_date}>
                                                    <DatePicker
                                                        // defaultValue={moment(moment(this.state.join_date).format("DD-MM-YYYY"), "DD-MM-YYYY")}
                                                        format={"DD-MM-YYYY"}
                                                        suffixIcon={<DatePickerLogo />}
                                                        className={style.Input2}
                                                        allowClear={false}
                                                    />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_ADDRESS")}</label>
                                                <Form.Item className={style.FormItem} name="address">
                                                    <Input.TextArea className={style.InputDisabled} autoSize={{ minRows: 1, maxRows: 6 }} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_COUNTRY")}</label>
                                                <Form.Item className={style.FormItem} name="country">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_DISTRICT")}</label>
                                                <Form.Item className={style.FormItem} name="district">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_SUB_DISTRICT")}</label>
                                                <Form.Item className={style.FormItem} name="sub_district">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_URBAN_VILLAGE")}</label>
                                                <Form.Item className={style.FormItem} name="urban_village">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_HAMLET")}</label>
                                                <Form.Item className={style.FormItem} name="hamlet">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_NEIGHBOURHOOD")}</label>
                                                <Form.Item className={style.FormItem} name="neighbourhood">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_POSTAL_CODE")}</label>
                                                <Form.Item className={style.FormItem} name="postal_code">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>

                                                <label className={style.LabelForm}>{this.props.t("MAPPING_PRINCIPAL:FIELD_ISLAND")}</label>
                                                <Form.Item className={style.FormItem} name="island">
                                                    <Input className={style.InputDisabled} disabled />
                                                </Form.Item>
                                            </div>
                                            <AutoFillNotification />
                                        </div>

                                        <Form.Item className={style.ButtonArea}>
                                            <ButtonCancel router={ROUTER_CONSTANT.MAPPING_PRINCIPAL.LIST} />
                                            <div style={{ display: "contents" }}>
                                                <ButtonNext />
                                            </div>
                                        </Form.Item>
                                    </div>
                                }

                                {/* Contact Person */}
                                {this.state.comp === 2 &&
                                    <div>
                                        {/* <ButtonContactPerson1 /> */}
                                        {/* {Array.from({ length: this.state.jumlahContactPerson }, (_, i) => 
                                        <div><ButtonContactPersonDst index={i + 1} /> <ContactPerson /></div>)} */}
                                        {Array.from({ length: this.state.jumlahContactPerson }, (_, i) =>
                                            <ButtonContactPersonDst index={i + 1} indexActive={this.state.indexContactPersonActive} onChangeActive={() => this._changeIndexContactPersonActive(i + 1)} />)}
                                        <ButtonAddContactPerson onAddContact={() => this._onAddContact()} />

                                        {Array.from({ length: this.state.jumlahContactPerson }, (_, i) =>
                                            <ContactPerson iContact={i + 1} />
                                        )}

                                        <Form.Item className={style.ButtonArea}>
                                            <div onClick={() => this._prevComp(this.state.comp)} style={{ display: "contents" }}>
                                                <ButtonBack />
                                            </div>
                                            <div style={{ display: "contents" }}>
                                                <ButtonSubmit />
                                            </div>
                                        </Form.Item>
                                    </div>
                                }

                            </Form>

                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ...stateRedux(state, "mapping_principal"),
    };

};

const mapDispatchToProps = (dispatch) => {
    return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(AddMappingPrincipal));