import { Button, Form, Input, Spin } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { doPostAuthorize, doPostVerify } from "../../../Action/AuthenticationAction";
import { setLanguage } from "../../../Action/CommonAction";
import { ROUTER_CONSTANT } from "../../../Enum/RouterConstant";
import style from "./Login.module.css";
import { ReactComponent as ReactLogo } from '../../../Asset/nc-dummy-logo.svg';
import { CRYPTO_RANDOM } from "../../../Enum/PropertiesConstant";

class Login extends Component {
  constructor(props) {
    super(props);

    if (props.isAuthenticated) {
      if (props.lastPage) {
        props.history.push(props.lastPage);
      } else {
        props.history.push(ROUTER_CONSTANT.HOME);
      }
    }
  }

  _formRef = React.createRef();
  _usernameRef = React.createRef();

  async componentDidMount() {
    this._usernameRef.current.focus();
    if (!this.props.isAuthenticated) await this.props.doPostAuthorize(CRYPTO_RANDOM);

    if (sessionStorage.getItem("username") && sessionStorage.getItem("password")) {
      this._formRef.current.setFieldsValue({
        username: sessionStorage.getItem("username"),
        password: sessionStorage.getItem("password"),
        remember: true,
      });
    }
  }

  _changeLanguage = (chosenLanguage) => {
    this.props.setLanguage(chosenLanguage);
  };

  _onFinish = (value) => {
    const { username, password, remember } = value;
    this.props.doPostVerify(username, password, remember, this.props.history);
  };

  // _handleSubmit = async (values) => {
  //   this.props.dispatch({ type: ActionType.TRUE_AUTHENTICATED });
  //   this.props.history.push(ROUTER_CONSTANT.HOME);
  // }

  render() {
    return (
      <Spin size="large" style={{ position: "absolute", top: 128 }} spinning={this.props.auth_loading}>
        <div className={style.Container}>
          <div className={style.Wrapper}>
            <div className={style.Body}>
              <div className={style.Logo}>
                <ReactLogo/>
              </div>
              <div className={style.Title}>{this.props.t("COMMON:WELCOME_BACK")}</div>
              <Form layout="vertical" ref={this._formRef} onFinish={this._onFinish}>
                <Form.Item name="username">
                  <Input className={style.Input} placeholder={this.props.t("COMMON:FIELD_USERNAME")} ref={this._usernameRef} disabled={this.props.auth_loading} />
                </Form.Item>
                <Form.Item name="password" style={{ marginBottom: 0 }}>
                  <Input.Password className={style.Input} placeholder={this.props.t("COMMON:FIELD_PASSWORD")} disabled={this.props.auth_loading} />
                </Form.Item>
                <div>
                  <p>{this.props.t("COMMON:PASSWORD_MIN_CHAR")}<br></br>{this.props.t("COMMON:PASSWORD_CONTAIN")}</p>
                </div>
                <Form.Item>
                  <Link to={ROUTER_CONSTANT.FORGET_PASSWORD}>
                    <Button size={"small"} className={style.ForgetPassword} type={"link"}>
                      {this.props.t("COMMON:FORGET_PASSWORD")}
                    </Button>
                  </Link>
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit" style={{ width: "100%", backgroundColor: "#3399FF", borderColor: "#3399FF" }} disabled={this.props.auth_loading}>
                    {this.props.t("COMMON:BUTTON_LOGIN")}
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>
          <div className={style.Copyright}>Copyright © 2020 NexSoft. All rights reserved.</div>
        </div>
      </Spin>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authentication.isAuthenticated,
    language: state.authentication.language,
    auth_loading: state.common.auth_loading,
    lastPage: state.common.lastPage,
  };
};

// const mapDispatchToProps = (dispatch) => {
//   return {
//     dispatch
//   }
// }

export default connect(mapStateToProps, { setLanguage, doPostAuthorize, doPostVerify })(withTranslation()(Login));
// export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Login));
