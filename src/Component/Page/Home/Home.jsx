import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { stateRedux } from "../../../Util/ReduxState";

class Home extends Component {

  render() {

    return this.props.permission.view ?
      <div style={{ padding: "90px 40px 40px 40px" }}>
        <div style={{ background: "#FFFFFF 0% 0% no-repeat padding-box", boxShadow: "0px 3px 6px #00000029", borderRadius: "5px",
opacity: 1, minHeight: "70vh" }}>
          <h2 style={{fontWeight:400, fontSize:"16px", margin:"auto auto 0px 40px", paddingTop:"40px"}}>Welcome to Nexsoft Nexchief Website</h2>
          <h1 style={{ fontWeight: 500, fontSize: "24px", marginLeft:"40px" }}>{`${this.props.first_name} ${this.props.last_name}`}</h1>
        </div>
      </div>
      :
      <div style={{padding:"90px 40px 40px 40px"}}>
        <div style={{backgroundColor:"white", minHeight:"70vh"}}>No Access!</div>
      </div>;
  }
}

const mapStateToProps = (state) => {
  return {
    ...stateRedux(state, "home"),
    first_name: state.authentication.first_name,
    last_name: state.authentication.last_name,
  };
};

const mapDispatchToProps = (dispatch) => {
  return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Home));
