import { Layout, PageHeader } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { ReactComponent as UserLogo } from '../../../Asset/User.svg';
import { RANDOM_ID } from "../../../Enum/PropertiesConstant";
import style from './Headmenu.module.css';

class Headmenu extends Component {
  state = {
    hideText: window.innerWidth > 768,
  };
  render() {

    const User = () => (
      <div style={{ display:"flex", alignItems: "center", marginTop:"5px" }}>
        <div style={{marginRight: "10px", textAlign:"right"}}>
          <div style={{fontSize:"10px", color:"#9F9F9F", letterSpacing: "1.5px"}}>{`${this.props.user_id}`}</div>
          <div className={style.Name}>{`${this.props.first_name} ${this.props.last_name}`}</div>
        </div>
        <div><UserLogo /></div>
      </div>
    );

    return (
      <Layout.Header style={{ backgroundColor: "#ffffff", padding: 10, height: 80 }}>
        <PageHeader
          style={{ padding: "8px 32px" }}
          // onBack={() => this.props.onCollapse()}
          // backIcon={this.props.collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
          // className={style.hideOnMobile}
          // title = {}
          extra={[<User key={RANDOM_ID(8)} />]}
        />

        {/* <PageHeader
          style={{ padding: "8px 32px" }}
          onBack={() => this.props.onCollapse()}
          backIcon={this.props.collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
          className={style.hideOnDesktop}
          extra={[<User />]}
        /> */}
      </Layout.Header>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.authentication.token,
    first_name: state.authentication.first_name,
    last_name: state.authentication.last_name,
    user_id: state.authentication.user,
    role: state.authentication.role,
  };
};

const mapDispatchToProps = (dispatch) => {
  return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(Headmenu));
