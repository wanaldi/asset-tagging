import { Menu } from "antd";
import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { setOpenKeys, setOpenPage, setSelectedKeys } from "../../../Action/CommonAction";
import { DEFAULT_LANGUAGE } from "../../../Enum/PropertiesConstant";
import { ROUTER_CONSTANT } from "../../../Enum/RouterConstant";
import style from "./MenuItem.module.css"
import HomeLogo from "../../Icon/HomeLogo";
import AccountLogo from "../../Icon/AccountLogo";
import RoleLogo from "../../Icon/RoleLogo";
import DataGroupLogo from "../../Icon/DataGroupLogo";
import UserLogo from "../../Icon/UserLogo";
import MappingLogo from "../../Icon/MappingLogo";
import SystemUserLogo from "../../Icon/SystemUserLogo";

class MenuItem extends Component {
  state = {
    openKeys: [],
    selectedKeys: [],
  };

  _onSelect = (select) => {
    this.props.setSelectedKeys(select.selectedKeys);
  };

  _setOpenMenu = (openUrl, openKeys) => {
    this.props.setOpenPage(openUrl);
    this.props.setOpenKeys([openKeys]);
  };

  render() {
    const { language, list_menu } = this.props;

    console.log("listmenu", list_menu);

    return (
      <Menu
        theme="light"
        mode="inline"
        triggerSubMenuAction="click"
        onSelect={this._onSelect}
        selectedKeys={this.props.selectedKeys}
        defaultOpenKeys={["master", "sync", "presentation", ...this.props.openKeys]}
        inlineIndent={10}
        className={style.Menu}
      >
        <Menu.Item
          key="home"
          onClick={() => this._setOpenMenu(ROUTER_CONSTANT.HOME, "home")}
          className={style.Item}
        >
          <NavLink exact to={ROUTER_CONSTANT.HOME} style={{ display: "flex", marginLeft: "12px" }}>
            <HomeLogo isActive={window.location.pathname.includes(ROUTER_CONSTANT.HOME)} style={{ minWidth: "22px", maxWidth: "22px", minHeight: "22px", maxHeight: "22px", marginRight: "0px"}} />
            {language === DEFAULT_LANGUAGE && !this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.HOME)? style.MenuItemActive : style.MenuItem}>Beranda</div>
              : ""
            }

            {language !== DEFAULT_LANGUAGE && this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.HOME) ? style.MenuItemActive : style.MenuItem}>Home</div>
              : ""
            }

          </NavLink>
        </Menu.Item>

        <Menu.Item
          key="account"
          onClick={() => this._setOpenMenu(ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.LIST, "account")}
          className={style.Item}
        >
          <NavLink exact to={ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.LIST} style={{ display: "flex", marginLeft: "12px" }}>
            <AccountLogo isActive={window.location.pathname.includes(ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.LIST)} style={{ minWidth: "22px", maxWidth: "22px", minHeight: "22px", maxHeight: "22px", marginRight: "0px" }} />
            {language === DEFAULT_LANGUAGE && !this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.LIST) ? style.MenuItemActive : style.MenuItem}>Akun</div>
              : ""
            }

            {language !== DEFAULT_LANGUAGE && this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.LIST) ? style.MenuItemActive : style.MenuItem}>Account</div>
              : ""
            }

          </NavLink>
        </Menu.Item>

        <Menu.Item
          key="role"
          onClick={() => this._setOpenMenu(ROUTER_CONSTANT.ROLE.LIST, "role")}
          className={style.Item}
        >
          <NavLink exact to={ROUTER_CONSTANT.ROLE.LIST} style={{ display: "flex", marginLeft: "12px" }}>
            <RoleLogo isActive={window.location.pathname.includes(ROUTER_CONSTANT.ROLE.LIST)} style={{ minWidth: "22px", maxWidth: "22px", minHeight: "22px", maxHeight: "22px", marginRight: "0px" }} />
            {language === DEFAULT_LANGUAGE && !this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.ROLE.LIST) ? style.MenuItemActive : style.MenuItem}>Role</div>
              : ""
            }

            {language !== DEFAULT_LANGUAGE && this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.ROLE.LIST) ? style.MenuItemActive : style.MenuItem}>Role</div>
              : ""
            }

          </NavLink>
        </Menu.Item>

        <Menu.Item
          key="datagroup"
          onClick={() => this._setOpenMenu(ROUTER_CONSTANT.DATA_GROUP.LIST, "datagroup")}
          className={style.Item}
        >
          <NavLink exact to={ROUTER_CONSTANT.DATA_GROUP.LIST} style={{ display: "flex", marginLeft: "12px" }}>
            <DataGroupLogo isActive={window.location.pathname.includes(ROUTER_CONSTANT.DATA_GROUP.LIST)} style={{ minWidth: "22px", maxWidth: "22px", minHeight: "22px", maxHeight: "22px", marginRight: "0px" }} />
            {language === DEFAULT_LANGUAGE && !this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.DATA_GROUP.LIST) ? style.MenuItemActive : style.MenuItem}>Grup Data</div>
              : ""
            }

            {language !== DEFAULT_LANGUAGE && this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.DATA_GROUP.LIST) ? style.MenuItemActive : style.MenuItem}>Data Group</div>
              : ""
            }

          </NavLink>
        </Menu.Item>

        <Menu.Item
          key="user"
          onClick={() => this._setOpenMenu(ROUTER_CONSTANT.CLIENT_USER.LIST, "user")}
          className={style.Item}
        >
          <NavLink exact to={ROUTER_CONSTANT.CLIENT_USER.LIST} style={{ display: "flex", marginLeft: "12px" }}>
            <UserLogo isActive={window.location.pathname.includes(ROUTER_CONSTANT.CLIENT_USER.LIST)} style={{ minWidth: "22px", maxWidth: "22px", minHeight: "22px", maxHeight: "22px", marginRight: "0px" }} />
            {language === DEFAULT_LANGUAGE && !this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.CLIENT_USER.LIST) ? style.MenuItemActive : style.MenuItem}>Pengguna</div>
              : ""
            }

            {language !== DEFAULT_LANGUAGE && this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.CLIENT_USER.LIST) ? style.MenuItemActive : style.MenuItem}>User</div>
              : ""
            }

          </NavLink>
        </Menu.Item>

        <Menu.Item
          key="mapping"
          onClick={() => this._setOpenMenu(ROUTER_CONSTANT.MAPPING_NEXSELLER.LIST, "mapping")}
          className={style.Item}
        >
          <NavLink exact to={ROUTER_CONSTANT.MAPPING_NEXSELLER.LIST} style={{ display: "flex", marginLeft: "12px" }}>
            <MappingLogo isActive={window.location.pathname.includes(ROUTER_CONSTANT.MAPPING_NEXSELLER.LIST)} style={{ minWidth: "22px", maxWidth: "22px", minHeight: "22px", maxHeight: "22px", marginRight: "0px"}} />
            {language === DEFAULT_LANGUAGE && !this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.MAPPING_NEXSELLER.LIST) ? style.MenuItemActive : style.MenuItem}>Mapping</div>
              : ""
            }

            {language !== DEFAULT_LANGUAGE && this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.MAPPING_NEXSELLER.LIST) ? style.MenuItemActive : style.MenuItem}>Mapping</div>
              : ""
            }

          </NavLink>
        </Menu.Item>

        <Menu.Item
          key="systemuser"
          onClick={() => this._setOpenMenu(ROUTER_CONSTANT.LOGIN_LOG.LIST, "systemuser")}
          className={style.Item}
        >
          <NavLink exact to={ROUTER_CONSTANT.LOGIN_LOG.LIST} style={{ display: "flex", marginLeft: "12px" }}>
            <SystemUserLogo isActive={window.location.pathname.includes(ROUTER_CONSTANT.LOGIN_LOG.LIST)} style={{ minWidth: "22px", maxWidth: "22px", minHeight: "22px", maxHeight: "22px", marginRight: "0px" }} />
            {language === DEFAULT_LANGUAGE && !this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.LOGIN_LOG.LIST) ? style.MenuItemActive : style.MenuItem}>Sistem Pengguna</div>
              : ""
            }

            {language !== DEFAULT_LANGUAGE && this.props.collapsed ?
              <div className={window.location.pathname.includes(ROUTER_CONSTANT.LOGIN_LOG.LIST) ? style.MenuItemActive : style.MenuItem}>System User</div>
              : ""
            }

          </NavLink>
        </Menu.Item>

        {/* {list_menu.map((item) => {

          const icon_name = String(item.menu_code).substr(6);
          console.log("code", item.menu_code);
          console.log("icon", icon_name);
          console.log("item", item);
          
          return (
            <Menu.Item key={item.id} onClick={() => this._setOpenMenu(item.url, item.menu_code)}
            >
              <NavLink exact to={item.url}>
                {<Label text={language === DEFAULT_LANGUAGE ? item.name : item.en_name} />}
              </NavLink>
            </Menu.Item>
          );
        })} */}

        {/* {list_menu.map((parent) => {
          return (
            <Menu.SubMenu
              key={parent.menu_code}
              icon={<i className={parent.icon_name} style={{ marginRight: 12 }} />}
              title={<Label text={language === DEFAULT_LANGUAGE ? parent.name : parent.en_name} weight="medium" />}
            >
              {parent.service_menu.map((service) => {
                return (
                  <Menu.SubMenu key={service.menu_code} title={<Label text={language === DEFAULT_LANGUAGE ? service.name : service.en_name} weight="medium" />}>
                    {service.menu_item.map((item) => {
                      return (
                        <Menu.Item key={item.menu_code} onClick={() => this._setOpenMenu(item.url, service.menu_code)}>
                          <NavLink exact to={item.url}>
                            {<Label text={language === DEFAULT_LANGUAGE ? item.name : item.en_name} />}
                          </NavLink>
                        </Menu.Item>
                      );
                    })}
                  </Menu.SubMenu>
                );
              })}
            </Menu.SubMenu>
          );
        })} */}

        

      </Menu>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.authentication.language,
    list_menu: state.authentication.list_menu,
    openKeys: state.common.openKeys,
    selectedKeys: state.common.selectedKeys,
  };
};

export default connect(mapStateToProps, { setOpenKeys, setSelectedKeys, setOpenPage })(MenuItem);
