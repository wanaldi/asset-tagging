import React, { Component, Fragment } from "react";
import { Layout } from "antd";
import MenuItem from "./MenuItem";
import style from "./Sidemenu.module.css";
import { APP_NAME } from "../../../Enum/AppConstant";
import Logo from "../../../Asset/nc-dummy-logo.svg";
import { doLogout } from "../../../Action/AuthenticationAction";
import { connect } from "react-redux";
import { withTranslation } from "react-i18next";
import { ReactComponent as LogoutLogo } from '../../../Asset/Logout.svg';
import ButtonCollapsed from "../../Common/Button/ButtonCollapsed";

class Sidemenu extends Component {

  state = {
    collapsed: false,
  };

  _onCollapse = () => {
    this.setState({ collapsed: !this.state.collapsed });
  }

  render() {

    return (
      <Fragment>
        <Layout.Sider
          theme="light"
          style={{ marginBottom:0, background: "#FFFFFF 0% 0% no-repeat padding-box", boxShadow: "0px 3px 6px #00000029",
            opacity: 1, position: "fixed", zIndex: 1, height: "100%", transition:"0.5s"
          }}
          // className={style.hideOnMobile}
          width={this.props.collapsed ? "70px" : "220px"}
          trigger={null}
        >
          
          <div style={{padding:"5px 14.5px", display:"flex", marginBottom:"6vh", marginTop:"5px"}}>
            <img src={Logo} style={{ width: "50px", height: "53px" }} />
            {this.props.collapsed ?
              ""
              :
              <div style={{ paddingLeft: "5px", paddingTop: "17px", display: "flex" }}>
                <label style={{ fontWeight: "bold", fontSize: "18px", color: "#4E5B66" }}>{APP_NAME}</label>
              </div>
            }
          </div>

          <ButtonCollapsed collapsed={this.props.collapsed} onCollapse={this.props.onCollapse}/>

          <div>
            <MenuItem {...this.props} />
          </div>

          <button className={style.buttonLogout} onClick={() => doLogout(this.props.token, this.props.dispatch)}>
            <LogoutLogo style={{ marginLeft: "17px", minWidth: "22px", maxWidth: "22px"}}/>
            {this.props.collapsed ? '' : <p style={{fontSize:"14px", margin:"auto auto auto 10px", color:"black", fontWeight:"500"}}>{this.props.t("COMMON:BUTTON_LOGOUT")}</p>}
          </button>
        </Layout.Sider>

        {/* <Drawer
          title={<Label text={APP_NAME} weight="bold" />}
          placement="left"
          closable={true}
          visible={this.props.collapsed}
          onClose={this.props.onCollapse}
          getContainer={false}
          bodyStyle={{ padding: 0, backgroundColor: "#001529" }}
          className={style.hideOnDesktop}
        >

          <PageHeader
            title={<Label text={APP_NAME} weight="bold" />}
            avatar={{ src: Logo }}
            style={{ padding: "8px 32px" }}
            className={style.hideOnDesktop}
          />

          <MenuItem {...this.props} />
        </Drawer> */}
      </Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch
  }
}

export default connect(null, mapDispatchToProps)(withTranslation()(Sidemenu));