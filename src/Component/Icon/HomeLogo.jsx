import React, { Component } from "react";

class HomeLogo extends Component {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width={22}
                height={22}
                viewBox="0 0 21.478 25"
            >
                <path
                    d="M2.547 24.5A2.047 2.047 0 01.5 22.452V9.1a2.047 2.047 0 01.768-1.6L9.461.947a2.046 2.046 0 012.558 0L20.21 7.5a2.047 2.047 0 01.768 1.6v13.351a2.047 2.047 0 01-2.047 2.048zm0-15.4v13.351h16.384V9.1l-8.192-6.553h0zm2.857 6.063a5.336 5.336 0 115.336 5.336 5.335 5.335 0 01-5.34-5.335zm2 0a3.336 3.336 0 103.336-3.335 3.34 3.34 0 00-3.34 3.336z"
                    fill={this.props.isActive ? "#39f" : "#999"}
                    stroke="rgba(0,0,0,0)"
                    strokeMiterlimit={10}
                />
            </svg >
        )
    }
};

export default HomeLogo;