import React, { Component } from "react";

class ClearSearchLogo extends Component {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width={20}
                height={20}
                viewBox="0 0 20 20"
                onClick={this.props.onClear}
            >
                <circle cx={10} cy={10} r={10} fill="#4e5b66" />
                <path
                    d="M15.71 14.297a1 1 0 01-1.412 1.412l-4.3-4.3-4.3 4.3a1 1 0 01-1.408-1.412l4.3-4.3-4.3-4.295A1 1 0 015.703 4.29l4.3 4.3 4.3-4.3a1 1 0 011.407 1.412l-4.3 4.3z"
                    fill="#fff"
                />
            </svg>
        )
    }
};

export default ClearSearchLogo;