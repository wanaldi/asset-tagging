import React, { Component } from "react";

class AccountLogo extends Component {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width={22}
                height={22}
                viewBox="0 0 19.698 24"
            >
                <path
                    d="M2 18v4h10a7.2 7.2 0 002.3 2H0v-6a8 8 0 012.9-6.2H3a6.3 6.3 0 0010.2 0l.4.4.5.5.4.5.4.5.2.3-.2.2a3.2 3.2 0 01-1.1.9h-.3l-.4-.7-1.5.8h-.1a9 9 0 01-8.2-.8A5.9 5.9 0 002 18"
                    fill={this.props.isActive ? "#39f" : "#999"}
                />
                <path
                    d="M8 0a6 6 0 00-6 6 1.7 1.7 0 000 .3A6.002 6.002 0 1014 6a6 6 0 00-6-6m0 10a4 4 0 114-4 4 4 0 01-4 4"
                    fill={this.props.isActive ? "#39f" : "#999"}
                />
                <path
                    className="prefix__a"
                    d="M19.5 16a.4.4 0 00-.4-.3h-.7l-1.4-.3a3.4 3.4 0 01-1.4-1H15a3.5 3.5 0 01-1.3 1l-1.4.3h-.7a.4.4 0 00-.3.3 6.8 6.8 0 00-.2 1.7 6.7 6.7 0 003.4 5.8l.7.4h.3l.7-.4a6.7 6.7 0 003.3-7.5m-3.9 6.4h-.5a5.4 5.4 0 01-2.8-4.7v-.5a5 5 0 001.9-.4l1.1-.7 1.1.7a5 5 0 001.9.4 5.4 5.4 0 01-2.7 5.2"
                    fill={this.props.isActive ? "#39f" : "#999"}
                />
            </svg>
        )
    }
};

export default AccountLogo;