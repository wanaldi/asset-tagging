import React, { Component } from "react";

class MappingLogo extends Component {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width={22}
                height={22}
                viewBox="0 0 23.9 24.1"
            >
            <path
                d="M19.3 14.9a4.6 4.6 0 00-4.5 3.7 2.5 2.5 0 01-2.4-2.5v-8a2.5 2.5 0 012.4-2.5 4.6 4.6 0 100-2 4.5 4.5 0 00-4.4 4.5v3H9.1a4.6 4.6 0 100 2h1.3v3.1a4.5 4.5 0 004.4 4.5 4.6 4.6 0 104.5-5.7m0-13a2.6 2.6 0 11-2.6 2.6A2.6 2.6 0 0119.3 2M4.6 14.7a2.6 2.6 0 112.6-2.6 2.6 2.6 0 01-2.6 2.6m14.6 7.4a2.6 2.6 0 112.6-2.6 2.6 2.6 0 01-2.6 2.6"
                fill={this.props.isActive ? "#39f" : "#999"}
            />
            </svg>
        )
    }
};

export default MappingLogo;