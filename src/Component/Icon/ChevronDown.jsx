import React, { Component } from "react";

class ChevronDown extends Component {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width={23.959}
                height={14.829}
                viewBox="0 0 23.959 14.829"
            >
                <path
                    d="M11.999 9.169L3.417.587a2 2 0 00-2.83 0 2 2 0 000 2.83l11.412 11.412L23.373 3.455a2 2 0 000-2.83 2 2 0 00-2.83 0z"
                    fill="#39f"
                />
            </svg>
        )
    }
};

export default ChevronDown;