import React, { Component } from "react";

class SearchLogo extends Component {
    render() {
        return (
            <svg
                xmlns="http://www.w3.org/2000/svg"
                width={15.999}
                height={16}
                viewBox="0 0 15.999 16"
            >
                <path
                    d="M8.639 1.481A5.063 5.063 0 107.612 9.41l.984.983a1.976 1.976 0 00.572 1.6l3.417 3.417a2 2 0 002.829-2.829l-3.417-3.412a1.975 1.975 0 00-1.6-.572l-.984-.984a5.058 5.058 0 00-.774-6.132zm-6.19 6.194a3.7 3.7 0 115.23 0 3.7 3.7 0 01-5.23 0zm6.377.782L10.07 9.7l.278-.07a.981.981 0 01.938.255l3.417 3.417a.99.99 0 01-1.4 1.4l-3.417-3.417a.979.979 0 01-.254-.938l.07-.279-1.245-1.246z"
                    fill="#999"
                />
            </svg>
        )
    }
};

export default SearchLogo;