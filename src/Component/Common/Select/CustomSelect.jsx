import { Input, Select, Spin } from "antd";
import React from "react";

export const selectOption = (currentData, previousData, label) => {
  let data = [...currentData];

  console.log("prevdata", previousData);

  if (previousData) {
    for (let index = 0; index < previousData.length; index++) {
      data.push(
        <Select.Option key={previousData[index].id} value={previousData[index].id}>
          {previousData[index][label]}
        </Select.Option>
      );
    }
  }

  return data;
};

export const otherSelectOption = (currentData, previousData, label) => {
  let data = [...currentData];

  if (previousData) {
    for (let index = 0; index < previousData.length; index++) {
      data.push(
        <Select.Option key={previousData[index][label]} value={previousData[index][label]}>
          {previousData[index][label]}
        </Select.Option>
      );
    }
  }

  return data;
};

export const selectOptionRole = (currentData, previousData, label) => {
  let data = [...currentData];

  if (previousData) {
    for (let index = 0; index < previousData.length; index++) {
      data.push(
        <Select.Option key={previousData[index].role_id} value={previousData[index].value}>
          {previousData[index][label]}
        </Select.Option>
      );
    }
  }

  return data;
};

export const LoadingOption = () => {
  return (
    <Select.Option key="loading">
      <Spin size="small" />
    </Select.Option>
  );
};

export const dropdownCustom = (menu, search, value) => (
  <>
    <div style={{ padding: "2px 8px" }}>
      <Input placeholder="Search" size="middle" onChange={search} allowClear defaultValue={value} />
    </div>
    <div style={{ padding: 2 }}>{menu}</div>
  </>
);
