import { Alert } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";

class BannerUserExist extends Component {
  state = {};
  render() {
    return (
      <Alert
        banner
        type={this.props.status ? "success" : "error"}
        message={this.props.status ? this.props.t("USER:CHECK_USER.EXIST") : this.props.t("USER:CHECK_USER.NOT_EXIST")}
      />
    );
  }
}

export default withTranslation()(BannerUserExist);
