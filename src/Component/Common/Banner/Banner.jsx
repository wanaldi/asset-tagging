import { Alert } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";

class Banner extends Component {
  state = {};
  render() {
    return (
      <Alert
        banner
        type={this.props.status === "A" ? "success" : "error"}
        message={this.props.status === "A" ? this.props.t("COMMON:ACTIVE") : this.props.t("COMMON:NOT_ACTIVE")}
      />
    );
  }
}

export default withTranslation()(Banner);
