import { CaretDownOutlined, CaretUpOutlined, MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import { Button, Card, Col, Form, Input, Radio, Row, Select, Skeleton, Space, Tooltip } from "antd";
import React, { Component, Fragment } from "react";
import { withTranslation } from "react-i18next";
import Label from "../Label/Label";

class FilterAudit extends Component {
  render() {
    const { state, permission, initiateList } = this.props;

    return (
      <Card size="small">
        {this.props.table_loading ? (
          <Skeleton paragraph={{ rows: 2, width: "100%" }} title={false} active />
        ) : (
          <Row>
            <Col md={24} lg={12}>
              <Form
                layout="vertical"
                colon={false}
                initialValues={this.props.initialValues}
                onFinish={this.props.function.onFilter}
                onFieldsChange={this.props.function.onChangeFilter}
              >
                <Form.List name="search">
                  {(fields, { add, remove }) => (
                    <Fragment>
                      {fields.map((field) => (
                        <Space key={field.key} style={{ display: "flex" }} align="baseline">
                          <Form.Item
                            {...field}
                            name={[field.name, "search_by"]}
                            fieldKey={[field.fieldKey, "search_by"]}
                            rules={[{ required: true, message: <Label text={this.props.t("COMMON:NOT_EMPTY")} size="smaller" /> }]}
                          >
                            <Select
                              allowClear
                              style={{ textTransform: "capitalize", width: 196 }}
                              placeholder={this.props.t("COMMON:FIELD_SEARCH_BY")}
                              disabled={permission.view ? false : !permission.view_own}
                            >
                              {this.props.valid_search_by &&
                                this.props.valid_search_by.map((result, i) => (
                                  <Select.Option key={i} value={result.replace(/_/g, " ")} style={{ textTransform: "capitalize" }}>
                                    {result && result.replace(/_/g, " ")}
                                  </Select.Option>
                                ))}
                            </Select>
                          </Form.Item>
                          <Form.Item
                            {...field}
                            name={[field.name, "search_key"]}
                            fieldKey={[field.fieldKey, "search_key"]}
                            rules={[{ required: true, message: <Label text={this.props.t("COMMON:NOT_EMPTY")} size="smaller" /> }]}
                          >
                            <Input
                              allowClear
                              value={state.search_key ? state.search_key : null}
                              placeholder={this.props.t("COMMON:FIELD_SEARCH_KEY")}
                              disabled={permission.view ? false : !permission.view_own}
                            />
                          </Form.Item>
                          {fields.length > 1 && <MinusCircleOutlined onClick={() => remove(field.name)} />}
                        </Space>
                      ))}
                      <Form.Item>
                        <Space>
                          {initiateList && initiateList.valid_search_by.length > fields.length && (
                            <Button type="dashed" onClick={() => add()} icon={<PlusOutlined />}>
                              <span style={{ fontSize: "small" }}>{this.props.t("COMMON:ADD_FILTER")}</span>
                            </Button>
                          )}
                          <Button htmlType="submit" type="primary" disabled={permission.view ? false : !permission.view_own}>
                            {this.props.t("COMMON:BUTTON_SEARCH")}
                          </Button>

                          <Button onClick={() => this.props.function.onReset()} disabled={permission.view ? false : !permission.view_own}>
                            {this.props.t("COMMON:BUTTON_RESET")}
                          </Button>
                        </Space>
                      </Form.Item>
                    </Fragment>
                  )}
                </Form.List>
              </Form>
            </Col>

            <Col md={24} lg={{ span: 7, offset: 5 }}>
              <Form layout="vertical" colon={false}>
                <Row gutter={8}>
                  <Col>
                    <Form.Item label={<Label text={this.props.t("COMMON:FIELD_ORDER_BY")} size="small" weight="medium" />}>
                      <Select
                        placeholder="-"
                        value={state.order_by}
                        style={{ textTransform: "capitalize", width: 128 }}
                        disabled={permission.view ? false : !permission.view_own}
                        onChange={(value) => this.props.function.onOrder(value, state.order_params)}
                      >
                        {this.props.valid_order_by &&
                          this.props.valid_order_by.map((result, i) => (
                            <Select.Option value={result} key={i} style={{ textTransform: "capitalize" }}>
                              {result ? result.replace(/_/g, " ") : result}
                            </Select.Option>
                          ))}
                      </Select>
                    </Form.Item>
                  </Col>

                  <Col>
                    <Form.Item style={{ marginTop: 30, marginBottom: 0 }}>
                      <Radio.Group
                        size="small"
                        optionType="button"
                        disabled={permission.view ? false : !permission.view_own}
                        value={state.order_params}
                        onChange={(event) => {
                          this.props.function.onOrder(state.order_by, event.target.value);
                        }}
                      >
                        <Tooltip title={this.props.t("COMMON:ASC")}>
                          <Radio.Button value="asc">
                            <CaretUpOutlined />
                          </Radio.Button>
                        </Tooltip>

                        <Tooltip title={this.props.t("COMMON:DESC")}>
                          <Radio.Button value="desc">
                            <CaretDownOutlined />
                          </Radio.Button>
                        </Tooltip>
                      </Radio.Group>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        )}
      </Card>
    );
  }
}

export default withTranslation()(FilterAudit);
