import { withTranslation } from "react-i18next";
import { Button, Card, Col, Form, Input, Radio, Row, Select, Skeleton, Tooltip } from "antd";
import React, { Component } from "react";
import Label from "../Label/Label";
import { CaretDownOutlined, CaretUpOutlined } from "@ant-design/icons";

class FilterContactPerson extends Component {
  state = {};

  componentDidUpdate() {
    if (!this.props.state.search_by) {
      this.props.initiateList && this.props.function.onChange("search_by", this.props.initiateList.valid_search_by[0]);
    }
    if (!this.props.state.operator) {
      this.props.initiateList && this.props.function.onChange("operator", this.props.initiateList.valid_operator[this.props.initiateList.valid_search_by[0]].operator[0]);
    }
    if (!this.props.state.order_by) {
      this.props.initiateList && this.props.function.onChange("order_by", this.props.initiateList.valid_order_by[0]);
    }
  }

  render() {
    const { state, permission, initiateList } = this.props;

    return (
      <Card size="small">
        {this.props.table_loading ? (
          <Skeleton paragraph={{ rows: 2, width: "100%" }} title={false} active />
        ) : (
          <Form layout="vertical" colon={false}>
            <Row gutter="8">
              <Col xs={{ span: 12 }} sm={{ span: 8 }} md={{ span: 8 }} lg={{ span: 4 }}>
                <Form.Item label={<Label text={this.props.t("COMMON:FIELD_CONNECTOR")} size="small" weight="medium" />} style={{ margin: 0 }}>
                  <Select
                    placeholder="-"
                    disabled={permission.view ? false : !permission.view_own}
                    value={state.connector}
                    style={{ textTransform: "capitalize" }}
                    onChange={(value) => {
                      this.props.function.onChange("connector", value);
                    }}
                  >
                    <Select.Option style={{ textTransform: "capitalize" }} value="group_of_distributor">
                      {this.props.t("COMMON:OPTION_DISTRIBUTOR")}
                    </Select.Option>
                    <Select.Option style={{ textTransform: "capitalize" }} value="principal">
                      {this.props.t("COMMON:OPTION_PRINCIPAL")}
                    </Select.Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={{ span: 12 }} sm={{ span: 8 }} md={{ span: 8 }} lg={{ span: 4 }}>
                <Form.Item label={<Label text={this.props.t("COMMON:FIELD_SEARCH_BY")} size="small" weight="medium" />} style={{ margin: 0 }}>
                  <Select
                    placeholder="-"
                    disabled={permission.view ? false : !permission.view_own}
                    value={state.search_by}
                    style={{ textTransform: "capitalize" }}
                    onChange={(value) => {
                      this.props.function.onChange("search_by", value);
                      this.props.function.onChange("operator", initiateList.valid_operator[value].operator[0]);
                    }}
                  >
                    {initiateList ? (
                      initiateList.valid_search_by.length > 1 ? (
                        initiateList.valid_search_by.map((result, i) => (
                          <Select.Option style={{ textTransform: "capitalize" }} key={i} value={result}>
                            {result && result.replace(/_/g, " ")}
                          </Select.Option>
                        ))
                      ) : (
                        <Select.Option style={{ textTransform: "capitalize" }} value={initiateList.valid_search_by[0]}>
                          {initiateList.valid_search_by[0].replace(/_/g, " ")}
                        </Select.Option>
                      )
                    ) : null}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={{ span: 12 }} sm={{ span: 8 }} md={{ span: 8 }} lg={{ span: 4 }}>
                <Form.Item style={{ margin: 0 }} label={<Label text={this.props.t("COMMON:FIELD_SEARCH_KEY")} size="small" weight="medium" />}>
                  {state.search_by && initiateList.valid_operator[state.search_by].data_type === "enum" ? (
                    <Select
                      disabled={permission.view ? false : !permission.view_own}
                      onChange={(value) => this.props.function.onChange("search_key", value)}
                      style={{ textTransform: "capitalize" }}
                      value={initiateList.enum_data[state.search_by].includes(state.search_key) ? state.search_key : null}
                    >
                      {state.search_by && initiateList
                        ? initiateList.enum_data[state.search_by].map((result, i) => (
                            <Select.Option key={i} value={result} style={{ textTransform: "capitalize" }}>
                              {result.replace(/_/g, " ")}
                            </Select.Option>
                          ))
                        : null}
                    </Select>
                  ) : (
                    <Input
                      allowClear
                      disabled={permission.view ? false : !permission.view_own}
                      value={state.search_key ? state.search_key : null}
                      onChange={(event) => this.props.function.onChange("search_key", event.target.value)}
                    />
                  )}
                </Form.Item>
              </Col>

              <Col xs={{ span: 12 }} sm={{ span: 8 }} md={{ span: 8 }} lg={{ span: 6 }}>
                <Form.Item style={{ marginTop: 30, marginBottom: 0 }}>
                  <Row gutter="4">
                    <Col>
                      <Button type={"primary"} onClick={() => this.props.function.onFilter()} disabled={permission.view ? false : !permission.view_own}>
                        {this.props.t("COMMON:BUTTON_SEARCH")}
                      </Button>
                    </Col>
                    <Col>
                      <Button onClick={() => this.props.function.onReset()} disabled={permission.view ? false : !permission.view_own}>
                        {this.props.t("COMMON:BUTTON_RESET")}
                      </Button>
                    </Col>
                  </Row>
                </Form.Item>
              </Col>

              <Col xs={{ span: 8 }} sm={{ span: 8 }} md={{ span: 8 }} lg={{ span: 4 }}>
                <Form.Item label={<Label text={this.props.t("COMMON:FIELD_ORDER_BY")} size="small" weight="medium" />} style={{ margin: 0 }}>
                  <Select
                    placeholder="-"
                    disabled={permission.view ? false : !permission.view_own}
                    value={state.order_by}
                    style={{ textTransform: "capitalize" }}
                    onChange={(value) => {
                      this.props.function.onOrder(value, state.order_params);
                    }}
                  >
                    {initiateList ? (
                      initiateList.valid_order_by.length > 1 ? (
                        initiateList.valid_order_by.map((result, i) => (
                          <Select.Option value={result} key={i} style={{ textTransform: "capitalize" }}>
                            {result ? result.replace(/_/g, " ") : result}
                          </Select.Option>
                        ))
                      ) : (
                        <Select.Option style={{ textTransform: "capitalize" }} value={initiateList.valid_order_by[0]}>
                          {initiateList.valid_order_by[0].replace(/_/g, " ")}
                        </Select.Option>
                      )
                    ) : null}
                  </Select>
                </Form.Item>
              </Col>

              <Col xs={{ span: 4 }} sm={{ span: 8 }} md={{ span: 8 }} lg={{ span: 2 }}>
                <Form.Item style={{ marginTop: 30, marginBottom: 0 }}>
                  <Radio.Group
                    size="small"
                    optionType="button"
                    disabled={permission.view ? false : !permission.view_own}
                    value={state.order_params}
                    onChange={(event) => {
                      this.props.function.onOrder(state.order_by, event.target.value);
                    }}
                  >
                    <Tooltip title={this.props.t("COMMON:ASC")}>
                      <Radio.Button value="asc">
                        <CaretUpOutlined />
                      </Radio.Button>
                    </Tooltip>

                    <Tooltip title={this.props.t("COMMON:DESC")}>
                      <Radio.Button value="desc">
                        <CaretDownOutlined />
                      </Radio.Button>
                    </Tooltip>
                  </Radio.Group>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        )}
      </Card>
    );
  }
}

export default withTranslation()(FilterContactPerson);
