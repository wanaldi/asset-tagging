import React from 'react';
import { List, Spin, Form, Input } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';
import { nexchiefaccount, mappingnexseller, mappingprincipal } from '../../../Enum/PropertiesConstant';
import { prepareComponent } from '../../../Action/CommonAction';
import { getInitiate, getList } from '../../../Service/Util';
import style from "./Filter.module.css";
import SearchLogo from '../../Icon/SearchLogo';
import ClearSearchLogo from '../../Icon/ClearSearchLogo';

class Filter extends React.Component {
  state = {
    data: [],
    loading: false,
    hasMore: true,
    ...nexchiefaccount,
    ...mappingnexseller,
    ...mappingprincipal,
    showSuggestion: false,
    query: '',
  };

  formRef = React.createRef();

  async componentDidMount() {
    this.setState({
      data: this.props.state.list
    });

    prepareComponent(this.props.dispatch, true);

    setTimeout(() => prepareComponent(this.props.dispatch, false), 256);
  }

  _initiateNexchiefAccount = async (page, limit, filter) => {

    console.log("filter123", filter);
    const { token, refresh_token, dispatch } = this.props;

    const initiateNexchiefAccount = await getInitiate(token, refresh_token, "admin/nexchiefaccount", filter, dispatch);

    const resList = await getList(token, refresh_token, "admin/nexchiefaccount/search", page, limit, null, filter, dispatch);
    if (resList == null) {
      this.setState({
        initiateNexchiefAccount : null,
        dataNexchiefAccount: [],
        data:[],
      });
      this.setState({ loading: false, showSuggestion: false });
    } else {
      this.setState({
        initiateNexchiefAccount,
        dataNexchiefAccount: resList,
        data: resList,
      });

      this.setState({ loading: false, showSuggestion: true });
    }
    
  };

  _initiateMappingNexseller = async (page, limit, filter) => {
    const { token, refresh_token, dispatch } = this.props;

    const initiateMappingNexseller = await getInitiate(token, refresh_token, "admin/mappingnexseller", filter, dispatch);

    const resList = await getList(token, refresh_token, "admin/mappingnexseller/search", page, limit, null, filter, dispatch);
    if (resList == null) {
      this.setState({
        initiateMappingNexseller: null,
        dataMappingNexseller: [],
        data:[],
      });
      this.setState({ loading: false, showSuggestion: false });
    } else {
      this.setState({
        initiateMappingNexseller,
        dataMappingNexseller: resList,
        data: resList,
      });

      this.setState({ loading: false, showSuggestion: true });
    }

  };

  _initiateMappingPrincipal = async (page, limit, filter) => {
    const { token, refresh_token, dispatch } = this.props;

    const initiateMappingPrincipal = await getInitiate(token, refresh_token, "admin/mappingprincipal", filter, dispatch);

    const resList = await getList(token, refresh_token, "admin/mappingprincipal/search", page, limit, null, filter, dispatch);
    if (resList == null) {
      this.setState({
        initiateMappingPrincipal: null,
        dataMappingPrincipal: [],
        data:[],
      });
      this.setState({ loading: false, showSuggestion: false });
    } else {
      this.setState({
        initiateMappingPrincipal,
        dataMappingPrincipal: resList,
        data: resList,
      });

      this.setState({ loading: false, showSuggestion: true });
    }

  };

  handleInfiniteOnLoad = async () => {
    switch (this.props.namespace) {
      case "nexchiefaccount":
        let { dataNexchiefAccount } = this.state;
        if (dataNexchiefAccount) {
          this.setState({
            loading: true,
          });

          if (dataNexchiefAccount.length === 0) break;

          if (dataNexchiefAccount.length === this.state.initiateNexchiefAccount.count_data) {
            this.setState({
              hasMore: false,
              loading: false,
            });
            return;
          }

          await this.setState({ pageNexchiefAccount: this.state.pageNexchiefAccount + 1 });
          const { token, refresh_token, dispatch } = this.props;
          const nextData = await getList(token, refresh_token, "admin/nexchiefaccount/search", this.state.pageNexchiefAccount, this.state.limitNexchiefAccount, null, this.state.filterNexchiefAccount, dispatch);

          dataNexchiefAccount = dataNexchiefAccount.concat(nextData);

          this.setState({
            dataNexchiefAccount,
            loading: false,
            data: dataNexchiefAccount,
          });
        }
        
        break;
      
      case "mappingnexseller":
        let { dataMappingNexseller } = this.state;
        if (dataMappingNexseller) {
          this.setState({
            loading: true,
          });

          if (dataMappingNexseller.length === 0) break;

          if (dataMappingNexseller.length === this.state.initiateMappingNexseller.count_data) {
            this.setState({
              hasMore: false,
              loading: false,
            });
            return;
          }

          await this.setState({ pageMappingNexseller: this.state.pageMappingNexseller + 1 });
          const { token, refresh_token, dispatch } = this.props;
          const nextData = await getList(token, refresh_token, "admin/mappingnexseller/search", this.state.pageMappingNexseller, this.state.limitMappingNexseller, null, this.state.filterMappingNexseller, dispatch);

          dataMappingNexseller = dataMappingNexseller.concat(nextData);

          this.setState({
            dataMappingNexseller,
            loading: false,
            data: dataMappingNexseller,
          });
        }

        break;
      
      case "mappingprincipal":
        let { dataMappingPrincipal } = this.state;
        if (dataMappingPrincipal) {
          this.setState({
            loading: true,
          });

          if (dataMappingPrincipal.length === 0) break;

          if (dataMappingPrincipal.length === this.state.initiateMappingPrincipal.count_data) {
            this.setState({
              hasMore: false,
              loading: false,
            });
            return;
          }

          await this.setState({ pageMappingPrincipal: this.state.pageMappingPrincipal + 1 });
          const { token, refresh_token, dispatch } = this.props;
          const nextData = await getList(token, refresh_token, "admin/mappingprincipal/search", this.state.pageMappingPrincipal, this.state.limitMappingPrincipal, null, this.state.filterMappingPrincipal, dispatch);

          dataMappingPrincipal = dataMappingPrincipal.concat(nextData);

          this.setState({
            dataMappingPrincipal,
            loading: false,
            data: dataMappingPrincipal,
          });
        }

        break;

      default:
        break;
    }


  };

  handleInputChange = async (searchValue) => {
    await this.setState({query: searchValue});
      if (this.state.query && this.state.query.length > 1) {
        if (this.state.query.length > 2) {
          switch (this.props.namespace) {
            case "nexchiefaccount":
              await this.setState({ filterNexchiefAccount: this.state.query });
              await this._initiateNexchiefAccount(this.state.pageNexchiefAccount, this.state.limitNexchiefAccount, "name like " + this.state.filterNexchiefAccount + ", code like " + this.state.filterNexchiefAccount);
              break;
            
            case "mappingnexseller":
              await this.setState({ filterMappingNexseller: this.state.query });
              await this._initiateMappingNexseller(this.state.pageMappingNexseller, this.state.limitMappingNexseller, "name like " + this.state.filterMappingNexseller + ", code like " + this.state.filterMappingNexseller);
              break;
            
            case "mappingprincipal":
              await this.setState({ filterMappingPrincipal: this.state.query });
              await this._initiateMappingPrincipal(this.state.pageMappingPrincipal, this.state.limitMappingPrincipal, "name like " + this.state.filterMappingPrincipal + ", code like " + this.state.filterMappingPrincipal);
              break;
          
            default:
              break;
          }
          
        }
      } else if (!this.state.query) {
        this.setState({ showSuggestion: false });
      }
  }

  _onClear = () => {
    this.formRef.current.setFieldsValue({ search: '' });
    this.setState({ showSuggestion: false });
    this.props.function.onReset();
  }

  render() {
    return (
      <div style={{marginRight: "0px", marginLeft:"auto"}}>
        <Form style={{ width: "312px", height: "40px", display: "block" }} ref={this.formRef}>
          <Form.Item name="search">
            <Input
              className={style.InputSearch}
              placeholder={this.props.placeholder}
              onChange={(event) => this.handleInputChange(event.target.value)}
              prefix={<SearchLogo />}
              suffix={<ClearSearchLogo onClear={()=>this._onClear()} />}
            />
          </Form.Item>
        </Form>

        {this.state.showSuggestion &&
          <div className={this.state.data.length === 1 ? style.Suggestion : style.SuggestionMoreThanOne}>
            <InfiniteScroll
              initialLoad={false}
              pageStart={0}
              loadMore={this.handleInfiniteOnLoad}
              hasMore={!this.state.loading && this.state.hasMore}
              useWindow={false}
            >
              <List
                dataSource={this.state.data}
                renderItem={item => (
                  <List.Item key={item.id} className={style.List} onClick={this.props.changeFilter(item.company_profile_name)}>
                    <div>{item.value}</div>
                  </List.Item>
                )}
              >
                {this.state.loading && this.state.hasMore && (
                  <div style={{
                    position: "absolute",
                    bottom: "40px",
                    width: "100%",
                    textAlign: "center"
                  }}>
                    <Spin />
                  </div>
                )}
              </List>
            </InfiniteScroll>
          </div>
        }
      </div>
    );
  }
}

export default Filter