import { ReloadOutlined } from "@ant-design/icons";
import { Button, Result } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { restoreWithThunk } from "../../Util/ErorrHandler";

class ErrorBoundary extends Component {
  state = { error: null, errorInfo: null };

  componentDidCatch(error, errorInfo) {
    this.setState({ error: error, errorInfo: errorInfo });
    this.props.restoreWithThunk();
  }

  render() {
    if (this.state.error) {
      return (
        <Result
          status="error"
          title={this.props.t("COMMON:CRASH")}
          style={{ position: "relative", top: 160 }}
          extra={[
            <Button key="console" onClick={() => window.location.reload()} icon={<ReloadOutlined />}>
              {this.props.t("COMMON:RELOAD")}
            </Button>,
          ]}
        />
      );
    }

    return this.props.children;
  }
}

export default connect(null, { restoreWithThunk })(withTranslation()(ErrorBoundary));
