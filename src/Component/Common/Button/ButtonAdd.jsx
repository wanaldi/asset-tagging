import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import style from './ButtonAdd.module.css';

class ButtonAdd extends Component {
  state = {};
  render() {
    return (
      <div style={{display:"contents", padding: 0, margin: 0}}>
        {
          this.props.router !== "" ?
            <Link to={this.props.router} style={{ marginLeft: "10px", marginRight: "0px" }}>
              <Button onClick={this.props.openModal} className={style.ButtonAdd}  disabled={this.props.disabled}>{this.props.t("COMMON:BUTTON_ADD")} {this.props.text}</Button>
            </Link>
            :
            <Button onClick={this.props.openModal} className={style.ButtonAdd} style={{ marginLeft: "10px", marginRight: "0px" }} disabled={this.props.disabled}>{this.props.t("COMMON:BUTTON_ADD")} {this.props.text}</Button>
        }
      </div>
      
    );
  }
}

export default withTranslation()(ButtonAdd);
