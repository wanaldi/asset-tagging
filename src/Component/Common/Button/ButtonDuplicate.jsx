import { CopyOutlined } from "@ant-design/icons";
import { Button, Skeleton } from "antd";
import React from "react";
import { withTranslation } from "react-i18next";

class ButtonDuplicate extends React.Component {
  render() {
    return this.props.detail ? <Button disabled={this.props.disabled} onClick={this.props.onClick} icon={<CopyOutlined />} /> : <Skeleton.Button size="small" shape="round" />;
  }
}

export default withTranslation()(ButtonDuplicate);
