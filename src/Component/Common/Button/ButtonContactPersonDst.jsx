import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import style from './ButtonContactPerson.module.css';

class ButtonContactPersonDst extends Component {
    render() {
        return (
            <Button htmlType="submit" className={this.props.index===this.props.indexActive ? style.ButtonContactPerson1 : style.ButtonContactPersonDst} onClick={this.props.onChangeActive}>
                {this.props.t("COMMON:BUTTON_CONTACT_PERSON")} {this.props.index}
            </Button>
        );
    }
}

export default withTranslation()(ButtonContactPersonDst);
