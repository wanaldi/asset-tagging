import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import style from './ButtonSubmit.module.css';

class ButtonSubmit extends Component {
  render() {
    return (
      <Button type="primary" htmlType="submit" className={style.ButtonSubmit}>
        {this.props.t("COMMON:BUTTON_SAVE")}
      </Button>
    );
  }
}

export default withTranslation()(ButtonSubmit);
