import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import Left from '../../../Asset/Sidebar-Menu-Left.svg';
import Right from '../../../Asset/Sidebar-Menu-Right.svg';


class ButtonCollapsed extends Component {
    render() {
        return (
            <div onClick={this.props.onCollapse} style={{ float: "right", marginRight: "-20px", zIndex: 99, position: "relative", cursor: "pointer", marginBottom:"5vh"}}>
                {this.props.collapsed ?
                    <img src={Right} />
                    :
                    <img src={Left} />
                }
            </div>
        );
    }
}

export default withTranslation()(ButtonCollapsed);
