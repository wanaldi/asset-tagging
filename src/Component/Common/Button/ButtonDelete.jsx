import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import style from './ButtonDelete.module.css';

class ButtonDelete extends React.Component {
  render() {
    // return this.props.detail ? (
    //   <Popconfirm
    //     placement="bottomRight"
    //     onConfirm={this.props.onConfirm}
    //     title={this.props.t("COMMON:CONFIRM_DELETE")}
    //     okText={this.props.t("COMMON:OK")}
    //     cancelText={this.props.t("COMMON:NO")}
    //   >
    //     <Button danger icon={<DeleteOutlined />} disabled={this.props.disabled} />
    //   </Popconfirm>
    // ) : (
    //   <Skeleton.Button size={"small"} shape={"round"} />
    // );

    return (
        <Button className={style.ButtonDelete} onClick={this.props.openModal} style={{ marginLeft: "auto", marginRight: "0px" }} disabled={this.props.disabled}>{this.props.t("COMMON:BUTTON_DELETE")} {this.props.text}</Button>
    );
  }
}

export default withTranslation()(ButtonDelete);
