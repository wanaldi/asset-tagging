import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import style from './ButtonContactPerson.module.css';

class ButtonAddContactPerson extends Component {
    render() {
        return (
            <Button htmlType="submit" className={style.ButtonAddContactPerson} onClick={this.props.onAddContact}>
                + {this.props.t("COMMON:BUTTON_CONTACT_PERSON")}
            </Button>
        );
    }
}

export default withTranslation()(ButtonAddContactPerson);
