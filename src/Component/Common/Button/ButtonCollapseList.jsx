import React, { Component } from "react";
import Up from '../../../Asset/Chevron-Up.svg';
import Down from '../../../Asset/Chevron-Down.svg';


class ButtonCollapseList extends Component {
    render() {
        return (
            <div onClick={this.props.onCollapse} style={{ cursor: "pointer", marginLeft:"auto", marginRight:"0px"}}>
                {this.props.collapsed ?
                    <img src={Down} />
                    :
                    <img src={Up} />
                }
            </div>
        );
    }
}

export default ButtonCollapseList;
