import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import style from './ButtonContactPerson.module.css';

class ButtonContactPerson1 extends Component {
    render() {
        return (
            <Button className={style.ButtonContactPerson1}>
                {this.props.t("COMMON:BUTTON_CONTACT_PERSON")} 1
            </Button>
        );
    }
}

export default withTranslation()(ButtonContactPerson1);
