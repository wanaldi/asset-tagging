import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import style from './ButtonCancel.module.css';

class ButtonBack extends Component {
    render() {
        return (
            <Button className={style.ButtonCancel}
                // onClick={() => window.history.back()}
            >
                {this.props.t("COMMON:BUTTON_BACK")}
            </Button>
        );
    }
}

export default withTranslation()(ButtonBack);
