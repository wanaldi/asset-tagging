import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import style from './ButtonCheck.module.css';

class ButtonCheck extends Component {
    render() {
        return (
            <Button className={style.ButtonCheck} onClick={this.props.onCheck}>
                Check
            </Button>
        );
    }
}

export default withTranslation()(ButtonCheck);
