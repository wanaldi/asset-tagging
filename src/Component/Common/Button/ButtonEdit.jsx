// import { EditOutlined } from "@ant-design/icons";
// import { Button, Skeleton } from "antd";
// import React from "react";
// import { withTranslation } from "react-i18next";
// import { Link } from "react-router-dom";

import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import style from './ButtonEdit.module.css';

class ButtonEdit extends Component {
  render() {
    // return this.props.detail ? (
    //   <Link to={this.props.router}>
    //     <Button disabled={this.props.disabled} icon={<EditOutlined />} />
    //   </Link>
    // ) : (
    //   <Skeleton.Button size={"small"} shape={"round"} />
    // );

    return this.props.detail && (
      <Link to={{ pathname: this.props.router, state: { detail: this.props.detail }  }} style={{ marginLeft: "10px", marginRight: "59px" }} >
          <Button className={style.ButtonEdit} disabled={this.props.disabled} detail={this.props.detail}>{this.props.t("COMMON:BUTTON_EDIT")} {this.props.text}</Button>
        </Link>
      );
  }
}

export default withTranslation()(ButtonEdit);
