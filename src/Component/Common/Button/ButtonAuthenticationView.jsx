import { PaperClipOutlined } from "@ant-design/icons";
import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";

class ButtonAuthView extends Component {
  render() {
    return <Button type="primary" size="small" icon={<PaperClipOutlined />} onClick={this.props.openDrawer} style={{margin : "2px" }} />;
  }
}

export default withTranslation()(ButtonAuthView);
