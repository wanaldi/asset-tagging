import { Radio } from "antd";
import React, { Component } from "react";
import Label from "../Label/Label";

class RadioButton extends Component {
  render() {
    return (
      <Radio.Button value={this.props.value}>
        <Label text={this.props.text} size="small" />
      </Radio.Button>
    );
  }
}

export default RadioButton;
