import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import style from './ButtonNext.module.css';

class ButtonNext extends Component {
    render() {
        return (
            <Button type="primary" htmlType="submit" className={style.ButtonNext}>
                {this.props.t("COMMON:BUTTON_NEXT")}
            </Button>
        );
    }
}

export default withTranslation()(ButtonNext);
