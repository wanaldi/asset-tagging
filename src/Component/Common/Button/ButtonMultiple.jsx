import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { Badge, Button, Popconfirm, Space } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";

class ButtonMultiple extends Component {
  state = {};
  render() {
    const { state } = this.props;

    return state.multiple_delete ? (
      <Space style={{ margin: "0 32px" }}>
        <Button icon={<CloseOutlined />} onClick={this.props.disableMultiple} />

        <Popconfirm
          placement="bottomRight"
          title={this.props.t("COMMON:CONFIRM_DELETE_SELECTED")}
          onConfirm={this.props.multipleDelete}
          okText={this.props.t("COMMON:OK")}
          cancelText={this.props.t("COMMON:NO")}
        >
          <Badge count={state.selectedRowKeys.length}>
            <Button icon={<CheckOutlined />} disabled={state.selectedRows.length < 1} />
          </Badge>
        </Popconfirm>
      </Space>
    ) : (
      <Button disabled={this.props.disabled} onClick={this.props.enableMultiple}>
        {this.props.t("COMMON:BUTTON_MULTIPLE_DELETE")}
      </Button>
    );
  }
}

export default withTranslation()(ButtonMultiple);
