import { EyeFilled } from "@ant-design/icons";
import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";

class ButtonView extends Component {
  render() {
    return <Button type="primary" size="small" icon={<EyeFilled />} onClick={this.props.openDrawer} style={{margin : "2px" }} />;
  }
}

export default withTranslation()(ButtonView);
