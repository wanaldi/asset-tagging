import React, { Component } from "react";
import { Radio } from "antd";
import { withTranslation } from "react-i18next";
import Label from "../Label/Label";

class ButtonLanguage extends Component {
  render() {
    return (
      <Radio.Group value={this.props.language} onChange={(event) => this.props.changeLanguage(event.target.value)}>
        <Radio.Button value="en-US">
          <Label text="EN" size="small" weight="bold" />
        </Radio.Button>
        <Radio.Button value="id-ID">
          <Label text="ID" size="small" weight="bold" />
        </Radio.Button>
      </Radio.Group>
    );
  }
}

export default withTranslation()(ButtonLanguage);
