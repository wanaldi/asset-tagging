import { Button } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import style from './ButtonCancel.module.css';

class ButtonCancel extends Component {
  render() {
    return (
      <Button className={style.ButtonCancel}
        // onClick={() => window.history.back()}
      >
        <Link to={ this.props.router }>
          {this.props.t("COMMON:BUTTON_CANCEL")}
        </Link>
        
      </Button>
    );
  }
}

export default withTranslation()(ButtonCancel);
