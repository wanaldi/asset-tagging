import { Input } from "antd";
import React, { forwardRef } from "react";
import ReactInputMask from "react-input-mask";

const InputMask = forwardRef((props, ref) => {
  return <ReactInputMask {...props}>{(inputProps) => <Input {...inputProps} ref={ref} disabled={props.disabled ? props.disabled : null} />}</ReactInputMask>;
});

export default InputMask;
