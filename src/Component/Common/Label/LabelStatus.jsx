import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import Label from "./Label";

class LabelStatus extends Component {
  render() {
    return this.props.status === "A" ? (
      <Label text={this.props.t("COMMON:ACTIVE")} size="small" showIcon typeIcon="success" />
    ) : (
      <Label text={this.props.t("COMMON:NOT_ACTIVE")} size="small" showIcon typeIcon="error" />
    );
  }
}

export default withTranslation()(LabelStatus);
