import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import Label from "./Label";

class LabelAction extends Component {
  render() {
    if (this.props.action === 1) {
      return <Label text={this.props.t("COMMON:ACTION_INSERT")} size="small" />;
    } else if (this.props.action === 2) {
      return <Label text={this.props.t("COMMON:ACTION_UPDATE")} size="small" />;
    } else {
      return <Label text={this.props.t("COMMON:ACTION_DELETE")} size="small" />;
    }
  }
}

export default withTranslation()(LabelAction);
