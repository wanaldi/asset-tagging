import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import Label from "./Label";

class LabelAvailable extends Component {
  render() {
    return this.props.is === "Y" ? (
      <Label text={this.props.t("COMMON:OK")} size="small" showIcon typeIcon="success" />
    ) : (
      <Label text={this.props.t("COMMON:NO")} size="small" showIcon typeIcon="error" />
    );
  }
}

export default withTranslation()(LabelAvailable);
