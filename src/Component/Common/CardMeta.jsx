import { Card } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";

class CardMeta extends Component {
  render() {
    return (
      <Card size={"small"} style={{ backgroundColor: "#F5F6FF", ...this.props.style }} bordered={false}>
        <Card.Meta
          title={<span style={{ fontSize: "small", fontWeight: 384 }}>{this.props.title}</span>}
          description={this.props.description ? <span style={{ fontWeight: 512, color: "#000000" }}>{this.props.description}</span> : "-"}
        />
      </Card>
    );
  }
}

export default withTranslation()(CardMeta);
