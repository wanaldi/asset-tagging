import { Card, Empty, Table } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";

class ListTable extends Component {
  render() {
    const styleTable = {
      marginTop: this.props.permission.view ? 0 : this.props.permission.view_own ? 0 : 32,
    };

    const empty = {
      emptyText: (
        <Empty
          description={
            this.props.permission.view
              ? this.props.table_loading
                ? this.props.t("COMMON:LOADING")
                : this.props.t("COMMON:DATA_NOT_FOUND")
              : this.props.permission.view_own
              ? this.props.table_loading
                ? this.props.t("COMMON:LOADING")
                : this.props.t("COMMON:DATA_NOT_FOUND")
              : this.props.t("COMMON:NOT_AUTHORIZE")
          }
        />
      ),
    };

    return (
      <Card size={"small"}>
        <Table
          locale={empty}
          bordered={true}
          style={styleTable}
          columns={this.props.columns}
          loading={this.props.table_loading}
          size={this.props.size ? this.props.size : "small"}
          dataSource={this.props.dataSource ? this.props.dataSource : null}
          pagination={this.props.pagination ? this.props.pagination : false}
          rowSelection={this.props.rowSelection}
          scroll={{ x: true }}
        />
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    table_loading: state.common.table_loading,
  };
};

export default connect(mapStateToProps, null)(withTranslation()(ListTable));
