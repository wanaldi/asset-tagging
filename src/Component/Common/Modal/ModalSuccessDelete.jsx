import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import style from './ModalSuccessDelete.module.css';
import { Modal, Button } from 'antd';

class ModalSuccessDelete extends Component {
    render() {

        return (
            <Modal
                visible={this.props.modalSuccessDeleteOpen}
                closable={false}
                centered={true}
                width="20vw"
                footer={null}
            >

                <div className={style.ModalTitle}>{this.props.t("COMMON:SUCCESS")}</div>
                {/* <div className={style.ModalSubTitle}>{this.props.response}</div> */}
                <div className={style.ModalSubTitle}>Your data was saved<br/>successfully!</div>

                <div style={{ justifyItems: "center", margin: 0, padding: 0 }}>
                    <Link to={this.props.router}>
                        <Button className={style.ButtonOK} >
                            {this.props.t("COMMON:BUTTON_OK")}
                        </Button>
                    </Link>
                </div>

            </Modal>
        );
    }
}

export default withTranslation()(ModalSuccessDelete);
