import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import style from './ModalDelete.module.css';
import { Modal, Button } from 'antd';

class ModalDelete extends Component {
    render() {

        return (
            <Modal
                visible={this.props.modalOpen}
                closable={false}
                centered={true}
                width="20vw"
                footer={null}
            >

                <div className={style.ModalTitle}>{this.props.t("COMMON:ACTION_DELETE")}</div>
                <div className={style.ModalSubTitle}>{this.props.t("COMMON:CONFIRM_DELETE")} {this.props.menu}?</div>

                <div style={{ justifyItems: "center", margin: 0, padding: 0 }}>
                    {/* <Link to={this.props.router}> */}
                        <Button className={style.ButtonDelete} onClick={this.props.onDelete}>
                            {this.props.t("COMMON:OK_DELETE")} {this.props.okDelete}
                        </Button>
                    {/* </Link> */}
                    <Button className={style.ButtonCancel} onClick={this.props.closeModal}>
                        {this.props.t("COMMON:BUTTON_CANCEL")}
                    </Button>
                </div>

            </Modal>
        );
    }
}

export default withTranslation()(ModalDelete);
