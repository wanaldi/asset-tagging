import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import style from './ModalNPWP.module.css';
import { Modal, Button, Input, Form } from 'antd';
import { rulesMappingNexseller } from "../../../Util/FormRules";
import { connect } from "react-redux";
import { stateRedux } from "../../../Util/ReduxState";
import * as CompanyProfileService from "../../../Service/Admin/CompanyProfileService";
import { propTable } from "../../../Enum/PropertiesConstant";

class ModalNPWP extends Component {
    state = {
        ...propTable,
        npwpFound: false,
        company_profile: null,
    }

    _onCheck = async () => {
        var npwpValue = document.getElementById("npwp").value;

        const data2 = {
            page: this.state.page,
            limit: this.state.limit,
            order_by: this.state.order_by,
            npwp: npwpValue,
        }

        if (npwpValue === '') {
            this.setState({ npwpFound: false });
        } else {
            this._getData(data2);
        }

    };

    _getData = async (value) => {
        const { token, refresh_token, dispatch } = this.props;

        if (this.props.permission.view || this.props.permission.view_own) {
            const response = await CompanyProfileService.doGetList(token, refresh_token, value, dispatch);
            console.log("res2", response);
            if (response) {
                this.setState({ npwpFound: true });
                this.setState({ company_profile: response[0] });
            } else {
                this.setState({ npwpFound: false });
            }

        }
    };

    _onFinish = (values) => {
        console.log("masuk");
        const title = values.npwp;
        const content = values.content;
        console.log(title, content);
    }

    render() {

        const rules = rulesMappingNexseller(this.props.t);

        return (
            <Modal
                visible={this.props.modalOpen}
                closable={false}
                centered={true}
                width="40vw"
                footer={null}
            >

                <div className={style.ModalTitle}>{this.props.t("COMMON:BUTTON_ADD")} {this.props.title}</div>
                <div className={style.ModalSubTitle}>Ensure the code you input is correct</div>

                <Form onFinish={this._onFinish}>
                    
                    <div style={{ display: "grid", gridTemplateColumns: "80% 20%" }}>
                        <div>
                            <label className={style.LabelForm}>{this.props.labelNPWP}</label>
                            <Form.Item name="npwp" rules={rules.npwp} className={style.FormItem}>
                                <Input className={style.Input} id="npwp" onChange={()=>this.setState({npwpFound: false})}/>
                            </Form.Item>
                        </div>
                        
                        <Form.Item className={style.FormItem} style={{paddingTop: "8px"}}>
                            <Button htmlType="submit" className={style.ButtonCheck} style={{ marginLeft: "10px" }} onClick={this._onCheck}>
                                {this.props.t("COMMON:BUTTON_CHECK")}
                            </Button>
                        </Form.Item>
                    </div>
                    
                </Form>

                {this.state.npwpFound ?
                    <div className={style.ModalSubTitle}>Your NPWP Code found and can be used</div> : 
                    <div className={style.ModalSubTitle}>Your NPWP Code not found and cannot be used</div>}

                <div style={{ display:"flex", margin: 0, padding: 0 }}>
                    <Button className={style.ButtonCancel} onClick={this.props.closeModal}>
                        {this.props.t("COMMON:BUTTON_CANCEL")}
                    </Button>
                    
                    <Link to={{ pathname: this.props.router, state: { company_profile: this.state.company_profile } }}
                        style={{ marginRight: 0, marginLeft: "10px" }}>
                        <Button className={style.ButtonNext} disabled={!this.state.npwpFound}>
                            {this.props.t("COMMON:BUTTON_NEXT")}
                        </Button>
                    </Link>
                    
                </div>

            </Modal>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ...stateRedux(state, "mapping_nexseller"),
    };

};

const mapDispatchToProps = (dispatch) => {
    return { dispatch };
};

export default connect(mapStateToProps, mapDispatchToProps)(withTranslation()(ModalNPWP));
