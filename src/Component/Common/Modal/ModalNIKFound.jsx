import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import style from './ModalSuccessDelete.module.css';
import { Modal, Button } from 'antd';

class ModalNIKFound extends Component {
    render() {

        return (
            <Modal
                visible={this.props.modalSuccessSaveOpen}
                closable={false}
                centered={true}
                width="20vw"
                footer={null}
            >

                <div className={style.ModalTitle} style={{textAlign: "left"}}>NIK</div>
                {/* <div className={style.ModalSubTitle}>{this.props.response}</div> */}
                {this.props.nikFound ? 
                    <div>
                        <div>{this.props.t("COMMON:NIK_FOUND")}</div>
                        <div>Nama NIK</div>
                    </div>
                    :
                    <div>
                        <div>{this.props.t("COMMON:NIK_NOT_FOUND_1")}</div>
                        <div>{this.props.t("COMMON:NIK_NOT_FOUND_2")}</div>
                    </div>
                }

                <div style={{ display: "flex", margin: 0, padding: 0 }}>
                    <Button className={style.ButtonCancel} onClick={this.props.closeModal}>
                        {this.props.t("COMMON:BUTTON_CANCEL")}
                    </Button>

                    {this.props.nikFound ? 
                        // <Link to={{ pathname: this.props.router, state: { company_profile: this.state.company_profile } }}
                        //     style={{ marginRight: 0, marginLeft: "10px" }}>
                            <Button className={style.ButtonNext}>
                                {this.props.t("COMMON:BUTTON_NEXT")}
                            </Button>
                        // </Link>
                        :
                        <Button className={style.ButtonNext} onClick={this.props.closeModal}>
                            {this.props.t("COMMON:BUTTON_OK")}
                        </Button>
                    }

                </div>

            </Modal>
        );
    }
}

export default withTranslation()(ModalNIKFound);
