import React, { Component } from "react";
import { connect } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { setLanguage } from "../Action/CommonAction";
import { API_PREFIX } from "../Enum/ApiConstant";
import { DEFAULT_LANGUAGE } from "../Enum/PropertiesConstant";
import { ROUTER_CONSTANT } from "../Enum/RouterConstant";
import { PrivateRoute } from "../Util/PrivateRoute";
import App from "./App/App";
import ErrorBoundary from "./Common/ErrorBoundary";
// import ActivationUser from "./Page/Admin/User/ActivationUser";
// import Forbidden from "./Page/Error/Forbidden";
import Login from "./Page/Login/Login";
import ForgetPassword from "./Page/Password/ForgetPassword";
import ResetPassword from "./Page/Password/ResetPassword";

class Nexchief extends Component {
  constructor(props) {
    super(props);
    props.setLanguage(this.props.language ? this.props.language : DEFAULT_LANGUAGE);
  }

  render() {
    return (
      <BrowserRouter basename={`/${API_PREFIX}`}>
        <Switch>
          <Route path={ROUTER_CONSTANT.INDEX} component={Login} exact />
          <Route path={ROUTER_CONSTANT.LOGIN} component={Login} exact />
          {/* <Route path={ROUTER_CONSTANT.FORBIDDEN} component={Forbidden} exact /> */}
          <Route path={ROUTER_CONSTANT.FORGET_PASSWORD} component={ForgetPassword} exact />
          <Route path={ROUTER_CONSTANT.RESET_PASSWORD} component={ResetPassword} exact />
          {/* <Route path={ROUTER_CONSTANT.USER.VERIFY} component={ActivationUser} exact /> */}

          <ErrorBoundary {...this.props}>
            <PrivateRoute component={App} />
          </ErrorBoundary>
        </Switch>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.authentication.language,
  };
};

export default connect(mapStateToProps, { setLanguage })(Nexchief);
