import { Layout } from "antd";
import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { Switch } from "react-router-dom";
import { checkPermission } from "../../Action/PermissionAction";
import { ROUTER_CONSTANT } from "../../Enum/RouterConstant";
import { ROUTE_ITEM } from "../../Router/Router";
import { PrivateRoute } from "../../Util/PrivateRoute";
import Headmenu from "../Layout/Headmenu/Headmenu";
import Sidemenu from "../Layout/Sidemenu/Sidemenu";
import Home from "../Page/Home/Home";
import style from "./App.module.css";
import { Link } from 'react-router-dom';

class App extends Component {
  constructor(props) {
    super(props);
    this.props.checkPermission();
  }

  state = {
    prepareComponent: true,
    collapsed: false,
  };

  componentDidMount() {
    this.setState({ prepareComponent: false });
  }

  _onCollapse = () => {
    this.setState({ collapsed: !this.state.collapsed });
  }

  render() {

    const SubMenu = () => (
      <div className={style.HeaderMenu} style={{ marginLeft: this.state.collapsed ? "70px" : "220px" }}>
        {window.location.pathname.includes(ROUTER_CONSTANT.CLIENT_USER.LIST) &&
          <div style={{ display: "flex" }}>
          <div style={{ marginRight: "40px" }}>Client User</div>
            <div>Nexsoft User</div>
          </div>
        }

        {window.location.pathname.includes(ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.LIST) && !window.location.pathname.includes(ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.DETAIL) && !window.location.pathname.includes(ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.EDIT) &&
          <div>
            <div>{this.props.t("NEXCHIEF_ACCOUNT:TITLE_HEADER")}</div>
          </div>
        }

        {
          ((window.location.pathname.includes(ROUTER_CONSTANT.MAPPING_NEXSELLER.LIST) &&
          !window.location.pathname.includes(ROUTER_CONSTANT.MAPPING_NEXSELLER.DETAIL) &&
          !window.location.pathname.includes(ROUTER_CONSTANT.MAPPING_NEXSELLER.EDIT)) ||
          (window.location.pathname.includes(ROUTER_CONSTANT.MAPPING_PRINCIPAL.LIST) &&
          !window.location.pathname.includes(ROUTER_CONSTANT.MAPPING_PRINCIPAL.DETAIL) &&
          !window.location.pathname.includes(ROUTER_CONSTANT.MAPPING_PRINCIPAL.EDIT))) &&
          <div style={{ display: "flex" }}>
          <div style={{ marginRight: "40px" }}>
            <Link to={ROUTER_CONSTANT.MAPPING_NEXSELLER.LIST} className={window.location.pathname.includes(ROUTER_CONSTANT.MAPPING_NEXSELLER.LIST) ? style.LinkActive : style.Link}>
              Nexseller
            </Link>
          </div>
          <div>
            <Link to={ROUTER_CONSTANT.MAPPING_PRINCIPAL.LIST} className={window.location.pathname.includes(ROUTER_CONSTANT.MAPPING_PRINCIPAL.LIST) ? style.LinkActive : style.Link}>
              Principal
            </Link>
          </div>
          </div>
        }

        {window.location.pathname.includes(ROUTER_CONSTANT.DATA_GROUP.LIST) && !window.location.pathname.includes(ROUTER_CONSTANT.DATA_GROUP.DETAIL) && !window.location.pathname.includes(ROUTER_CONSTANT.DATA_GROUP.EDIT) &&
          <div>
            <div>{this.props.t("NEXCHIEF_ACCOUNT:TITLE")}</div>
          </div>
        }

      </div>
    );

    return (
      <Layout style={{ minHeight: '100vh' }}>
        <Sidemenu width={this.props.collapsed ? "70px" : "220px"} collapsed={this.state.collapsed} onCollapse={this._onCollapse} />
        <Layout className="site-layout">
          <Headmenu />
          <SubMenu />
            <Layout.Content className={style.Content} style={{ overflowX: "auto", position:"relative", transition:"0.5s", marginLeft: this.state.collapsed ? "70px" : "220px"}}>
            <Switch>
              <PrivateRoute path={ROUTER_CONSTANT.HOME} component={Home} />
              
                {ROUTE_ITEM.map(({ ROUTER }) => {
                  return ROUTER.map(({ PATH, COMPONENT, EXACT }) => <PrivateRoute path={PATH} component={COMPONENT} exact={EXACT} />);
                })}
              </Switch>
            </Layout.Content>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.authentication.isAuthenticated,
    language: state.authentication.language,
    auth_loading: state.common.auth_loading,
  };
};

export default connect(mapStateToProps, { checkPermission })(withTranslation()(App));
