import * as Permission from "../Enum/PermissionConstant";
import { concatMenuPermission, validatePermission } from "../Service/Util";

export const generatePermission = (menu, permission) => {
  let initial_permission = {
    view: false,
    view_own: false,
    insert: false,
    insert_own: false,
    update: false,
    update_own: false,
    delete: false,
    delete_own: false,
    duplicate: false,
    duplicate_own: false,
    changepassword: false,
    changepassword_own: false,
    synchronize: false,
    synchronize_own: false,
  };

  if (menu && permission) {
    initial_permission.view = validatePermission(concatMenuPermission(menu, Permission.VIEW), permission);
    initial_permission.view_own = validatePermission(concatMenuPermission(menu, Permission.VIEW_OWN), permission);
    initial_permission.insert = validatePermission(concatMenuPermission(menu, Permission.INSERT), permission);
    initial_permission.insert_own = validatePermission(concatMenuPermission(menu, Permission.INSERT_OWN), permission);
    initial_permission.update = validatePermission(concatMenuPermission(menu, Permission.UPDATE), permission);
    initial_permission.update_own = validatePermission(concatMenuPermission(menu, Permission.UPDATE_OWN), permission);
    initial_permission.delete = validatePermission(concatMenuPermission(menu, Permission.DELETE), permission);
    initial_permission.delete_own = validatePermission(concatMenuPermission(menu, Permission.DELETE_OWN), permission);
    initial_permission.duplicate = validatePermission(concatMenuPermission(menu, Permission.DUPLICATE), permission);
    initial_permission.duplicate_own = validatePermission(concatMenuPermission(menu, Permission.DUPLICATE_OWN), permission);
    initial_permission.changepassword = validatePermission(concatMenuPermission(menu, Permission.CHANGE_PASSWORD), permission);
    initial_permission.changepassword_own = validatePermission(concatMenuPermission(menu, Permission.CHANGE_PASSWORD_OWN), permission);
    initial_permission.synchronize = validatePermission(concatMenuPermission(menu, Permission.SYNCHRONIZE), permission);
    initial_permission.synchronize_own = validatePermission(concatMenuPermission(menu, Permission.SYNCHRONIZE_OWN), permission);
  }

  return initial_permission;
};
