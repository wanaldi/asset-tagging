import _ from "lodash";
import moment from "moment";
import { doRefreshToken, validateExpired } from "../Action/CommonAction";
import { data, initiate, initiateFromMasterData, list, listFromMasterData, menu } from "../Api/Initiate";
import { errorFetching } from "../Util/ErorrHandler";

export const getInitiate = async (prevToken, refreshToken, namespace, filter, dispatch) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  try {
    const response = await initiate(token, namespace, filter);
    if (response) return response.data.nexsoft.payload.data.content;
  } catch (error) {
    errorFetching(dispatch, error);
  }
};

export const getInitiateFromMasterData = async (prevToken, refreshToken, namespace, filter, dispatch) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  try {
    const response = await initiateFromMasterData(token, namespace, filter);
    if (response) return response.data.nexsoft.payload.data.content;
  } catch (error) {
    errorFetching(dispatch, error);
  }
};

export const getData = async (prevToken, refreshToken, namespace, page, limit, order, filter, dispatch) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  try {
    const response = await data(token, namespace, page, limit, order, filter);
    if (response) return response.data.nexsoft.payload.data.content;
  } catch (error) {
    errorFetching(dispatch, error);
  }
};

export const getList = async (prevToken, refreshToken, namespace, page, limit, order, filter, dispatch) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  try {
    const response = await list(token, namespace, page, limit, order, filter);
    if (response) return response.data.nexsoft.payload.data.content;
  } catch (error) {
    errorFetching(dispatch, error);
  }
};

export const getListFromMasterData = async (prevToken, refreshToken, namespace, page, limit, order, filter, dispatch) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  const data = {
    page: page,
    limit: limit,
    order: order,
    name: filter,
  }

  try {
    const response = await listFromMasterData(token, namespace, data);
    if (response) return response.data.nexsoft.payload.data.content;
  } catch (error) {
    errorFetching(dispatch, error);
  }
};

export const initiateMenu = async (prevToken, refreshToken, namespace, id, dispatch) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  try {
    const response = await menu(token, namespace, id);
    if (response) return response.data.nexsoft.payload.data.content;
  } catch (error) {
    errorFetching(dispatch, error);
  }
};

export const checkResource = (arrResource, constResource) => {
  let isValid;

  for (let i = 0; i < arrResource.length; i++) {
    isValid = arrResource[i] === constResource;
  }

  return isValid;
};

export const concatMenuPermission = (menu, permission) => {
  return `${menu}:${permission}`;
};

export const validatePermission = (mustHavePermission, listPermission) => {
  let globalValid = false;
  let permissionValid = false;

  const splitMustHavePermission = mustHavePermission.split(":");
  let menu = splitMustHavePermission[0];
  let permission = splitMustHavePermission[1];

  if (permission.split("-").length > 1) {
    if (listPermission[permission.split("-")[0]]) {
      globalValid = permissionChecker(permission.split("-")[0], listPermission[permission.split("-")[0]]);
    }
  } else {
    if (listPermission[permission]) {
      globalValid = permissionChecker(permission, listPermission[permission]);
    }
  }

  if (!globalValid) {
    return permissionValid;
  } else {
    let splitMenu = menu.split(".");
    let len = splitMenu.length;

    for (let i = 0; i < len; len--) {
      menu = "";
      for (let j = 0; j < len; j++) {
        menu += splitMenu[j];
        if (j < len - 1) menu += ".";
      }

      if (listPermission[menu]) {
        permissionValid = permissionChecker(permission, listPermission[menu]);
        if (permissionValid) break;
      }
    }

    return permissionValid;
  }
};

export const permissionChecker = (needPermission, arrPermission) => {
  for (let i = 0; i < arrPermission.length; i++) {
    if (needPermission === arrPermission[i]) return true;
  }

  return false;
};

export const availableMenu = (menu, permission) => {
  const arrMenu = [];

  console.log("menu", menu);
  console.log("permission", permission);

  for (const key in permission) {
    console.log("key", key);
    console.log("permission", permission[key][0]);
    if (key !== permission[key][0]) {
      arrMenu.push(key);
    }
  }

  console.log("arrmenu", arrMenu);

  if (arrMenu) {
    console.log("h1");
    menu.forEach((menuItem) => {
      console.log("h2", menuItem);
      // parentMenu.service_menu.forEach((subMenu) => {
      //   console.log("h3");
      //   const menuItemArray = [];

      //   subMenu.menu_item.forEach((menuItem) => {
      //     if (menuChecker(menuItem.menu_code, arrMenu)) menuItemArray.push(menuItem);
      //   });

      //   subMenu.menu_item = _.uniqBy(menuItemArray, "menu_code");
      // });
      menuItem.menu_code = _.uniqBy(menuItem, "menu_code");
    });
    // menu.forEach((parentMenu) => {
    //   console.log("h4");
    //   parentMenu.service_menu = parentMenu.service_menu.filter((subMenu) => subMenu.menu_item.length !== 0);
    // });

    return menu.filter((menuItem) => menuItem.length !== 0);
  }
};

export const menuChecker = (menuCode, arrMenu) => {
  let validMenu = false;

  let splitMenu = menuCode.split(".");
  let len = splitMenu.length;

  for (let i = 0; i < len; len--) {
    menuCode = "";
    for (let j = 0; j < len; j++) {
      menuCode += splitMenu[j];
      if (j < len - 1) menuCode += ".";
    }

    if (arrMenu.includes(menuCode)) {
      validMenu = true;
      if (validMenu) break;
    }
  }

  return validMenu;
};

export const mappingRequestMultipleDelete = (data, properties) => {
  const arrData = [];

  for (let i = 0; i < data.length; i++) {
    arrData.push({ [properties]: data[i].key, updated_at: data[i].updated_at });
  }

  return arrData;
};

export const generatePrefix = (initiate) => {
  return `${initiate.valid_search_by[0]} ${initiate.valid_operator[initiate.valid_search_by[0]].operator[0]}`;
};

export const concatPrefix = (prefix, value) => {
  return `${prefix} ${value}`;
};

export const generateOrder = (initiate) => {
  return `${initiate.valid_order_by[1]} asc`;
};

export const formatDate = (date) => {
  return moment.utc(date).format("DD/MM/YYYY");
};

export const formatDateTime = (date) => {
  return moment.utc(date).format("DD/MM/YYYY HH:mm:ss");
};

export const formatISO = (date) => {
  return new Date(date.format("YYYY-MM-DD HH:mm:ss")).toISOString();
};

export const validateCheckedData = (data, itemChecked, properties) => {
  data.forEach((arr) => {
    arr.available_action.forEach((subArr) => {
      itemChecked.forEach((newArr) => {
        if (subArr.key === newArr[properties]) {
          subArr.is_checked = newArr.is_checked;
        }
      });
    });
  });

  return data;
};

export const mappingRolePermission = (data) => {
  data.forEach((item) => {
    let permission = [];
    const splitter = item.available_action.split(",");

    for (let i = 0; i < splitter.length; i++) {
      if (!item.menu_code || item.menu_code === "") {
        permission.push({
          label: splitter[i].trim().split(":")[0],
          key: splitter[i].trim(),
          is_checked: false,
        });
      } else {
        permission.push({
          label: splitter[i].trim(),
          key: `${item.menu_code}:${splitter[i].trim()}`,
          is_checked: false,
        });
      }
    }

    if (!item.menu_code) item.menu_code = "global";
    item.available_action = permission;
  });

  return data;
};

export const checkedData = (data, properties) => {
  let checked = [];

  data.forEach((arr) => {
    arr.available_action.forEach((subArr) => {
      checked.push({
        [properties]: subArr.key,
        is_checked: subArr.is_checked,
      });
    });
  });

  return checked;
};

export const mappingCheckedRequest = (data, namespace) => {
  const arr = [];

  for (let index = 0; index < data.length; index++) {
    if (data[index].is_checked === true) {
      arr.push(data[index][namespace]);
    }
  }

  return arr;
};

export const mappingOwnData = (data, properties) => {
  let ownData = [];

  data.forEach((arr) => {
    arr[properties].forEach((subArr) => {
      ownData.push({
        [properties]: subArr.value,
        is_checked: subArr.is_checked,
      });
    });
  });

  return ownData;
};
