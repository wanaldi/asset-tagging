import { message } from "antd";
import i18next from "i18next";
import * as ActionType from "../../Action/ActionTypes";
import { doRefreshToken, validateExpired } from "../../Action/CommonAction";
import * as ContactPersonAPI from "../../Api/Admin/ContactPersonAPI.js";
import { errorFetching } from "../../Util/ErorrHandler";

export const doGetList = async (prevToken, refreshToken, data, dispatch) => {
    let response = [];

    let token = prevToken;
    if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);
    // dispatch({ type: ActionType.TABLE_REST_FETCHING });

    console.log("data", data);

    // const formData = new FormData();
    // formData.append("data", JSON.stringify(data));

    
    // console.log("formData", formData);

    if (token)
        try {
            // const response_initiate = await ContactPersonAPI.initiateList(token, filter);
            const response_list = await ContactPersonAPI.list(token, data);

            // console.log("response list", response_list);

            // if (response_initiate && response_list) {
            if (response_list) {
                console.log("response list2", response_list);
                // response.initiate_list = response_initiate.data.nexsoft.payload.data.content;
                response = response_list.data.nexsoft.payload.data.content;
                // dispatch({ type: ActionType.TABLE_REST_SUCCESS });
            }
        } catch (error) {
            errorFetching(dispatch, error);
        }
    
    console.log("response123", response);

    return response;
};

export const doGetDetail = async (prevToken, refreshToken, id, dispatch) => {
    let response = { detail: null };

    let token = prevToken;
    if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);
    dispatch({ type: ActionType.SKELETON_REST_FETCHING });

    if (token)
        try {
            const response_detail = await ContactPersonAPI.detail(token, id);
            if (response_detail) {
                response.detail = response_detail.data.nexsoft.payload.data.content;
                dispatch({ type: ActionType.SKELETON_REST_SUCCESS });
            }
        } catch (error) {
            errorFetching(dispatch, error);
        }

    return response;
};