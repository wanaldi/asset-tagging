import { message } from "antd";
import i18next from "i18next";
import * as ActionType from "../../Action/ActionTypes";
import { doRefreshToken, validateExpired } from "../../Action/CommonAction";
import * as UserAPI from "../../Api/Admin/UserAPI";
import { errorFetching } from "../../Util/ErorrHandler";

export const doGetList = async (prevToken, refreshToken, page, limit, order, filter, dispatch) => {
  let response = { initiate_list: undefined, list: [] };

  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);
  dispatch({ type: ActionType.TABLE_REST_FETCHING });

  if (token)
    try {
      const response_initiate = await UserAPI.initiateList(token, filter);
      const response_list = await UserAPI.list(token, page, limit, order, filter);

      if (response_initiate && response_list) {
        response.initiate_list = response_initiate.data.nexsoft.payload.data.content;
        response.list = response_list.data.nexsoft.payload.data.content;
        dispatch({ type: ActionType.TABLE_REST_SUCCESS });
      }
    } catch (error) {
      errorFetching(dispatch, error);
    }

  return response;
};

export const doGetDetail = async (prevToken, refreshToken, id, dispatch) => {
  let response = { detail: null };

  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);
  dispatch({ type: ActionType.SKELETON_REST_FETCHING });

  if (token)
    try {
      const response_detail = await UserAPI.detail(token, id);
      if (response_detail) {
        response.detail = response_detail.data.nexsoft.payload.data.content;
        dispatch({ type: ActionType.SKELETON_REST_SUCCESS });
      }
    } catch (error) {
      errorFetching(dispatch, error);
    }

  return response;
};

export const getInitiateInsert = async (prevToken, refreshToken, dispatch) => {
  let response = { detail: null };

  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);
  dispatch({ type: ActionType.SKELETON_REST_FETCHING });

  if (token)
    try {
      const response_detail = await UserAPI.initiateInsert(token);
      if (response_detail) {
        response = response_detail.data.nexsoft.payload.data.content;
        dispatch({ type: ActionType.SKELETON_REST_SUCCESS });
      }
    } catch (error) {
      errorFetching(dispatch, error);
    }
  return response;
};

export const doInsert = async (prevToken, refreshToken, data, dispatch, redirect) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  if (token) {
    const hide = message.loading(i18next.t("COMMON:LOADING"), 0);
    try {
      const response = await UserAPI.insert(token, data);
      if (response) {
        message.success({ content: response.data.nexsoft.payload.status.message });
        setTimeout(hide, 0);

        setTimeout(() => redirect(), 64);
      }
    } catch (error) {
      errorFetching(dispatch, error);
      setTimeout(hide, 0);
    }
  }
};

export const doUpdate = async (prevToken, refreshToken, id, data, dispatch, redirect) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  if (token) {
    const hide = message.loading(i18next.t("COMMON:LOADING"), 0);
    try {
      const response = await UserAPI.update(token, id, data);
      if (response) {
        message.success({ content: response.data.nexsoft.payload.status.message });
        setTimeout(hide, 0);
        setTimeout(() => redirect(), 64);
      }
    } catch (error) {
      errorFetching(dispatch, error);
      setTimeout(hide, 0);
    }
  }
};

export const doDelete = async (prevToken, refreshToken, id, data, dispatch, redirect) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  if (token) {
    const hide = message.loading(i18next.t("COMMON:LOADING"), 0);

    try {
      const response = await UserAPI.deleted(token, id, data);
      if (response) {
        message.success({ content: response.data.nexsoft.payload.status.message });
        setTimeout(hide, 0);
        setTimeout(() => redirect(), 64);
      }
    } catch (error) {
      errorFetching(dispatch, error);
      setTimeout(hide, 0);
    }
  }
};

export const doMultipleDelete = async (prevToken, refreshToken, data, dispatch, reset) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  if (token) {
    const hide = message.loading(i18next.t("COMMON:LOADING"), 0);

    try {
      const response = await UserAPI.multipleDelete(token, data);
      if (response) {
        setTimeout(() => reset(), 64);
        setTimeout(hide, 0);
        message.success({ content: response.data.nexsoft.payload.status.message });
      }
    } catch (error) {
      setTimeout(hide, 0);
      errorFetching(dispatch, error);
    }
  }
};

export const doActivation = async (data, namespace, dispatch) => {
  let success = false;

  const hide = message.loading(i18next.t("COMMON:LOADING"), 0);
  try {
    const response = await UserAPI.Activation(data, namespace);
    if (response) {
      success = true;
      message.success({ content: response.data.nexsoft.payload.status.message });
      setTimeout(hide, 0);
    }
  } catch (error) {
    errorFetching(dispatch, error);
    setTimeout(hide, 0);
  }

  return success;
};

export const doResendActivation = async (data, namespace, dispatch) => {
  let success = false;

  const hide = message.loading(i18next.t("COMMON:LOADING"), 0);
  try {
    const response = await UserAPI.resendActivation(data, namespace);
    if (response) {
      success = true;
      message.success({ content: response.data.nexsoft.payload.status.message });
      setTimeout(hide, 0);
    }
  } catch (error) {
    errorFetching(dispatch, error);
    setTimeout(hide, 0);
  }

  return success;
};

export const doChangeOwnPassword = async (prevToken, refreshToken, data, dispatch, redirect) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  if (token) {
    const hide = message.loading(i18next.t("COMMON:LOADING"), 0);
    try {
      const response = await UserAPI.changeOwnPassword(token, data);
      if (response) {
        message.success({ content: response.data.nexsoft.payload.status.message });
        setTimeout(hide, 0);

        setTimeout(() => redirect(), 64);
      }
    } catch (error) {
      errorFetching(dispatch, error);
      setTimeout(hide, 0);
    }
  }
};

export const doForgetPassword = async (type_token, namespace, token, data, dispatch, redirect) => {
  if (token) {
    const hide = message.loading(i18next.t("COMMON:LOADING"), 0);
    try {
      const response = await UserAPI.forgetPassword(type_token, namespace, token, data);
      if (response) {
        if (response.data.nexsoft.payload.data.content.email_status.email_notify) {
          if (!response.data.nexsoft.payload.data.content.email_status.email_notify_status) {
            message.error({ content: response.data.nexsoft.payload.data.content.email_status.email_notify_message });
          } else {
            message.success({ content: response.data.nexsoft.payload.status.message });
            setTimeout(() => redirect(), 64);
          }
        }

        // if (response.data.nexsoft.payload.data.content.phone_status.phone_notify) {
        //   if (!response.data.nexsoft.payload.data.content.phone_status.phone_notify_status) {
        //     message.error({ content: response.data.nexsoft.payload.data.content.phone_status.phone_notify_message });
        //   } else {
        //     message.success({ content: response.data.nexsoft.payload.status.message });
        //     setTimeout(() => redirect(), 64);
        //   }
        // }

        message.success({ content: response.data.nexsoft.payload.status.message });
        setTimeout(hide, 0);

        setTimeout(() => redirect(), 64);
      }
    } catch (error) {
      errorFetching(dispatch, error);
      setTimeout(hide, 0);
    }
  }
};

export const doResetPassword = async (type_token, namespace, token, data, dispatch) => {
  let isSuccess = false;

  if (token) {
    const hide = message.loading(i18next.t("COMMON:LOADING"), 0);
    try {
      const response = await UserAPI.resetPassword(type_token, namespace, token, data);
      if (response) {
        message.success({ content: response.data.nexsoft.payload.status.message });
        setTimeout(hide, 0);

        isSuccess = true;
      }
    } catch (error) {
      errorFetching(dispatch, error);
      setTimeout(hide, 0);
    }
  }

  return isSuccess;
};

export const doGetAuthDetail = async (prevToken, refreshToken, id, dispatch) => {
  let response = { detail: null };

  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);
  dispatch({ type: ActionType.SKELETON_REST_FETCHING });

  if (token)
    try {
      const response_detail = await UserAPI.authDetail(token, id);
      if (response_detail) {
        response.detail = response_detail.data.nexsoft.payload.data.content;
        dispatch({ type: ActionType.SKELETON_REST_SUCCESS });
      }
    } catch (error) {
      errorFetching(dispatch, error);
    }

  return response;
};

export const doUpdateAuth = async (prevToken, refreshToken, id, data, dispatch, redirect) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  if (token) {
    const hide = message.loading(i18next.t("COMMON:LOADING"), 0);
    try {
      const response = await UserAPI.updateAuth(token, id, data);
      if (response) {
        message.success({ content: response.data.nexsoft.payload.status.message });
        setTimeout(hide, 0);
        setTimeout(() => redirect(), 64);
      }
    } catch (error) {
      errorFetching(dispatch, error);
      setTimeout(hide, 0);
    }
  }
};

export const doAddResource = async (prevToken, refreshToken, data, dispatch, redirect) => {
  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

  if (token) {
    const hide = message.loading(i18next.t("COMMON:LOADING"), 0);
    try {
      const response = await UserAPI.addResource(token, data);
      if (response) {
        message.success({ content: response.data.nexsoft.payload.status.message });
        setTimeout(hide, 0);

        setTimeout(() => redirect(), 64);
      }
    } catch (error) {
      errorFetching(dispatch, error);
      setTimeout(hide, 0);
    }
  }
};

export const doCheckUser = async (prevToken, refreshToken, id, dispatch) => {
  let response = { detail: null };

  let token = prevToken;
  if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);
  dispatch({ type: ActionType.SKELETON_REST_FETCHING });

  if (token)
    try {
      const response_detail = await UserAPI.CheckUser(token, id);
      if (response_detail) {
        response.detail = response_detail.data.nexsoft.payload.data.content;
        dispatch({ type: ActionType.SKELETON_REST_SUCCESS });
      }
    } catch (error) {
      // if (error.response.status === 502 || error.response.status === 500) {
        errorFetching(dispatch, error);
      // }
    }
  return response;
};
