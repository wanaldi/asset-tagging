import { message } from "antd";
import i18next from "i18next";
import * as ActionType from "../../Action/ActionTypes";
import { doRefreshToken, validateExpired } from "../../Action/CommonAction";
import * as CompanyProfileAPI from "../../Api/Admin/CompanyProfileAPI.js";
import { errorFetching } from "../../Util/ErorrHandler";

export const doGetList = async (prevToken, refreshToken, data, dispatch) => {
    let response = null;

    let token = prevToken;
    if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

    if (token)
        try {
            const response_list = await CompanyProfileAPI.list(token, data);

            if (response_list) {
                response = response_list.data.nexsoft.payload.data.content;
            }
        } catch (error) {
            errorFetching(dispatch, error);
        }
    
    return response;
};

export const doGetDetail = async (prevToken, refreshToken, id, dispatch) => {
    let response = { detail: null };

    let token = prevToken;
    if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);
    dispatch({ type: ActionType.SKELETON_REST_FETCHING });

    if (token)
        try {
            const response_detail = await CompanyProfileAPI.detail(token, id);
            if (response_detail) {
                response.detail = response_detail.data.nexsoft.payload.data.content;
                dispatch({ type: ActionType.SKELETON_REST_SUCCESS });
            }
        } catch (error) {
            errorFetching(dispatch, error);
        }

    return response;
};