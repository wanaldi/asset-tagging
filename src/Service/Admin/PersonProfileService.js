import { message } from "antd";
import i18next from "i18next";
import * as ActionType from "../../Action/ActionTypes";
import { doRefreshToken, validateExpired } from "../../Action/CommonAction";
import * as PersonProfileAPI from "../../Api/Admin/PersonProfileAPI.js";
import { errorFetching } from "../../Util/ErorrHandler";

export const doGetList = async (prevToken, refreshToken, data, dispatch) => {
    let response = null;

    let token = prevToken;
    if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

    console.log("data", data);

    if (token)
        try {
            const response_list = await PersonProfileAPI.list(token, data);

            if (response_list) {
                console.log("response list2", response_list);
                response = response_list.data.nexsoft.payload.data.content;
            }
        } catch (error) {
            errorFetching(dispatch, error);
        }
    
    return response;
};

export const doGetDetail = async (prevToken, refreshToken, id, dispatch) => {
    let response = { detail: null };

    let token = prevToken;
    if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);
    dispatch({ type: ActionType.SKELETON_REST_FETCHING });

    if (token)
        try {
            const response_detail = await PersonProfileAPI.detail(token, id);
            if (response_detail) {
                response.detail = response_detail.data.nexsoft.payload.data.content;
                dispatch({ type: ActionType.SKELETON_REST_SUCCESS });
            }
        } catch (error) {
            errorFetching(dispatch, error);
        }

    return response;
};