import { message } from "antd";
import i18next from "i18next";
import * as ActionType from "../../Action/ActionTypes";
import { doRefreshToken, validateExpired } from "../../Action/CommonAction";
import * as DataGroupAPI from "../../Api/Admin/DataGroupAPI";
import { errorFetching } from "../../Util/ErorrHandler";

export const doGetList = async (prevToken, refreshToken, page, limit, order, filter, dispatch) => {
    let response = { initiate_list: undefined, list: [] };

    let token = prevToken;
    if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);
    dispatch({ type: ActionType.TABLE_REST_FETCHING });

    console.log("getlist2");

    if (token)
        try {
            const response_initiate = await DataGroupAPI.initiateList(token, filter);
            const response_list = await DataGroupAPI.list(token, page, limit, order, filter);

            if (response_initiate && response_list) {
                response.initiate_list = response_initiate.data.nexsoft.payload.data.content;
                response.list = response_list.data.nexsoft.payload.data.content;
                dispatch({ type: ActionType.TABLE_REST_SUCCESS });
            }
        } catch (error) {
            errorFetching(dispatch, error);
        }

    return response;
};

export const doGetDetail = async (prevToken, refreshToken, id, dispatch) => {
    let response = { detail: null };

    let token = prevToken;
    if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);
    dispatch({ type: ActionType.SKELETON_REST_FETCHING });

    if (token)
        try {
            const response_detail = await DataGroupAPI.detail(token, id);
            if (response_detail) {
                response.detail = response_detail.data.nexsoft.payload.data.content;
                dispatch({ type: ActionType.SKELETON_REST_SUCCESS });
            }
        } catch (error) {
            errorFetching(dispatch, error);
        }

    return response;
};

export const doUpdate = async (prevToken, refreshToken, id, previousData, dispatch, redirect) => {
    // let token = prevToken;
    // if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

    // const data = {
    //     company_title_id: previousData.company_title_id,
    //     npwp: previousData.npwp,
    //     name: previousData.name,
    //     address_1: previousData.address_1,
    //     address_2: previousData.address_2,
    //     address_3: previousData.address_3,
    //     hamlet: previousData.hamlet,
    //     neighbourhood: previousData.neighbourhood,
    //     country_id: previousData.country_id,
    //     district_id: previousData.district_id,
    //     sub_district_id: previousData.sub_district_id,
    //     urban_village_id: previousData.urban_village_id,
    //     postal_code_id: previousData.postal_code_id,
    //     island_id: previousData.island_id,
    //     latitude: parseFloat(previousData.latitude),
    //     longitude: parseFloat(previousData.longitude),
    //     phone_country_code: previousData.phone_country_code && `+${previousData.phone_country_code}`,
    //     phone: previousData.phone_country_code && previousData.phone,
    //     phone_fax_country_code: previousData.phone_fax_country_code && `+${previousData.phone_fax_country_code}`,
    //     fax: previousData.phone_fax_country_code && previousData.fax,
    //     email: previousData.email,
    //     alternative_email: previousData.alternative_email,
    //     is_pkp: previousData.is_pkp.trim(),
    //     is_bumn: previousData.is_bumn.trim(),
    //     is_pbf: previousData.is_pbf.trim(),
    //     pbf_license_number: previousData.pbf_license_number,
    //     pbf_license_end_date: previousData.pbf_license_end_date && formatISO(previousData.pbf_license_end_date),
    //     pbf_trustee_sipa_number: previousData.pbf_trustee_sipa_number,
    //     pbf_sipa_license_end_date: previousData.pbf_sipa_license_end_date && formatISO(previousData.pbf_sipa_license_end_date),
    //     additional_info: previousData.additional_info,
    //     status: previousData.status,
    //     deleted_photo_id: previousData.deleted_photo_id,
    //     updated_at: previousData.updated_at,
    // };

    // const formData = new FormData();
    // formData.append("content", JSON.stringify(data));

    // if (previousData.logo) {
    //     for (let index = 0; index < previousData.logo.length; index++) {
    //         if (!previousData.logo[index].url) {
    //             formData.append(`photo${index + 1}`, previousData.logo[index].originFileObj);
    //         }
    //     }
    // }

    // if (token) {
    //     const hide = message.loading(i18next.t("COMMON:LOADING"), 0);
    //     try {
    //         const response = await DataGroupAPI.update(token, id, formData);
    //         if (response) {
    //             message.success({ content: response.data.nexsoft.payload.status.message });
    //             setTimeout(hide, 0);
    //             setTimeout(() => redirect(), 64);
    //         }
    //     } catch (error) {
    //         errorFetching(dispatch, error);
    //         setTimeout(hide, 0);
    //     }
    // }
};

export const doDelete = async (prevToken, refreshToken, id, data, dispatch) => {
    let res = undefined;
    let token = prevToken;
    if (validateExpired(token, dispatch)) token = await doRefreshToken(token, refreshToken, dispatch);

    if (token) {
        // const hide = message.loading(i18next.t("COMMON:LOADING"), 0);

        try {
            const response = await DataGroupAPI.deleted(token, id, data);
            if (response) {
                // message.success({ content: response.data.nexsoft.payload.status.message });
                res = response.data.nexsoft.payload.status.message;
                // setTimeout(hide, 0);
                // setTimeout(() => redirect(), 64);
            }
        } catch (error) {
            errorFetching(dispatch, error);
            // setTimeout(hide, 0);
        }
    }

    return res;
};