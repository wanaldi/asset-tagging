import "antd/dist/antd.css";
import React from "react";
import ReactDOM from "react-dom";
import { I18nextProvider } from "react-i18next";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import Nexchief from "./Component/Nexchief";
import i18next from "./Config/I18nConfig";
import storeApp from "./Config/StoreConfig";
import "./index.css";
import serviceWorker from "./serviceWorker";

ReactDOM.render(
  <Provider store={storeApp().store}>
    <PersistGate loading={null} persistor={storeApp().persist}>
      <React.Suspense fallback={<React.Fragment />}>
        <I18nextProvider i18n={i18next}>
          <Nexchief />
        </I18nextProvider>
      </React.Suspense>
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

// reportWebVitals(console.log);
serviceWorker();
