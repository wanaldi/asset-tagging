import Home from "../Component/Page/Home/Home";
import * as Menu from "../Enum/MenuConstant";
import { ROUTER_CONSTANT } from "../Enum/RouterConstant";

import ListNexchiefAccount from "../Component/Page/Admin/NexchiefAccount/ListNexchiefAccount";
import DetailNexchiefAccount from "../Component/Page/Admin/NexchiefAccount/DetailNexchiefAccount";
import EditNexchiefAccount from "../Component/Page/Admin/NexchiefAccount/EditNexchiefAccount";

import ListMappingNexseller from "../Component/Page/Admin/MappingNexseller/ListMappingNexseller";
import AddMappingNexseller from "../Component/Page/Admin/MappingNexseller/AddMappingNexseller";
import DetailMappingNexseller from "../Component/Page/Admin/MappingNexseller/DetailMappingNexseller";

import ListMappingPrincipal from "../Component/Page/Admin/MappingPrincipal/ListMappingPrincipal";
import AddMappingPrincipal from "../Component/Page/Admin/MappingPrincipal/AddMappingPrincipal";
import DetailMappingPrincipal from "../Component/Page/Admin/MappingPrincipal/DetailMappingPrincipal";

// import AddCompanyProfile from "../Component/Page/Admin/CompanyProfile/AddCompanyProfile";

// import AddContactPerson from "../Component/Page/Admin/ContactPerson/AddContactPerson";

// import AddPersonProfile from "../Component/Page/Admin/PersonProfile/AddPersonProfile";

// import ListRole from "../Component/Page/Admin/Role/ListRole";
// import DetailRole from "../Component/Page/Admin/Role/DetailRole";
// import AddRole from "../Component/Page/Admin/Role/AddRole";
// import EditRole from "../Component/Page/Admin/Role/EditRole";

// import ListNexsoftRole from "../Component/Page/Admin/NexsoftRole/ListNexsoftRole";
// import DetailNexsoftRole from "../Component/Page/Admin/NexsoftRole/DetailNexsoftRole";
// import AddNexsoftRole from "../Component/Page/Admin/NexsoftRole/AddNexsoftRole";
// import EditNexsoftRole from "../Component/Page/Admin/NexsoftRole/EditNexsoftRole";

import ListDataGroup from "../Component/Page/Admin/DataGroup/ListDataGroup";
// import DetailDataGroup from "../Component/Page/Admin/DataGroup/DetailDataGroup";
// import AddDataGroup from "../Component/Page/Admin/DataGroup/AddDataGroup";
// import EditDataGroup from "../Component/Page/Admin/DataGroup/EditDataGroup";

// import ListDataScope from "../Component/Page/Admin/DataScope/ListDataScope";
// import DetailDataScope from "../Component/Page/Admin/DataScope/DetailDataScope";
// import AddDataScope from "../Component/Page/Admin/DataScope/AddDataScope";
// import EditDataScope from "../Component/Page/Admin/DataScope/EditDataScope";

import ListClientUser from "../Component/Page/Admin/ClientUser/ListClientUser";
import AddClientUser from "../Component/Page/Admin/ClientUser/AddClientUser";
// import DetailUser from "../Component/Page/Admin/User/DetailUser";
// import EditUser from "../Component/Page/Admin/User/EditUser";
// import AddResource from "../Component/Page/Admin/User/AddResource";
// import CheckUser from "../Component/Page/Admin/User/CheckUser";
// import EditResource from "../Component/Page/Admin/User/EditResource";

// import ListNexsoftUser from "../Component/Page/Admin/NexsoftUser/ListNexsoftUser";

// import ListJobProcess from "../Component/Page/Admin/JobProcess/ListJobProcess";
// import DetailJobProcess from "../Component/Page/Admin/JobProcess/DetailJobProcess";

// import ListAudit from "../Component/Page/Admin/Audit/ListAudit";
// import DetailAudit from "../Component/Page/Admin/Audit/DetailAudit";

// import Password from "../Component/Page/Admin/User/Password";
// import OwnPassword from "../Component/Page/Admin/User/OwnPassword";

// import ListParameter from "../Component/Page/Admin/Parameter/ListParameter";
// import DetailParameter from "../Component/Page/Admin/Parameter/DetailParameter";


export const ROUTE_ITEM = [
  {
    MENU: Menu.HOME,
    ROUTER: [
      { PATH: ROUTER_CONSTANT.HOME, COMPONENT: Home, EXACT: false },
    ],
  },
  {
    MENU: Menu.CLIENT_USER,
    ROUTER: [
      { PATH: ROUTER_CONSTANT.CLIENT_USER.LIST, COMPONENT: ListClientUser, EXACT: true },
      // { PATH: ROUTER_CONSTANT.CLIENT_USER.DETAIL + `/:id`, COMPONENT: DetailUser, EXACT: false },
      { PATH: ROUTER_CONSTANT.CLIENT_USER.ADD, COMPONENT: AddClientUser, EXACT: false },
      // { PATH: ROUTER_CONSTANT.CLIENT_USER.EDIT + `/:id`, COMPONENT: EditUser, EXACT: false },
      // { PATH: ROUTER_CONSTANT.CLIENT_USER.PASSWORD, COMPONENT: Password, EXACT: false },
      // { PATH: ROUTER_CONSTANT.CLIENT_USER.OWN_PASSWORD, COMPONENT: OwnPassword, EXACT: false },
      // { PATH: ROUTER_CONSTANT.CLIENT_USER.ADD_RESOURCE, COMPONENT: AddResource, EXACT: false },
      // { PATH: ROUTER_CONSTANT.CLIENT_USER.CHECK_USER, COMPONENT: CheckUser, EXACT: false },
      // { PATH: ROUTER_CONSTANT.CLIENT_USER.EDIT_RESOURCE + `/:id`, COMPONENT: EditResource, EXACT: false },
    ],
  },
  {
    MENU: Menu.NEXCHIEF_ACCOUNT,
    ROUTER: [
      { PATH: ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.LIST, COMPONENT: ListNexchiefAccount, EXACT: true },
      { PATH: ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.DETAIL + `/:id`, COMPONENT: DetailNexchiefAccount, EXACT: false },
      { PATH: ROUTER_CONSTANT.NEXCHIEF_ACCOUNT.EDIT + `/:id`, COMPONENT: EditNexchiefAccount, EXACT: false },
    ],
  },
  {
    MENU: Menu.MAPPING_NEXSELLER,
    ROUTER: [
      { PATH: ROUTER_CONSTANT.MAPPING_NEXSELLER.LIST, COMPONENT: ListMappingNexseller, EXACT: true },
      { PATH: ROUTER_CONSTANT.MAPPING_NEXSELLER.ADD, COMPONENT: AddMappingNexseller, EXACT: false },
      { PATH: ROUTER_CONSTANT.MAPPING_NEXSELLER.DETAIL + `/:id`, COMPONENT: DetailMappingNexseller, EXACT: false },
    ],
  },
  {
    MENU: Menu.MAPPING_PRINCIPAL,
    ROUTER: [
      { PATH: ROUTER_CONSTANT.MAPPING_PRINCIPAL.LIST, COMPONENT: ListMappingPrincipal, EXACT: true },
      { PATH: ROUTER_CONSTANT.MAPPING_PRINCIPAL.ADD, COMPONENT: AddMappingPrincipal, EXACT: false },
      { PATH: ROUTER_CONSTANT.MAPPING_PRINCIPAL.DETAIL + `/:id`, COMPONENT: DetailMappingPrincipal, EXACT: false },
    ],
  },
  // {
    // MENU: Menu.NEXSOFT_USER,
    // ROUTER: [
    //   { PATH: ROUTER_CONSTANT.NEXSOFT_USER.LIST, COMPONENT: ListNexsoftUser, EXACT: true },
      // { PATH: ROUTER_CONSTANT.NEXSOFT_USER.DETAIL + `/:id`, COMPONENT: DetailUser, EXACT: false },
      // { PATH: ROUTER_CONSTANT.NEXSOFT_USER.ADD, COMPONENT: AddUser, EXACT: false },
      // { PATH: ROUTER_CONSTANT.NEXSOFT_USER.EDIT + `/:id`, COMPONENT: EditUser, EXACT: false },
      // { PATH: ROUTER_CONSTANT.NEXSOFT_USER.PASSWORD, COMPONENT: Password, EXACT: false },
      // { PATH: ROUTER_CONSTANT.NEXSOFT_USER.OWN_PASSWORD, COMPONENT: OwnPassword, EXACT: false },
      // { PATH: ROUTER_CONSTANT.NEXSOFT_USER.ADD_RESOURCE, COMPONENT: AddResource, EXACT: false },
      // { PATH: ROUTER_CONSTANT.NEXSOFT_USER.CHECK_USER, COMPONENT: CheckUser, EXACT: false },
      // { PATH: ROUTER_CONSTANT.NEXSOFT_USER.EDIT_RESOURCE + `/:id`, COMPONENT: EditResource, EXACT: false },
  //   ],
  // },
  // {
  //   MENU: Menu.COMPANY_PROFILE,
  //   ROUTER: [ 
  //     { PATH: ROUTER_CONSTANT.COMPANY_PROFILE.ADD, COMPONENT: AddCompanyProfile, EXACT: false },
  //   ],
  // },
  // {
  //   MENU: Menu.CONTACT_PERSON,
  //   ROUTER: [
  //     { PATH: ROUTER_CONSTANT.CONTACT_PERSON.ADD, COMPONENT: AddContactPerson, EXACT: false },
  //   ],
  // },
  // {
  //   MENU: Menu.PERSON_PROFILE,
  //   ROUTER: [
  //     { PATH: ROUTER_CONSTANT.PERSON_PROFILE.ADD, COMPONENT: AddPersonProfile, EXACT: false },
  //   ],
  // },
  // {
  //   MENU: Menu.ROLE,
  //   ROUTER: [
  //     { PATH: ROUTER_CONSTANT.ROLE.LIST, COMPONENT: ListRole, EXACT: true },
  //     { PATH: ROUTER_CONSTANT.ROLE.DETAIL + `/:id`, COMPONENT: DetailRole, EXACT: false },
  //     { PATH: ROUTER_CONSTANT.ROLE.ADD, COMPONENT: AddRole, EXACT: false },
  //     { PATH: ROUTER_CONSTANT.ROLE.EDIT + `/:id`, COMPONENT: EditRole, EXACT: false },
  //   ],
  // },
  // {
  //   MENU: Menu.NEXSOFT_ROLE,
  //   ROUTER: [
  //     { PATH: ROUTER_CONSTANT.NEXSOFT_ROLE.LIST, COMPONENT: ListNexsoftRole, EXACT: true },
  //     { PATH: ROUTER_CONSTANT.NEXSOFT_ROLE.DETAIL + `/:id`, COMPONENT: DetailNexsoftRole, EXACT: false },
  //     { PATH: ROUTER_CONSTANT.NEXSOFT_ROLE.ADD, COMPONENT: AddNexsoftRole, EXACT: false },
  //     { PATH: ROUTER_CONSTANT.NEXSOFT_ROLE.EDIT + `/:id`, COMPONENT: EditNexsoftRole, EXACT: false },
  //   ],
  // },
  {
    MENU: Menu.DATA_GROUP,
    ROUTER: [
      { PATH: ROUTER_CONSTANT.DATA_GROUP.LIST, COMPONENT: ListDataGroup, EXACT: true },
  //     { PATH: ROUTER_CONSTANT.DATA_GROUP.DETAIL + `/:id`, COMPONENT: DetailDataGroup, EXACT: false },
  //     { PATH: ROUTER_CONSTANT.DATA_GROUP.ADD, COMPONENT: AddDataGroup, EXACT: false },
  //     { PATH: ROUTER_CONSTANT.DATA_GROUP.EDIT + `/:id`, COMPONENT: EditDataGroup, EXACT: false },
    ],
  },
  // {
  //   MENU: Menu.DATA_SCOPE,
  //   ROUTER: [
  //     { PATH: ROUTER_CONSTANT.DATA_SCOPE.LIST, COMPONENT: ListDataScope, EXACT: true },
  //     { PATH: ROUTER_CONSTANT.DATA_SCOPE.DETAIL + `/:id`, COMPONENT: DetailDataScope, EXACT: false },
  //     { PATH: ROUTER_CONSTANT.DATA_SCOPE.ADD, COMPONENT: AddDataScope, EXACT: false },
  //     { PATH: ROUTER_CONSTANT.DATA_SCOPE.EDIT + `/:id`, COMPONENT: EditDataScope, EXACT: false },
  //   ],
  // },
  // {
  //   MENU: Menu.JOB_PROCESS,
  //   ROUTER: [
  //     { PATH: ROUTER_CONSTANT.JOB_PROCESS.LIST, COMPONENT: ListJobProcess, EXACT: true },
  //     { PATH: ROUTER_CONSTANT.JOB_PROCESS.DETAIL + `/:id`, COMPONENT: DetailJobProcess, EXACT: false },
  //   ],
  // },
  // {
  //   MENU: Menu.AUDIT,
  //   ROUTER: [
  //     { PATH: ROUTER_CONSTANT.AUDIT.LIST, COMPONENT: ListAudit, EXACT: true },
  //     { PATH: ROUTER_CONSTANT.AUDIT.DETAIL + `/:id`, COMPONENT: DetailAudit, EXACT: false },
  //   ],
  // },
  // {
  //   MENU: Menu.PARAMETER,
  //   ROUTER: [
  //     { PATH: ROUTER_CONSTANT.PARAMETER.LIST, COMPONENT: ListParameter, EXACT: true },
  //     { PATH: ROUTER_CONSTANT.PARAMETER.DETAIL + `/:id`, COMPONENT: DetailParameter, EXACT: false },
  //   ],
  // },
];
