import { message } from "antd";
import * as AuthenticationAPI from "../Api/AuthenticationAPI";
import { CRYPTO_RANDOM } from "../Enum/PropertiesConstant";
import { ROUTER_CONSTANT } from "../Enum/RouterConstant";
import { availableMenu } from "../Service/Util";
import * as ErrorHandler from "../Util/ErorrHandler";
import * as ActionType from "./ActionTypes";

export const doPostAuthorize = (crypto_random) => {
  return (dispatch) => {
    AuthenticationAPI.authorize(crypto_random)
      .then((response) => {
        if (response) {
          // message.success({ content: response.data.nexsoft.payload.status.message });
          dispatch({ type: ActionType.SET_CRYPTO_RANDOM, payload: crypto_random });
          dispatch({ type: ActionType.SET_CODE_PKCE, payload: response.headers.authorization });
        }
      })
      .catch((error) => {
        ErrorHandler.errorFetching(dispatch, error);
      });
  };
};

export const rePostAuthorize = (crypto_random, dispatch) => {
  AuthenticationAPI.authorize(crypto_random)
    .then((response) => {
      if (response) {
        // message.success({ content: response.data.nexsoft.payload.status.message });
        dispatch({ type: ActionType.SET_CRYPTO_RANDOM, payload: crypto_random });
        dispatch({ type: ActionType.SET_CODE_PKCE, payload: response.headers.authorization });
      }
    })
    .catch((error) => {
      ErrorHandler.errorFetching(dispatch, error);
    });
};

export const doPostVerify = (username, password, remember, history) => {
  return (dispatch, getState) => {
    const { authentication } = getState();

    if (remember) {
      sessionStorage.setItem("username", username);
      sessionStorage.setItem("password", password);
    } else {
      sessionStorage.removeItem("username");
      sessionStorage.removeItem("password");
    }

    dispatch({ type: ActionType.AUTH_REST_FETCHING });
    AuthenticationAPI.verify(username, password, authentication.code_pkce)
      .then((response) => {
        if (response) {
          // message.success({ content: response.data.nexsoft.payload.status.message });
          doPostGenerateToken(response.data.nexsoft.payload.data.content.authentication_code, authentication.crypto_random, dispatch, history);
        }
      })
      .catch(async (error) => {
        ErrorHandler.errorAuth(dispatch, error, () => rePostAuthorize(CRYPTO_RANDOM, dispatch));
      });
  };
};

export const doPostGenerateToken = (authorization_code, crypto_random, dispatch, history) => {
  AuthenticationAPI.generateToken(authorization_code, crypto_random)
    .then(async (response) => {
      if (response) {
        doUserVerify(response.headers.authorization, response.data.nexsoft.payload.data.content.refresh_token, dispatch, history);
      }
    })
    .catch((error) => {
      ErrorHandler.errorAuth(dispatch, error, () => rePostAuthorize(CRYPTO_RANDOM, dispatch));
    });
};

export const doUserVerify = (token, refresh_token, dispatch, history) => {
  AuthenticationAPI.userVerify(token)
    .then((response) => {
      if (response) {
        console.log("response", response);
        if (response.data.nexsoft.payload.status.success) {
          setSession(token, refresh_token, response.data.nexsoft.payload.data, dispatch);

          setTimeout(() => {
            dispatch({ type: ActionType.AUTH_REST_SUCCESS });
            history.push(ROUTER_CONSTANT.HOME);
          }, 256);
        }
      }
    })
    .catch((error) => {
      ErrorHandler.errorAuth(dispatch, error, () => rePostAuthorize(CRYPTO_RANDOM, dispatch));
    });
};

export const doLogout = (token, dispatch) => {
  dispatch({ type: ActionType.AUTH_REST_FETCHING });
  AuthenticationAPI.logout(token)
    .then((response) => {
      if (response) {
        setTimeout(() => {
          ErrorHandler.restoreApp(dispatch);
          dispatch({ type: ActionType.AUTH_REST_SUCCESS });
          message.success({ content: response.data.nexsoft.payload.status.message });
        }, 512);
      }
    })
    .catch((error) => {
      setTimeout(() => {
        ErrorHandler.restoreApp(dispatch);
        dispatch({ type: ActionType.AUTH_REST_ERROR });
      }, 512);
    });
};

const setSession = (token, refresh_token, data, dispatch) => {
  dispatch({ type: ActionType.TRUE_AUTHENTICATED });
  dispatch({ type: ActionType.SET_TOKEN, payload: token });
  dispatch({ type: ActionType.SET_REFRESH_TOKEN, payload: refresh_token });

  dispatch({ type: ActionType.SET_SCOPE, payload: data.content.scope && data.content.scope.split(" ") });
  dispatch({ type: ActionType.SET_USER, payload: data.content.user_id });
  dispatch({ type: ActionType.SET_LANGUAGE, payload: data.content.locale });

  dispatch({ type: ActionType.SET_FIRST_NAME, payload: data.content.first_name });
  dispatch({ type: ActionType.SET_LAST_NAME, payload: data.content.last_name });
  dispatch({ type: ActionType.SET_ROLE, payload: data.content.role });

  console.log("tes menu", data.content.permission);

  dispatch({ type: ActionType.SET_MENU, payload: data.content.menu && data.content.permission && availableMenu(data.content.menu, data.content.permission) });
  dispatch({ type: ActionType.SET_PERMISSION, payload: data.content.permission && data.content.permission });

  dispatch({ type: ActionType.CLEAR_PROPERTIES });
};
