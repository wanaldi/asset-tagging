import { message } from "antd";
import * as jwt from "jsonwebtoken";
import { refresh } from "../Api/AuthenticationAPI";
import { errorExpired, restoreApp } from "../Util/ErorrHandler";
import * as ActionType from "./ActionTypes";

export const setLanguage = (chosenLanguage) => {
  return (dispatch) => {
    dispatch({ type: ActionType.SET_LANGUAGE, payload: chosenLanguage });
  };
};

export const validateExpired = (token, dispatch) => {
  if (token) {
    Date.now() > jwt.decode(token).exp * 1000 && console.log("Expired");
    return Date.now() > jwt.decode(token).exp * 1000;
  } else {
    restoreApp(dispatch);
  }
};

export const doRefreshToken = async (token, refresh_token, dispatch) => {
  try {
    const response = await refresh(token, refresh_token);
    if (response) {
      message.success({ content: response.data.nexsoft.payload.status.message });
      dispatch({ type: ActionType.SET_TOKEN, payload: response.headers.authorization });
      return response.headers.authorization;
    }
  } catch (error) {
    errorExpired(dispatch, error);
  }
};

export const setOpenKeys = (openKeys) => {
  return (dispatch) => {
    dispatch({ type: ActionType.OPEN_KEYS, payload: openKeys });
  };
};

export const setSelectedKeys = (selectedKeys) => {
  return (dispatch) => {
    dispatch({ type: ActionType.SELECTED_KEYS, payload: selectedKeys });
  };
};

export const setOpenPage = (lastPage) => {
  return (dispatch) => {
    dispatch({ type: ActionType.OPEN_PAGE, payload: lastPage });
  };
};

export const prepareComponent = (dispatch, payload) => {
  dispatch({ type: ActionType.PREPARE_COMPONENT, payload });
};
