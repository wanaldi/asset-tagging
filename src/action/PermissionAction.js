import * as Menu from "../Enum/MenuConstant";
import { generatePermission } from "../Service/PermissionService";
import * as ActionType from "./ActionTypes";

export const checkPermission = () => {
  return (dispatch, getState) => {
    const { list_permission } = getState().authentication;

    // *** Home
    dispatch({
      type: ActionType.PERMISSION_HOME,
      payload: generatePermission(Menu.HOME, list_permission),
    });

    // *** Client User
    dispatch({
      type: ActionType.PERMISSION_CLIENT_USER,
      payload: generatePermission(Menu.CLIENT_USER, list_permission),
    });

    // *** Nexchief Account
    dispatch({
      type: ActionType.PERMISSION_NEXCHIEF_ACCOUNT,
      payload: generatePermission(Menu.NEXCHIEF_ACCOUNT, list_permission),
    });

    // *** Mapping Nexseller
    dispatch({
      type: ActionType.PERMISSION_MAPPING_NEXSELLER,
      payload: generatePermission(Menu.MAPPING_NEXSELLER, list_permission),
    });

    // *** Mapping Principal
    dispatch({
      type: ActionType.PERMISSION_MAPPING_PRINCIPAL,
      payload: generatePermission(Menu.MAPPING_PRINCIPAL, list_permission),
    });

    // --------------------------------------------------------------------

    // *** Company Profile
    dispatch({
      type: ActionType.PERMISSION_COMPANY_PROFILE,
      payload: generatePermission(Menu.COMPANY_PROFILE, list_permission),
    });

    // *** Contact Person
    dispatch({
      type: ActionType.PERMISSION_CONTACT_PERSON,
      payload: generatePermission(Menu.CONTACT_PERSON, list_permission),
    });

    // *** Person Profile
    dispatch({
      type: ActionType.PERMISSION_PERSON_PROFILE,
      payload: generatePermission(Menu.PERSON_PROFILE, list_permission),
    });

    // *** Role
    dispatch({
      type: ActionType.PERMISSION_ROLE,
      payload: generatePermission(Menu.ROLE, list_permission),
    });

    // *** Data Group
    dispatch({
      type: ActionType.PERMISSION_DATA_GROUP,
      payload: generatePermission(Menu.DATA_GROUP, list_permission),
    });

    // *** Data Scope
    dispatch({
      type: ActionType.PERMISSION_DATA_SCOPE,
      payload: generatePermission(Menu.DATA_SCOPE, list_permission),
    });

    // *** Job Process
    dispatch({
      type: ActionType.PERMISSION_JOB_PROCESS,
      payload: generatePermission(Menu.JOB_PROCESS, list_permission),
    });

    // *** Audit
    dispatch({
      type: ActionType.PERMISSION_AUDIT,
      payload: generatePermission(Menu.AUDIT, list_permission),
    });

    // *** Parameter
    dispatch({
      type: ActionType.PERMISSION_PARAMETER,
      payload: generatePermission(Menu.PARAMETER, list_permission),
    });

  };
};
