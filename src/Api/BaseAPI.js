import axios from "axios";
import { API_PREFIX, API_URL, API_VERSION, AUTH_API } from "../Enum/ApiConstant";

const api = `${API_URL}/${API_VERSION}/${API_PREFIX}`;
const loginApi = `${API_URL}/${API_VERSION}`;
const auth_api = `${AUTH_API}`;

export const doGet = (endpoint, token, params) => {
  const url = api + endpoint;
  const method = "GET";
  const headers = { Authorization: token };
  const request = { url, method, headers, params };

  return axios(request);
};

export const doPost = (endpoint, token, data) => {
  const url = api + endpoint;
  const method = "POST";
  const headers = { Authorization: token };
  const request = { url, method, headers, data };

  return axios(request);
};

export const doPut = (endpoint, token, data) => {
  const url = api + endpoint;
  const method = "PUT";
  const headers = { Authorization: token };
  const request = { url, method, headers, data };

  return axios(request);
};

export const doDelete = (endpoint, token, data) => {
  const url = api + endpoint;
  const method = "DELETE";
  const headers = { Authorization: token };
  const request = { url, method, headers, data };

  return axios(request);
};

export const otherPost = (endpoint, type_token, token, data) => {
  const url = api + endpoint;
  const method = "POST";
  const headers = { [type_token]: token };
  const request = { url, method, headers, data };

  return axios(request);
};

export const doGetLogin = (endpoint, token, params) => {
  const url = loginApi + endpoint;
  const method = "GET";
  const headers = { Authorization: token };
  const request = { url, method, headers, params };

  return axios(request);
};

export const doPostLogin = (endpoint, token, data) => {
  console.log(loginApi + endpoint)
  const url = loginApi + endpoint;
  const method = "POST";
  const headers = { Authorization: token };
  const request = { url, method, headers, data };

  return axios(request);
};

// AUTHENTICATION

export const authGet = (endpoint, token, params) => {
  const url = auth_api + endpoint;
  const method = "GET";
  const headers = { Authorization: token };
  const request = { url, method, headers, params };

  return axios(request);
};

export const authPost = (endpoint, token, data) => {
  const url = auth_api + endpoint;
  const method = "POST";
  const headers = { Authorization: token };
  const request = { url, method, headers, data };

  return axios(request);
};

export const authPut = (endpoint, token, data) => {
  const url = auth_api + endpoint;
  const method = "PUT";
  const headers = { Authorization: token };
  const request = { url, method, headers, data };

  return axios(request);
};

export const authDelete = (endpoint, token, data) => {
  const url = auth_api + endpoint;
  const method = "DELETE";
  const headers = { Authorization: token };
  const request = { url, method, headers, data };

  return axios(request);
};

export const authOtherPost = (endpoint, type_token, token, data) => {
  const url = auth_api + endpoint;
  const method = "POST";
  const headers = { [type_token]: token };
  const request = { url, method, headers, data };

  return axios(request);
};
