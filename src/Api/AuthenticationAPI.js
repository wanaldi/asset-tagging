import { sha256 } from "js-sha256";
import * as BaseAPI from "./BaseAPI";

export const authorize = (crypto_random) => {
  const endpoint = `/session/login/authorize`;
  const data = { code_challenger: sha256(crypto_random), redirect_uri: "", response_type: "code_pkce", state: "", wanted_scope: "read write" };

  return BaseAPI.doPostLogin(endpoint, null, data);
};

export const verify = (username, password, code_pkce) => {
  const endpoint = `/session/login/verify`;
  const data = { username, password };

  return BaseAPI.doPostLogin(endpoint, code_pkce, data);
};

export const generateToken = (authorization_code, crypto_random) => {
  const endpoint = `/session/login/token`;
  const data = { authorization_code, code_verifier: crypto_random };

  return BaseAPI.doPostLogin(endpoint, null, data);
};

export const userVerify = (token) => {
  const endpoint = `/session`;

  return BaseAPI.doGetLogin(endpoint, token, null);
};

export const permissionAuthorize = (token) => {
  const endpoint = "/roles/permissions/authorize";

  return BaseAPI.doGetLogin(endpoint, token, null);
};

export const logout = (token) => {
  const endpoint = "/session/logout";

  return BaseAPI.doPostLogin(endpoint, token, null);
};

export const refresh = (token, refresh_token) => {
  const endpoint = "/session/refresh/token";
  const data = { grant_type: "refresh_token", refresh_token };

  return BaseAPI.doPostLogin(endpoint, token, data);
};
