import * as BaseAPI from "../BaseAPI";

export const initiateList = (token, filter) => {
    const endpoint = `/postalcode/initiate`;
    const params = { filter };

    return BaseAPI.doGet(endpoint, token, params);
};

export const list = (token, page, limit, order, filter) => {
    const endpoint = `/postalcode`;
    const params = { page, limit, order, filter };

    return BaseAPI.doGet(endpoint, token, params);
};