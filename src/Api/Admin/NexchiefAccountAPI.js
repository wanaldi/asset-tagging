import * as BaseAPI from "../BaseAPI";

export const initiateList = (token, filter) => {
    const endpoint = `/admin/nexchiefaccount/initiate`;
    const params = { filter };

    return BaseAPI.doGet(endpoint, token, params);
};

export const list = (token, page, limit, order, filter) => {
    const endpoint = `/admin/nexchiefaccount`;
    const params = { page, limit, order, filter };

    return BaseAPI.doGet(endpoint, token, params);
};

export const detail = (token, id) => {
    const endpoint = `/admin/nexchiefaccount/` + id;

    return BaseAPI.doGet(endpoint, token, null);
};

// export const insert = (token, data) => {
//     const endpoint = `/nexchiefaccount`;

//     return BaseAPI.doPost(endpoint, token, data);
// };

export const update = (token, id, data) => {
    const endpoint = `/admin/nexchiefaccount/` + id;

    return BaseAPI.doPut(endpoint, token, data);
};

export const deleted = (token, id, data) => {
    const endpoint = `/admin/nexchiefaccount/` + id;

    return BaseAPI.doDelete(endpoint, token, data);
};