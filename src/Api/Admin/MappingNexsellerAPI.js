import * as BaseAPI from "../BaseAPI";

export const initiateList = (token, filter) => {
    const endpoint = `/admin/mappingnexseller/initiate`;
    const params = { filter };

    return BaseAPI.doGet(endpoint, token, params);
};

export const list = (token, page, limit, order, filter) => {
    const endpoint = `/admin/mappingnexseller`;
    const params = { page, limit, order, filter };

    return BaseAPI.doGet(endpoint, token, params);
};

export const detail = (token, id) => {
    const endpoint = `/admin/mappingnexseller/` + id;

    return BaseAPI.doGet(endpoint, token, null);
};

// export const insert = (token, data) => {
//     const endpoint = `/mappingnexseller`;

//     return BaseAPI.doPost(endpoint, token, data);
// };

export const update = (token, id, data) => {
    const endpoint = `/admin/mappingnexseller/` + id;

    return BaseAPI.doPut(endpoint, token, data);
};

export const deleted = (token, id, data) => {
    const endpoint = `/admin/mappingnexseller/` + id;

    return BaseAPI.doDelete(endpoint, token, data);
};