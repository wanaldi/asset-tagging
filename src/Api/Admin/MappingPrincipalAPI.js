import * as BaseAPI from "../BaseAPI";

export const initiateList = (token, filter) => {
    const endpoint = `/admin/mappingprincipal/initiate`;
    const params = { filter };

    return BaseAPI.doGet(endpoint, token, params);
};

export const list = (token, page, limit, order, filter) => {
    const endpoint = `/admin/mappingprincipal`;
    const params = { page, limit, order, filter };

    return BaseAPI.doGet(endpoint, token, params);
};

export const detail = (token, id) => {
    const endpoint = `/admin/mappingprincipal/` + id;

    return BaseAPI.doGet(endpoint, token, null);
};

// export const insert = (token, data) => {
//     const endpoint = `/mappingprincipal`;

//     return BaseAPI.doPost(endpoint, token, data);
// };

export const update = (token, id, data) => {
    const endpoint = `/admin/mappingprincipal/` + id;

    return BaseAPI.doPut(endpoint, token, data);
};

export const deleted = (token, id, data) => {
    const endpoint = `/admin/mappingprincipal/` + id;

    return BaseAPI.doDelete(endpoint, token, data);
};