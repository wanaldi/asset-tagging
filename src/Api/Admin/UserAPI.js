import * as BaseAPI from "../BaseAPI";

export const initiateList = (token, filter) => {
  const endpoint = `/user/initiate`;
  const params = { filter };

  return BaseAPI.doGet(endpoint, token, params);
};

export const list = (token, page, limit, order, filter) => {
  const endpoint = `/user`;
  const params = { page, limit, order, filter };

  return BaseAPI.doGet(endpoint, token, params);
};

export const detail = (token, id) => {
  const endpoint = `/user/` + id;

  return BaseAPI.doGet(endpoint, token, null);
};

export const insert = (token, data) => {
  const endpoint = `/user`;

  return BaseAPI.doPost(endpoint, token, data);
};

export const update = (token, id, data) => {
  const endpoint = `/user/` + id;

  return BaseAPI.doPut(endpoint, token, data);
};

export const deleted = (token, id, data) => {
  const endpoint = `/user/` + id;

  return BaseAPI.doDelete(endpoint, token, data);
};

export const multipleDelete = (token, data) => {
  const endpoint = `/user/multidelete`;

  return BaseAPI.doDelete(endpoint, token, data);
};

export const initiateInsert = (token) => {
  const endpoint = `/user/insert/initiate`;

  return BaseAPI.doGet(endpoint, token, null);
};

export const Activation = (data, namespace) => {
  const endpoint = `/oauth/users/${namespace}/code`;

  return BaseAPI.authPost(endpoint, null, data);
};

export const resendActivation = (data, namespace) => {
  const endpoint = `/oauth/users/${namespace}/code`;

  return BaseAPI.authPut(endpoint, null, data);
};

export const changePassword = (token, data) => {
  const endpoint = "/oauth/users/password/admin";

  return BaseAPI.authPost(endpoint, token, data);
};

export const changeOwnPassword = (token, data) => {
  const endpoint = "/oauth/users/password";

  return BaseAPI.authPost(endpoint, token, data);
};

export const forgetPassword = (type_token, namespace, token, data) => {
  const endpoint = `/${namespace}/forget`;

  return BaseAPI.otherPost(endpoint, type_token, token, data);
};

export const resetPassword = (type_token, namespace, token, data) => {
  const endpoint = `/oauth/users/${namespace}/password`;

  return BaseAPI.authOtherPost(endpoint, type_token, token, data);
};

export const authDetail = (token, id) => {
  const endpoint = `/user/auth/` + id;

  return BaseAPI.doGet(endpoint, token, null);
};

export const updateAuth = (token, id, data) => {
  const endpoint = `/user/auth/` + id;

  return BaseAPI.doPut(endpoint, token, data);
};

export const addResource = (token, data) => {
  const endpoint = `/user/addresource`;

  return BaseAPI.doPost(endpoint, token, data);
};

export const CheckUser = (token, data) => {
  const endpoint = `/user/check`;

  return BaseAPI.doPost(endpoint, token, data);
};