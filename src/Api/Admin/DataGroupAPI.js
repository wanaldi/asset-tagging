import * as BaseAPI from "../BaseAPI";

export const initiateList = (token, filter) => {
    const endpoint = `/admin/datagroup/initiate`;
    const params = { filter };

    return BaseAPI.doGet(endpoint, token, params);
};

export const list = (token, page, limit, order, filter) => {
    const endpoint = `/admin/datagroup`;
    const params = { page, limit, order, filter };

    return BaseAPI.doGet(endpoint, token, params);
};

export const detail = (token, id) => {
    const endpoint = `/admin/datagroup/` + id;

    return BaseAPI.doGet(endpoint, token, null);
};

// export const insert = (token, data) => {
//     const endpoint = `/datagroup`;

//     return BaseAPI.doPost(endpoint, token, data);
// };

export const update = (token, id, data) => {
    const endpoint = `/admin/datagroup/` + id;

    return BaseAPI.doPut(endpoint, token, data);
};

export const deleted = (token, id, data) => {
    const endpoint = `/admin/datagroup/` + id;

    return BaseAPI.doDelete(endpoint, token, data);
};