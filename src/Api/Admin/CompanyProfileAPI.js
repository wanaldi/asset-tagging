import * as BaseAPI from "../BaseAPI";

export const initiateList = (token, filter) => {
    const endpoint = `/companyprofile/initiate`;
    const params = { filter };

    return BaseAPI.doGet(endpoint, token, params);
};

export const list = (token, data) => {
    const endpoint = `/companyprofile/list`;

    return BaseAPI.doPost(endpoint, token, data);
};

export const detail = (token, id) => {
    const endpoint = `/companyprofile/` + id;

    return BaseAPI.doGet(endpoint, token, null);
};

// export const insert = (token, data) => {
//     const endpoint = `/companyprofile`;

//     return BaseAPI.doPost(endpoint, token, data);
// };
