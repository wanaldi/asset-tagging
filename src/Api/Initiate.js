import * as BaseAPI from "./BaseAPI";

export const initiate = (token, namespace, filter) => {
  const endpoint = `/${namespace}/initiate`;
  const params = { filter };

  return BaseAPI.doGet(endpoint, token, params);
};

export const initiateFromMasterData = (token, namespace, filter) => {
  const endpoint = `/${namespace}/initiate`;
  const params = { filter };

  return BaseAPI.doPost(endpoint, token, params);
};

export const data = (token, namespace, page, limit, order, filter) => {
  const endpoint = `/${namespace}`;
  const params = { page, limit, order, filter };

  return BaseAPI.doGet(endpoint, token, params);
};

export const list = (token, namespace, page, limit, order, filter) => {
  const endpoint = `/${namespace}`;
  const params = { page, limit, order, filter };

  return BaseAPI.doGet(endpoint, token, params);
};

export const listFromMasterData = (token, namespace, data) => {
  const endpoint = `/${namespace}`;

  return BaseAPI.doPost(endpoint, token, data);
};

export const menu = (token, namespace, id) => {
  let endpoint = `/menu/${namespace}`;
  if (id) endpoint = `/menu/${namespace}/${id}`;

  return BaseAPI.doGet(endpoint, token, null);
};
