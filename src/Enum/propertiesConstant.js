import cryptoRandomString from "crypto-random-string";

export const X_NEXTOKEN = "Nexchief2021";
export const DEFAULT_LANGUAGE = "id-ID";
export const VERSION_APP = process.env.REACT_APP_VERSION;
export const CRYPTO_RANDOM = cryptoRandomString({ length: 13, type: "numeric" });
export const constantLayout = { labelCol: { sm: { span: 4 } }, wrapperCol: { sm: { span: 10 } } };
export const constantButtonLayout = { wrapperCol: { sm: { span: 10, offset: 4 } } };

export const constantStatus = {
  active: "A",
  non_active: "N",
  yes: "Y",
  no: "N",
};

export const RANDOM_ID = (length) => {
  let result = "";
  let characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let charactersLength = characters.length;

  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
};

export const propTable = {
  page: 1,
  limit: 10,
  filter: null,
  search_by: null,
  operator: null,
  search_key: null,
  order: null,
  order_by: null,
  order_params: "asc",
};

export const role = {
  pageRole: 1,
  limitRole: 10,
  orderRole: null,
  prefixRole: null,
  filterRole: null,
  initiateRole: null,
  dataRole: [],
  lastPageRole: false,
};

export const nexchiefaccount = {
  pageNexchiefAccount: 1,
  limitNexchiefAccount: 10,
  orderNexchiefAccount: null,
  prefixNexchiefAccount: null,
  filterNexchiefAccount: null,
  initiateNexchiefAccount: null,
  dataNexchiefAccount: [],
  lastPageNexchiefAccount: false,
};

export const mappingnexseller = {
  pageMappingNexseller: 1,
  limitMappingNexseller: 10,
  orderMappingNexseller: null,
  prefixMappingNexseller: null,
  filterMappingNexseller: null,
  initiateMappingNexseller: null,
  dataMappingNexseller: [],
  lastPageMappingNexseller: false,
};

export const mappingprincipal = {
  pageMappingPrincipal: 1,
  limitMappingPrincipal: 10,
  orderMappingPrincipal: null,
  prefixMappingPrincipal: null,
  filterMappingPrincipal: null,
  initiateMappingPrincipal: null,
  dataMappingPrincipal: [],
  lastPageMappingPrincipal: false,
};

export const group = {
  pageGroup: 1,
  limitGroup: 10,
  orderGroup: null,
  prefixGroup: null,
  filterGroup: null,
  initiateGroup: null,
  dataGroup: [],
  lastPageGroup: false,
};

export const user = {
  pageUser: 1,
  limitUser: 10,
  orderUser: null,
  prefixUser: null,
  filterUser: null,
  initiateUser: null,
  dataUser: [],
  lastPageUser: false,
};

export const postalcode = {
  pagePostalCode: 1,
  limitPostalCode: 10,
  orderPostalCode: null,
  prefixPostalCode: null,
  filterPostalCode: null,
  initiatePostalCode: null,
  dataPostalCode: [],
  lastPagePostalCode: false,
};

export const country = {
  pageCountry: 1,
  limitCountry: 10,
  orderCountry: null,
  prefixCountry: null,
  filterCountry: null,
  initiateCountry: null,
  dataCountry: [],
  lastPageCountry: false,
};

export const geotree = {
  pageGeoTree: 1,
  limitGeoTree: 10,
  orderGeoTree: null,
  prefixGeoTree: null,
  filterGeoTree: null,
  initiateGeoTree: null,
  dataGeoTree: [],
  lastPageGeoTree: false,
};

export const pricegroup = {
  pagePriceGroup: 1,
  limitPriceGroup: 10,
  orderPriceGroup: null,
  prefixPriceGroup: null,
  filterPriceGroup: null,
  initiatePriceGroup: null,
  dataPriceGroup: [],
  lastPagePriceGroup: false,
};

export const productcategory = {
  pageProductCategory: 1,
  limitProductCategory: 10,
  orderProductCategory: null,
  prefixProductCategory: null,
  filterProductCategory: null,
  initiateProductCategory: null,
  dataProductCategory: [],
  lastPageProductCategory: false,
};