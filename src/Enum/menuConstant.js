export const HOME = `admin.home`;

// ------------ SYSADMIN --------------
export const CLIENT_USER = "admin.user"
// export const NEXSOFT_USER = "admin.user"
export const NEXCHIEF_ACCOUNT = "admin.nexchief-account"
export const PARAMETER = "admin.parameter"
export const DATA_SCOPE = "admin.data-scope"
export const DATA_GROUP = "admin.data-group"
export const NEXSOFT_ROLE = "admin.nexsoft-role"
export const ROLE = "admin.role"
export const MAPPING_NEXSELLER = "admin.mapping-nexseller"
export const MAPPING_PRINCIPAL = "admin.mapping-principal"
export const LOGIN_LOG = "admin.login-log"
export const SERVER_LOG = "admin.server-log"
export const COMPANY_PROFILE = "admin.company-profile"
export const PERSON_PROFILE = "admin.person-profile"
export const CONTACT_PERSON = "admin.contact-person"
export const AUDIT = "admin.audit"
export const JOB_PROCESS = "admin.job-process"

// ------------ SYS USER --------------
export const MenuUserConstanta = "setup.role.user"
export const MenuNexchiefAccountParameterConstanta = "setup.setup.nexchief-account-parameter"