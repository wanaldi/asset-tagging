export const API_URL = process.env.REACT_APP_API_URL;
export const API_VERSION = process.env.REACT_APP_API_VERSION;
export const API_PREFIX = process.env.REACT_APP_API_PREFIX;
export const AUTH_API = process.env.REACT_APP_AUTH_API;
