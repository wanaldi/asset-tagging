export const ROUTER_CONSTANT = Object.freeze({
  INDEX: `/`,
  LOGIN: `/`,
  FORBIDDEN: `/forbidden`,
  HOME: `/admin/home`,
  FORGET_PASSWORD: `/admin/forget/password`,
  RESET_PASSWORD: `/admin/reset/password`,

  NEXCHIEF_ACCOUNT: {
    LIST: `/admin/nexchiefaccount`,
    DETAIL: `/admin/nexchiefaccount/detail`,
    EDIT: `/admin/nexchiefaccount/edit`,
  },

  MAPPING_NEXSELLER: {
    LIST: `/admin/mappingnexseller`,
    DETAIL: `/admin/mappingnexseller/detail`,
    ADD: `/admin/mappingnexseller/add`,
    EDIT: `/admin/mappingnexseller/edit`,
  },

  MAPPING_PRINCIPAL: {
    LIST: `/admin/mappingprincipal`,
    DETAIL: `/admin/mappingprincipal/detail`,
    ADD: `/admin/mappingprincipal/add`,
    EDIT: `/admin/mappingprincipal/edit`,
  },

  ROLE: {
    LIST: `/role`,
    DETAIL: `/role/detail`,
    ADD: `/role/add`,
    EDIT: `/role/edit`,
  },

  NEXSOFT_ROLE: {
    LIST: `/nexsoftrole`,
    DETAIL: `/nexsoftrole/detail`,
    ADD: `/nexsoftrole/add`,
    EDIT: `/nexsoftrole/edit`,
  },

  DATA_GROUP: {
    LIST: `/datagroup`,
    DETAIL: `/datagroup/detail`,
    ADD: `/datagroup/add`,
    EDIT: `/datagroup/edit`,
  },

  CLIENT_USER: {
    LIST: `/admin/user`,
    DETAIL: `/user/detail`,
    ADD: `/admin/user/add`,
    EDIT: `/user/edit`,
    VERIFY: `/user/verify`,
    PASSWORD: `/user/password`,
    OWN_PASSWORD: `/user/own-password`,
    ADD_RESOURCE: `/user/add-resource`,
    CHECK_USER: `/user/check-user`,
    EDIT_RESOURCE: `/user/edit-resource`,
  },

  JOB_PROCESS: {
    LIST: `/monitoring/jobprocess`,
    DETAIL: `/monitoring/jobprocess/detail`,
  },

  AUDIT: {
    LIST: `/monitoring/audit`,
    DETAIL: `/monitoring/audit/detail`,
  },

  PARAMETER: {
    LIST: `/parameter`,
    DETAIL: `/parameter/detail`,
  },

  LOGIN_LOG: {
    LIST: `/loginlog`,
    DETAIL: `/loginlog/detail`,
    ADD: `/loginlog/add`,
    EDIT: `/loginlog/edit`,
  },

});
